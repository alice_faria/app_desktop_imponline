import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { parametroObrigatorio } from 'prop-types';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

export const initMock = state => mockStore(state);

export const mockServiceCreator = ({
  body = parametroObrigatorio('body'),
  success = true
}) => () => new Promise((resolve, reject) => (success ? resolve(body) : reject(body)));
