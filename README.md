IMP Online Desktop - Documentação do Projeto
Este documento deve servir como fonte central de informação técnica sobre o projeto.

# 1 - Dependências
O projeto utiliza ou depende em algum grau dos seguintes pacotes/ferramentas:
# React 16.8.6
# React Redux 7.0.3
# React Router 5.0
# Node-sass
# SonarQube Scanner

Você poderá encontrar informações sobre como instalar estes pacotes na página web específica de cada um deles. Instale-os antes de
prosseguir com o desenvolvimento.
Obs.: A versão a ser utilizada é a mais recente, exceto quando explicitado de outra forma

# 2 - Instalação
Apos clonar o projeto, execute os seguintes comandos no seu terminal de preferência:
# cd ~/path/to/repo/clone/folder
# git fetch
# git checkout dev
# npm i -s
# npm run start-electron
Lembre-se de executar $ npm i -s sempre que fizer checkout para uma branch que você não criou localmente. Isso garantirá que
todas as dependências internas do código estejam instaladas, prevenindo erros comuns.
