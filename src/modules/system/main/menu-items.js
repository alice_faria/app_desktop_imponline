export const menuItems = [
  { path: '/', icon: 'course', label: 'Meus cursos' },
  { path: '/bookmark', icon: 'bookmark', label: 'Meus favoritos' },
  // { path: '/downloads', icon: 'download', label: 'Meus downloads' },
  { path: '/messages', icon: 'chats', label: 'Mensagens' },
  { path: '/dados', icon: 'user', label: 'Meus Dados' }
];
