import React, { useState } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import { Sidebar, Header } from 'common/components/layout';
import Message from 'common/components/message/message';

import MenuSystem from './menu-system';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    htmlFontSize: 8
  }
});

const ModuleSystem = props => {
  const [open,setOpen] = useState(false);

  return (
    <MuiThemeProvider theme={theme}>
      <div className='wrapper'>
        <Header />
        <Sidebar toggleSidebar={setOpen}>
          <MenuSystem open={open}/>
        </Sidebar>
        <div className="content-wrapper content-collapse" id="contentRoot">
          <div className="container-fluid" style={{ fontSize: '1.6rem' }}>
            {props.children}
          </div>
        </div>
        {/* <Footer /> */}
        <Message />
      </div>
    </MuiThemeProvider>
  );
};

export default ModuleSystem;
