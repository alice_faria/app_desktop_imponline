import React from 'react';

import {
  MenuItem
} from 'common/components/layout';

import { menuItems } from './menu-items';

const MenuSystem = props => {
  let items = menuItems;

  const renderMenu = () => {
    return items.map((item, index) => {
      return <MenuItem key={index} path={item.path} label={item.label} icon={item.icon} open={props.open}/>;
    });
  };

  return (
    <ul className="sidebar-menu">
      {renderMenu()}
    </ul >
  );

};

export default (MenuSystem);
