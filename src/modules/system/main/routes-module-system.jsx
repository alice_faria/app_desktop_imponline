import React from 'react';

import { Route, Switch, Redirect } from 'react-router-dom';
import Home from '../pages/home/home';
import Course from '../pages/course/course';
import Favorites from '../pages/favorites/favorites';
import Downloads from '../pages/downloads/downloads';
import Messages from '../pages/messages/messages';
import Consulting from '../pages/consulting/consulting';
import Dados from '../pages/dados/dados';

export const systemRouters = (
  <Switch path="system">
    <Route exact path='/' component={(props) => <Home {...props} />} />
    <Route exact path='/courses/:id' component={(props) => <Course {...props} />} />
    <Route exact path='/consulting/:id' component={(props) => <Consulting {...props} />} />
    <Redirect from="/search/:id" to="/courses/:id" />
    {/* <Route exact path='/search/:id' component={(props) => <Search {...props} />} /> */}
    <Route exact path='/bookmark' component={(props) => <Favorites {...props} />} />
    <Route exact path='/downloads' component={(props) => <Downloads {...props} />} />
    <Route exact path='/messages' component={(props) => <Messages {...props} />} />
    <Route exact path='/dados' component={(props) => <Dados {...props} />} />
  </Switch>
);
