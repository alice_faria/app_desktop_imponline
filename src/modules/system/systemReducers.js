import homeReducer from './pages/home/homeReducer';
import courseReducer from './pages/course/courseReducer';
import favoritesReducer from './pages/favorites/favoritesReducer';
import downloadsReducer from './pages/downloads/downloadsReducer';
import messagesReducer from './pages/messages/messagesReducer';
import consultingReducer from './pages/consulting/consultingReducer';
import dadosReducer from './pages/dados/dadosReducer';

export default {
  homePage: homeReducer,
  course: courseReducer,
  favorites: favoritesReducer,
  downloads: downloadsReducer,
  messages: messagesReducer,
  consulting: consultingReducer,
  dados: dadosReducer
};
