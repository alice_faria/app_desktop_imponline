/* eslint-disable no-fallthrough */
import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter, Redirect } from 'react-router-dom';

import { setLoading } from 'helpers/redux';
import moment from 'moment';
import 'moment/locale/pt-br';

import intl from 'react-intl-universal';
import ReactTooltip from 'react-tooltip';
import Img from 'react-image';
import _ from 'lodash';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

import Loading from 'common/components/ui/loading/loading';
import { getList, getCampaign, getListConsulting, getLastVideos, reset, openPlanner, postRenovacao } from './homeActions';
import { videoToOpen } from 'common/containers/containersActions';
import { setCourse } from '../course/courseActions';
import { setConsulting, loginMaximize } from '../consulting/consultingActions';
import Carousel from 'common/components/ui/carousel/carousel';
import emptyHome from 'assets/images/empty__curso.png';
import disconnected from 'assets/images/img_internet.png';
import Flickity from 'react-flickity-component';
import types from './homeTypes';
import { CarouselVideos } from 'common/components/ui/carousel/carouselVideo';
import RenovacaoModal from "../../../../common/components/ui/modals/renovacaoModal/renovacaoModal";
import {animateScroll as scroll} from "react-scroll";

moment.locale('pt-br');

export class Home extends Component {
  constructor(props) {
    super(props);
    this.verifyData();
    this.props.setLoading(types.HOME_LAST_VIDEO_LOADING, true);
    this.props.getLastVideos();
    this.state = {
      showActived: true,
      consulting: [],
      navigate: undefined,
      showModal: false,
      dadosRenovacao: undefined
    };
  }


  toggleActived = (toggle) => {
    this.setState({ showActived: toggle });
  }

  verifyData = () => {
    if (_.isEmpty(this.props.home.active)) {
      this.props.setLoading(types.HOME_LOADING, true);
      this.props.getList();
      this.props.getListConsulting();
      this.props.getCampaign(this.props.user.id_matricula);
    }
  }

  reset = () => {
    this.props.reset();
    setTimeout(() => {
      this.props.setLoading(types.HOME_LOADING, true);
      this.props.getList();
      this.props.getListConsulting();
      this.props.getCampaign(this.props.user.id_matricula);
    }, 500);
  }

  openPlanner = (item) => {
    this.props.setLoading(types.HOME_PLANNER_LOADING, true);
    this.props.openPlanner(item._id, item.matricula);
  }

  showModal(renovacao, id_matricula ,state){
    renovacao.id_matricula = id_matricula;
    this.setState({ showModal: true, dadosRenovacao: renovacao });
  }

  closeModal = () => {
    this.setState({ showModal: false, dadosRenovacao: undefined });
  }

  efetuarRenovacao = (id_produto, id_matricula) => {
    this.props.postRenovacao(id_produto, id_matricula, this.props.user.st_nomecompleto, this.props.user.id_usuario);
  }

  renderEmptyCourses = listCourses => {
    if (_.isEmpty(listCourses) && !this.state.showActived) {
      return (
        <div className="emptyHome">
          <img src={emptyHome} className="emptyHome__img" alt="empty" />
          <span className="emptyHome__title">{intl.getHTML('home.emptyPage')}</span>
        </div>
      );
    } else if (_.isEmpty(listCourses) && this.state.showActived) {
      return (
        <div className="emptyHome">
          <img src={emptyHome} className="emptyHome__img" alt="empty" />
          <span className="emptyHome__title">{intl.getHTML('home.emptyActive')}</span>
        </div>
      );
    }
    return listCourses;
  }

  renderLastVideos = () => {
    const { lastVideos, setCourse, videoToOpen } = this.props;
    return (
      <React.Fragment>
        <span className="campaign__title">{intl.get('home.lastVideos')}</span>
        <li className="courses" id="carousel">
          <div className="lastVideos__body">
            {!this.props.loadingLastVideos && !_.isNull(lastVideos)
              ? <CarouselVideos
                items={lastVideos}
                numberItems={0}
                animationDuration={1000}
                renderItems={true}
                classNameNext="courses__carousel--button-next lastVideos__buttons"
                classNamePrev="courses__carousel--button-prev lastVideos__buttons"
                infinite={false}
                slideClass="carousel__slide"
                autoPlay={false}
                setCourse={setCourse}
                videoToOpen={videoToOpen}
              />
              : <Loading isVisible={this.props.loadingLastVideos} overlay={false} width={100} height={100} className='lastVideos__loading' />}
          </div>
        </li>
      </React.Fragment>
    );
  }

  renderCourses = () => {
    const { home } = this.props; //myCourses
    let courses = this.state.showActived ? home.active : home.inactive;
    let listCourses = [];
    for (const [index, item] of courses.entries()) {
      const today = moment();
      const limitDate = moment(_.isNull(item.endDate) ? moment() : item.endDate);
      const course = (
        <React.Fragment key={index}>
          <li className="courses" key={index} id="carousel">
            <div className="courses-header">
              <span className="courses__title">{item.title}</span>
              {this.state.showActived
                ? <span className="courses__title--time">
                  <i className="icon-calendar courses__title--time-icon" />
                  {limitDate.diff(today, 'days')} {intl.get('home.days')}
                </span>
                : <span className="courses__title--time">
                  <i className="icon-calendar_off courses__title--time-icon" />
                  {intl.get('home.iconInative')}
                </span>
              }
              {item.dadosRenovacao ?
                <span className="courses__renovacao" style={{paddingRight:'1rem'}}>
                  <button
                    onClick={()=>{this.showModal(item.dadosRenovacao, item.matricula)}}
                    className="courses__renovacao--button">
                    <span className="courses__renovacao__title">Renovar</span>
                  </button>
                </span>
                :''}
              {item.bl_planner === '1' && this.state.showActived ?
                <span
                  data-tip={this.props.loadingPlanner ? "Carregando.." :"Acessar Planner" }
                  data-effect="solid"
                  data-place="left"
                  className="courses__planner">
                <button
                  onClick={()=>this.openPlanner(item)}
                  disabled={this.props.loadingPlanner}
                  className="courses__planner">
                  <span className="courses__planner__title">IMP Planner</span>
                  <i className="icon-right-arrow"></i>
                </button></span>:''
              }
            </div>
            <div className="courses__divider"></div>
            <div className="courses__body">
              {!_.isUndefined(item.subject)
                ? <Carousel
                  items={item.subject.courses}
                  id={item._id}
                  matricula={item.matricula}
                  numberItems={0}
                  animationDuration={1000}
                  renderItems={true}
                  classNameNext="courses__carousel--button-next"
                  classNamePrev="courses__carousel--button-prev"
                  active={this.state.showActived}
                  infinite={false}
                  slideClass="carousel__slide"
                  autoPlay={false}
                  setCourse={this.props.setCourse}
                />
                : null}
            </div>
          </li>
          {this.state.showActived && !_.isEmpty(this.props.campaign) && index === 0
            ? this.renderCampaign()
            : null}
        </React.Fragment>

      );
      if (!_.isUndefined(item.subject) && !_.isEmpty(item.subject.courses)) {
        listCourses = listCourses.concat(course);
      }
    }
    // this.renderEmptyCourses(listCourses);
    return this.renderEmptyCourses(listCourses);
  }

  renderOptions = (options) => {
    const optionHandled = options && options.map((option, index) => {
      if (moment().diff(moment(option.date)) > 0) {
        if (option.simulado) {
          return {
            value: option,
            label: 'Simulado'
          };
        }
        return {
          value: option,
          label: `${index + 1}ª semana`
        };
      }
      return null;
    });
    return optionHandled.filter((el) => {
      return el !== null && el !== undefined;
    });
  }

  _onSelect = (index, val) => {
    const consulting = [...this.state.consulting];
    if (val.value.simulado) {
      consulting[index - 1] = val;
      this.setState({ consulting });
      const url = val.value.url;
      this.props.loginMaximize(this.props.user.id_matricula, url);
    } else {
      consulting[index] = val;
      this.setState({ consulting });
    }
  }

  selectingConsulting = (consulting, matricula, id_projetopedagogico) => {
    this.setState({
      navigate: `${matricula}+${id_projetopedagogico}`
    }, () => {
      this.props.setConsulting(consulting);
    });
  }

  handlingConsulting = (item, index) => {
    let consultings = [];
    if (this.state.consulting[index] && !_.isNull(this.state.consulting[index])) {
      consultings = this.state.consulting[index].value.data;
    } else {
      consultings = item.subject.courses[0].data;
    }
    if (_.isUndefined(item.subject) && _.isEmpty(item.subject.courses)) {
      return null;
    }
    return consultings;
  }

  renderConsulting = () => {
    const { consulting } = this.props; //myCourses
    let PP = this.state.showActived ? consulting.consultingActive : consulting.consultingInactive;
    let listCourses = [];
    const today = moment();
    listCourses = PP && PP.map((item, index) => {
      const limitDate = moment(_.isNull(item.endDate) ? today : item.endDate);
      const consultings = this.handlingConsulting(item, index);
      return (
        <React.Fragment key={index}>
          <li className="courses" key={index} id="carousel">
            <div className="courses-header--titles">
              <div className="courses-header">
                <span className="courses__title">{item.title}</span>
                {this.state.showActived
                  ? <span className="courses__title--time">
                    <i className="icon-calendar courses__title--time-icon" />
                    {limitDate.diff(today, 'days')} {intl.get('home.days')}
                  </span>
                  : <span className="courses__title--time">
                    <i className="icon-calendar_off courses__title--time-icon" />
                    {intl.get('home.iconInative')}
                  </span>
                }
                {item.dadosRenovacao ?
                  <span className="courses__renovacao" style={{paddingRight:'1rem'}}>
                  <button
                    onClick={()=>{this.showModal(item.dadosRenovacao, item.matricula)}}
                    className="courses__renovacao--button">
                    <span className="courses__renovacao__title">Renovar</span>
                  </button>
                </span>
                  :''}
              </div>
              <Dropdown
                options={this.renderOptions(item.subject.courses)}
                onChange={val => this._onSelect(index, val)}
                value={this.state.consulting[index] ? this.state.consulting[index].label : this.renderOptions(item.subject.courses)[0].label}
                className="courses__title--weeks"
                placeholderClassName="courses__title--weeks-placeholder"
                controlClassName="courses__title--weeks-control"
                menuClassName="courses__title--weeks-menu"
                arrowClassName="courses__title--weeks-arrow"
                arrowClosed={<i className="icon-down-arrow arrow-closed" />}
                arrowOpen={<i className="icon-up-arrow arrow-open" />}
              />
            </div>
            <div className="courses__divider"></div>
            <div className="courses__body">
              {!_.isUndefined(item.subject)
                ? <Carousel
                  items={consultings}
                  id={item._id}
                  matricula={item.matricula}
                  numberItems={0}
                  animationDuration={1000}
                  renderItems={true}
                  onClick={(data, index) => this.selectingConsulting(data, item.matricula, item._id)}
                  classNameNext="courses__carousel--button-next"
                  classNamePrev="courses__carousel--button-prev"
                  active={this.state.showActived}
                  infinite={false}
                  slideClass="carousel__slide"
                  autoPlay={false}
                />
                : null}
            </div>
          </li>
        </React.Fragment>
      );
    });
    if (_.isEmpty(listCourses)) {
      return null;
    }
    return (
      <React.Fragment>
        <span className="campaign__title">{intl.get('home.consulting')}</span>
        {listCourses}
      </React.Fragment>
    );
  }

  renderCampaign = () => (
    <div className="home__body--campaign">
      <span className="campaign__title">{intl.get('home.highlights')}</span>
      <div className="campaign__body">
        <Flickity
          className="campaign__carousel"
          options={{
            prevNextButtons: false,
            autoPlay: 4000,
            cellAlign: 'center',
            pauseAutoPlayOnHover: true
          }}
        >
          {this.props.campaign.map((item, index) => (
            <div className="campaign__carousel" key={item.id_campanhacomercial}>
              <a target="_blank" rel="noopener noreferrer" href={item.st_link} className="campaign__carousel--item" id="campaign-container">
                <Img src={item.st_imagem} className="campaign__img" alt={item.st_descricao} />
              </a>
            </div>
          ))}

        </Flickity>
      </div>
    </div>
  )

  render() {
    if (this.props.loading || _.isEmpty(this.props.home)) {
      return <Loading isVisible={this.props.loading} overlay={true} />;
    } else if (this.props.disconnected) {
      return (
        <div className="emptyHome">
          <img src={disconnected} className="emptyHome__img" alt="empty" />
          <span className="emptyHome__title">{intl.getHTML('errorRequest.connection')}</span>
        </div>
      );
    }
    if (!_.isUndefined(this.state.navigate)) {
      return <Redirect to={`/consulting/${this.state.navigate}`} push={true} />;
    }

    return (
      <section className="home" data-test='homeComponent'>
        {this.state.showModal ?
          <RenovacaoModal
          isVisible={this.state.showModal}
          closeModal={this.closeModal}
          dadosRenovacao={this.state.dadosRenovacao}
          efetuarRenovacao={this.efetuarRenovacao}
          /> : ''
        }
        <ReactTooltip />
        <div className="home__header">
          <span className="home__header--title">{intl.get('home.title')}</span>
          <i className="icon-refresh home__header--icon" onClick={() => this.reset()}/>
          <span className={`home__header--select${!this.state.showActived ? '-active' : ''}`}>
            <span
              className={`home__header--select-label${this.state.showActived ? '-active' : ''}`}
              onClick={() => this.toggleActived(true)}
            >
              {intl.get('home.active')}
            </span>
            <span
              className={`home__header--select-label${!this.state.showActived ? '-active' : ''}`}
              onClick={() => this.toggleActived(false)}
            >
              {intl.get('home.inactive')}
            </span>
          </span>
        </div>
        <div className="home__body">
          {this.state.showActived && !_.isEmpty(this.props.lastVideos)
            ? <ul className="home__body--courses">
              {this.renderLastVideos()}
            </ul>
            : null}
          <ul className="home__body--courses">
            {this.renderCourses()}
          </ul>
          <ul className="home__body--courses">
            {this.renderConsulting()}
          </ul>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.homePage.loading,
    loadingLastVideos: state.homePage.loadingLastVideos,
    loadingPlanner : state.homePage.loadingPlanner,
    user: state.auth.user,
    home: state.homePage.myClasses,
    consulting: state.homePage.myConsulting,
    lastVideos: state.homePage.lastVideos,
    disconnected: state.homePage.disconnected,
    campaign: state.homePage.campaign
  };
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      setLoading,
      getList,
      getListConsulting,
      getLastVideos,
      getCampaign,
      setCourse,
      reset,
      setConsulting,
      loginMaximize,
      openPlanner,
      videoToOpen,
      postRenovacao
    },
    dispatch
  )
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Home));
