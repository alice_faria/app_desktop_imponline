/* eslint-disable no-undefined */
import HomeReducers from '../homeReducer';
import types from '../homeTypes';

describe('Testing Home Reducers', () => {
  const initialState = {
    myClasses: {
      active: [],
      inactive: []
    },
    myConsulting: {
      consultingActive: [],
      consultingInactive: []
    },
    lastVideos: [],
    campaign: {},
    disconnected: false,
    loadingClasses: true,
    loadingConsulting: true,
    loadingLastVideos: false,
    loading: false
  };

  it('should return initial state', () => {
    const action = { type: 'dummy_action' };

    expect(HomeReducers(undefined, action)).toEqual(initialState);
  });

  it('should activate loading component', () => {
    const action = { type: types.HOME_LOADING, payload: true };
    const expetedState = { ...initialState, loading: true };

    expect(HomeReducers(undefined, action)).toEqual(expetedState);
  });

  it('should deactivate loading component', () => {
    const action = { type: types.HOME_LOADING, payload: false };
    const expetedState = { ...initialState, loading: false };

    expect(HomeReducers(undefined, action)).toEqual(expetedState);
  });
  it('should save user token in localStorage and return then', () => {
    const token = '[jwt token]';
    const action = { type: types.HOME_CAMPAIGN_FETCHED, payload: token };

    expect(HomeReducers(undefined, action)).toMatchSnapshot();
  });

});
