/* eslint-disable no-undefined */
import React from 'react';
import { shallow } from 'enzyme';

import { Home as UnconnectedHome } from '../home';
import { findByTestAtrr } from 'helpers/wrappers';

const setup = ({ getList, setLoading, getCampaign, getLastVideos, home, campaign, user, consulting }) => {
  const wrapper = shallow(
    <UnconnectedHome
      getList={getList}
      getCampaign={getCampaign}
      setLoading={setLoading}
      home={home}
      campaign={campaign}
      user={user}
      getLastVideos={getLastVideos}
      consulting={consulting}
    />
  );
  return wrapper;
};

//Testing the connected component
describe('Testing Home', () => {
  let wrapper;

  beforeEach(() => {
    const campaign = [
      {
        id_campanhacomercial: '2392',
        st_campanhacomercial: 'Teste MOBILE',
        bl_aplicardesconto: '0',
        bl_ativo: '1',
        bl_disponibilidade: '1',
        st_link: 'http://www.globo.com',
        st_imagem: 'http://g2evolutiva.unyleya.xyz/upload/campanha/campanha_5d03d3055f4de.jpg'
      }
    ];
    const user = {
      id_usuario: 423424
    };
    const home = {
      active: [
        {
          subject: {},
          _id: '6882',
          title: 'Temas Complementares de Direito Constitucional',
          endDate: '2019-12-23 00:00:00.000000'
        }
      ],
      inactive: []
    };
    const consulting = {
      consultingActive: [
        {
          subject: {
            courses: [
              {
                _id: '6882',
                title: 'Temas Complementares de Direito Constitucional',
                date: '2019-10-23 00:00:00.000000'
              }
            ]
          },
          _id: '6882',
          title: 'Temas Complementares de Direito Constitucional',
          endDate: '2020-12-23 00:00:00.000000'
        }
      ],
      consultingInactive: []
    };
    const geList = jest.fn();
    const setLoading = jest.fn();
    const getLastVideos = jest.fn();
    const getCampaign = jest.fn();
    wrapper = setup({ geList, setLoading, getCampaign, getLastVideos, home, campaign, user, consulting });
  });

  it('should render the connected component', () => {
    const component = findByTestAtrr(wrapper, 'homeComponent');

    expect(component.length).toBe(1);
  });

  it('should renders all parts', () => {
    expect(wrapper).toMatchSnapshot();
  });

});
