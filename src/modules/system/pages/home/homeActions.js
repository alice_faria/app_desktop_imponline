/* eslint-disable no-undefined */
import axios from 'axios';
import types from './homeTypes';

import _ from 'lodash';
import moment from 'moment';

import { BASE_API_MATRIZ, USER_TOKEN, BASE_API_COMERCIAL, URL_LOJA } from 'config/consts';
import { errorHandler } from 'helpers/errorHandler';
import randomImages from 'common/containers/randomImages/randomImages';
import {toastr} from "react-redux-toastr";
import intl from "react-intl-universal";
const {shell} = window.require('electron');

const handleCoursesSubject = (courses) => {
  return courses.map(item => ({
    ...item,
    background: randomImages(item.id_disciplina)
  }));
};

const getSubject = (disciplinas) => {
  return {
    courses: handleCoursesSubject(disciplinas)
  };
};

const handleVideosConsulting = (videos) => {
  return videos.map(video => ({
    id_samba_id: video.id,
    st_samba_title: video.aula,
    bl_favorito: video.bl_favorito
  }));
};

const handleModulesConsulting = (modules) => {
  return modules.map((module, index) => ({
    st_modulopos: index,
    st_modulomatriz: module.modulo,
    st_modulo_pos: index + 1,
    arquivos: null,
    videos: handleVideosConsulting(module.listaAulas)
  }));
};

const handleCoursesConsulting = (courses, name_curso, week, consulting) => {
  return courses.map(course => ({
    id: Math.floor(Math.random() * 20),
    id_matricula: consulting.id_matricula,
    id_projetopedagogico: consulting.id_projetopedagogico,
    st_tituloexibicao: consulting.st_tituloexibicao,
    dt_inicio: consulting.dt_inicio,
    dt_termino: consulting.dt_termino,
    st_coordenador: consulting.st_coordenador,
    st_semana: week,
    course: name_curso,
    id_matriz: course.id_matriz,
    st_disciplina: course.nomeDisciplina,
    modulos: handleModulesConsulting(course.listaModulos),
    st_imagemprofessor: course.professor[0].foto,
    st_nomeprofessor: course.professor[0].nomeProfessor,
    teoria: course.teoria,
    questoes: course.questoes,
    background: randomImages(Math.floor(Math.random() * 20)),
    description: course.aviso
  }));
};

const handleContentByWeeks = (conteudos, consulting) => {
  return conteudos && conteudos.map(content => {
    if (content.simulado === 0) {
      return {
        simulado: false,
        meta: content.meta,
        data: handleCoursesConsulting(content.listaDisciplinas, content.curso, content.meta, consulting),
        date: content.data
      };
    } else {
      return {
        semana: content.semana,
        simulado: true,
        date: content.data,
        url: content.url
      };
    }
  });
};

const getConsulting = (conteudos, consulting) => {
  return {
    courses: handleContentByWeeks(conteudos, consulting)
  };
};

const filterHandle = filter => {
  return filter.map(item => ({
    subject: getSubject(item.matriz.disciplinas),
    _id: item.id_projetopedagogico,
    title: item.st_tituloexibicao,
    endDate: item.dt_termino,
    matricula: item.id_matricula,
    bl_planner : item.matriz.bl_planner
  }));
};

const filterHandleConsulting = filter => {
  return Promise.all(filter.map(async item => ({
    subject: await getConsulting(item.conteudos, item),
    _id: item.id_projetopedagogico,
    id_matricula: item.id_matricula,
    id_projetopedagogico: item.id_projetopedagogico,
    title: item.st_tituloexibicao,
    initialDate: item.dt_inicio,
    endDate: item.dt_termino,
    matricula: item.id_matricula
  })));
};

const getRenovacao = (item)=>{
  return new Promise((resolve)=>{
    let url = null;
    item.map((item)=>{
      item.map((item, index)=>{
        if(url === null){
          url = "?id_matricula[]="+item.matricula;
        }
        url += "&id_matricula[]="+item.matricula;
      })
    });
    if(url){
      axios.post(`${BASE_API_COMERCIAL}/api/imp/verifica-renovacao${url}`, {
        headers: {
          Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
        }
      }).then((response)=>{
        if(response.data.dados.aptos_renovacao){
          item.map((item)=>{
            item.filter((i, index)=>{
              Object.keys(response.data.dados.aptos_renovacao.id_matricula).map((ren)=>{
                if(response.data.dados.aptos_renovacao.id_matricula[i.matricula]){
                  sortRenovacao(response.data.dados.aptos_renovacao.id_matricula[i.matricula]).then((renovacao)=>{
                    i.dadosRenovacao = renovacao;
                  })
                }
              });
            });
          });
          resolve(item);
        }else{
          resolve(item);
        }
      }).catch(()=>{
        toastr.error('Houve um erro ao recuperar as informações de renovação.');
        resolve(item);
      });
    }else{
      resolve(item);
    }
  });
};

const sortRenovacao = (renovacao) => {
  return new Promise(resolve =>{
    let ren = renovacao.sort((a, b) => parseInt(a.nu_dias) < parseInt(b.nu_dias) ? -1 : 1);
    resolve(ren);
  });
}

/**
 * @function getList - Função utilizada para solicitar os projetos pedagógicos
 * do usuário
 * @param {Object} values
 * @returns {Function} Função para pegar os projetos pedagógicos do usuário
 */
export const getList = () => {
  return (dispatch) => {
    return axios.get(`${BASE_API_MATRIZ}/api/matriz`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      //Filtrar cursos ativos
      let filterPPInactive = [];
      let filterPPActive = response.data.filter(value => {
        const today = moment();

        if (_.isNull(value.dt_termino) || today.diff(moment(value.dt_termino)) < 0) {
          return value;
        }
        filterPPInactive.push(value);
        return null;
      });
      Promise.all([
        filterHandle(filterPPActive).sort((a, b) => {
          return a.nu_ordem < b.nu_ordem ? -1 : a.nu_ordem > b.nu_ordem ? 1 : 0;
        }),
        filterHandle(filterPPInactive)]
      ).then(
        res => {
          getRenovacao(res).then((res)=>{
              dispatch({
                type: types.HOME_MYCLASSES_FETCHED,
                payload: {
                  active: res[0],
                  inactive: res[1]
                }
              })
            }
          )
        }
      );
    }).catch((err) => {
      errorHandler(dispatch, err, 'HOME_MYCLASSES_ERROR', types.HOME_LOADING);
    });
  };
};

/**
 * @function getListConsulting - Função utilizada para solicitar os projetos pedagógicos
 * do usuário
 * @param {Object} values
 * @returns {Function} Função para pegar os projetos pedagógicos do usuário
 */
export const getListConsulting = () => {
  return (dispatch) => {
    return axios.get(`${BASE_API_MATRIZ}/api/consultorias`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      //Filtrar cursos ativos
      let filterConsultingInactive = [];
      let filterConsultingActive = response.data.filter(value => {
        const today = moment();
        if (_.isNull(value.dt_termino) || today.diff(moment(value.dt_termino)) < 0) {
          return value;
        }
        filterConsultingInactive.push(value);
        return null;
      });
      Promise.all([
        filterHandleConsulting(filterConsultingActive),
        filterHandleConsulting(filterConsultingInactive)]
      ).then(
        res => {
          getRenovacao(res).then(res=>{
            dispatch({
              type: types.HOME_CONSULTING_FETCHED,
              payload: {
                consultingActive: res[0],
                consultingInactive: res[1]
              }
            })
          })
        }
      );
    }).catch((err) => {
      errorHandler(dispatch, err, 'HOME_CONSULTING_FETCHED', types.HOME_LOADING);
    });
  };
};

/**
 * @function getCampaign - Função utilizada para solicitar as campanhas
 * do usuário
 * @param {Object} id
 * @returns {Function} Função que salva as campanhas no reducer
 */
export const getCampaign = (id) => {
  return (dispatch) => {
    return axios.get(`${BASE_API_MATRIZ}/api/campanhas/${id}`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.HOME_CAMPAIGN_FETCHED,
        payload: response.data
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'HOME_CAMPAIGN_ERROR', types.HOME_LOADING);
    });
  };
};

/**
 * @function getLastVideos - Função utilizada para solicitar os 10 últimos
 * vídeos assistidos pelo usuário
 * @returns {Function} Função que salva os últimos vídeos no reducer
 */
export const getLastVideos = () => {
  return (dispatch) => {
    return axios.get(`${BASE_API_MATRIZ}/api/matriz/videos/periodos?nu_limit=10`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.HOME_LAST_VIDEO_FETCHED,
        payload: response.data
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'HOME_LAST_VIDEO_ERROR', types.HOME_LAST_VIDEO_LOADING);
    });
  };
};

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.HOME_RESET
    });
  };
};

export const resetConsulting = () => {
  return (dispatch) => {
    dispatch({
      type: types.HOME_CONSULTING_FETCHED
    });
  };
};

export const openPlanner = (id_projetopedagogico, id_matricula) =>{
  return (dispatch) => {
    return axios.get(`${BASE_API_MATRIZ}/api/planner/${id_projetopedagogico}/${id_matricula}`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      axios.get(response.data.url_planner[0]).then((rps) => {
        if(rps.data.result === false){
          let err = {
            response:{
              status: 200,
              data:{
                titulo: "Atenção",
                mensagem: "O planner deste curso não está configurado."}}
          };
          return dispatch({
            type: types.HOME_PLANNER_FETCHED,
            payload: errorHandler(dispatch, err, "HOME_PLANNER_FETCHED", types.HOME_LOADING)
          });
        }
        return dispatch({
          type: types.HOME_PLANNER_FETCHED,
          payload: shell.openExternal(response.data.url_planner[0])
        });
      })
    }).catch((err) => {
      errorHandler(dispatch, err, 'HOME_PLANNER_ERROR', types.HOME_LOADING);
    });
  };
};

export const postRenovacao = (id_produto, id_matricula, st_nomecompleto, id_usuario) =>{
  return (dispatch) => {
      shell.openExternal(`${URL_LOJA}/loja/pagamento/iniciar/?produtos[0][id_produto]=${id_produto}&st_codchave=466a1a4f273e36c8e915c4f65dced9421ffbcd46&id_matricula=${id_matricula}&id_usuario=${id_usuario}`);
  };
};
