import types from './homeTypes';

const initialState = {
  myClasses: {
    active: [],
    inactive: []
  },
  myConsulting: {
    consultingActive: [],
    consultingInactive: []
  },
  lastVideos: [],
  campaign: {},
  disconnected: false,
  loadingClasses: true,
  loadingConsulting: true,
  loadingLastVideos: false,
  loading: false
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.HOME_MYCLASSES_FETCHED:
      return {
        ...state,
        myClasses: payload || initialState.myClasses,
        loadingClasses: false,
        loading: state.loadingConsulting
      };
    case types.HOME_CONSULTING_FETCHED:
      return {
        ...state,
        myConsulting: payload,
        loadingConsulting: false,
        loading: state.loadingClasses
      };
    case types.HOME_LAST_VIDEO_FETCHED:
      return {
        ...state,
        lastVideos: payload,
        loadingLastVideos: false
      };
    case types.HOME_CAMPAIGN_FETCHED:
      return {
        ...state,
        campaign: payload
      };
    case types.HOME_LOADING:
      return {
        ...state,
        loading: payload
      };
    case types.HOME_LAST_VIDEO_LOADING:
      return {
        ...state,
        loadingLastVideos: payload
      };
    case types.HOME_PLANNER_FETCHED:
      return {
        ...state,
        loadingPlanner: payload
      };
    case types.HOME_PLANNER_LOADING:
      return {
        ...state,
        loadingPlanner: payload
      };
    case 'RESET_APPLICATION' || types.HOME_RESET:
      return {
        ...state,
        ...initialState
      };
    case 'ERROR_CONNECTION':
      return {
        ...state,
        disconnected: true
      };
    default:
      return state;
  }
};
