import React from "react";
import {Field, Form, reduxForm} from "redux-form";
import {connect} from "react-redux";
import Img from "react-image";
import imgAvatarEmpty from 'assets/images/ic_avatar_empty.png';
import {createTextMask} from "redux-form-input-masks";

let phoneMask = createTextMask({
  pattern: '(99) 99999-9999',
});

let cpfMask = createTextMask({
  pattern: '999.999.999-99',
});

let cepMask = createTextMask({
  pattern: '99999-999',
});


export class FormEndereco extends React.Component {
  constructor(props) {
    super(props);
    this.state={loading:true}
    this.inputRef = React.createRef();
  }

  render(){
    let {handleSubmit, pristine, invalid, cidades, UFs, paises} = this.props;
    return (
      <Form name="formAlteraEnd" id="formAlteraEnd" onSubmit={handleSubmit}>
        <div className="meus-dados__avatar--container">
          <div className='meus-dados__avatar'>
            <Img src={[this.props.initialValues.st_urlavatars3, this.props.initialValues.st_urlavatar, imgAvatarEmpty]} className="meus-dados__avatar--image" alt="user" data-place="right" data-effect="solid"
                 data-tip='Alterar Avatar' onClick={()=>this.inputRef.current.click()}
                 loader={<Img src={[imgAvatarEmpty]} className="meus-dados__avatar--image" alt="user" data-place="bottom" data-effect="solid" data-tip='Carregando..'/>}/>
            <i className="icon-change_photo meus-dados__avatar--icon" onClick={()=>this.inputRef.current.click()}/>
            <input disabled={this.props.loadingImage} accept="image/jpeg, image/png, image/gif" type="file" style={{display:'none'}} ref={this.inputRef} onChange={(event)=> {this.props.uploadFoto(event.target.files[0])}}/>
          </div>
        </div>
        <h3 style={{backgroundColor: '#353535', textAlign: 'center', marginTop: '1rem'}}>Dados Pessoais</h3>
        <span id="helpBlock" className="text-atendimento">Para a alteração de dados cadastrais é necessário a abertura de um atendimento.</span>
        <div className="meus-dados__dadosPessoais">
          <div className="meus-dados__dadosPessoais__group">
            <div className="form-group">
              <label>Nome:</label>
              <Field name="st_nomecompleto" component="input" disabled className="form-control"/>
            </div>
            <div className="form-group">
              <label>Documento de Identificação:
              </label>
              <Field name="st_rg" component="input" disabled className="form-control"/>
            </div>
            <div className="form-group">
              <label>Orgão Emissor:
              </label>
              <Field name="st_orgaoexpeditor" component="input" disabled className="form-control"/>
            </div>
          </div>
          <div className="meus-dados__dadosPessoais__group">
            <div className="form-group">
              <label>E-mail:</label>
              <Field name="st_email" component="input" disabled className="form-control"/>
            </div>
            <div className="form-group">
              <label>CPF:
              </label>
              <Field name="st_cpf" component="input" disabled className="form-control" {...cpfMask}/>
            </div>
          </div>
        </div>

        <div className="meus-dados__flex-column">
          <h3 className="meus-dados__label">Endereço</h3>

          <div style={{justifyContent: 'space-between', flexDirection: 'row', display: 'flex'}}>
            <div className="meus-dados__dadosPessoais__group form-group">
              <label>CEP:</label>
              <Field name="st_cep" component="input" className="form-control" {...cepMask}/>
            </div>
            <div className="meus-dados__dadosPessoais__group form-group">
              <label>Endereço:</label>
              <Field name="st_endereco" component="input" className="form-control"/>
            </div>
          </div>
          <div style={{justifyContent: 'space-between', flexDirection: 'row', display: 'flex'}}>
            <div className="meus-dados__dadosPessoais__group form-group">
              <label>Número:</label>
              <Field name="nu_numero" component="input" className="form-control"/>
            </div>
            <div className="meus-dados__dadosPessoais__group form-group">
              <label>Complemento:</label >
              <Field name="st_complemento" component="input" className="form-control"/>
            </div>
          </div>

          <div className="meus-dados__space-row">
            <div className="meus-dados__dadosPessoais__group">
              <label>UF:</label>
              <Field component="select" name="sg_uf" className="form-control"
                      required="required" onChange={(event)=>this.props.getCidades(event.target.value)}>
                {UFs.mensagem.map((item, i) => {
                    return (
                      <option value={item.sg_uf} key={i}>{item.st_uf}</option>
                    )})}
              </Field>
            </div>
            <div className="meus-dados__dadosPessoais__group">
              <label>Cidade:</label>
              <Field component="select" name="st_cidade" className="form-control"
                     required="required">
                {cidades.map((item, i) => {
                    return (
                      <option value={item.st_nomemunicipio} key={i}>{item.st_nomemunicipio}</option>
                    )})}
              </Field>
            </div>
          </div>

          <div className="meus-dados__space-row">
            <div className="meus-dados__dadosPessoais__group">
              <label>Bairro:</label>
              <Field name="st_bairro" component="input" className="form-control"/>
            </div>
            <div className="meus-dados__dadosPessoais__group">
              <label>País:</label>
              <Field component="select" name="st_nomepais" className="form-control"
                     required="required">
                {paises.map((item, i) => {
                  return (
                    <option value={item.st_nomepais} key={i}>{item.st_nomepais}</option>
                  )})}
              </Field>
            </div>
          </div>
          <div className="meus-dados__dadosPessoais__group form-group">
            <label>Telefone:</label>
            <Field name="nu_telefone" component="input" className="form-control" {...phoneMask}/>
          </div>
        </div>
        <div className="form-group pull-right" style={{paddingTop:'1rem'}}>
          <button disabled={pristine || invalid} type="submit"
                  className="btn btn-pinterest pull-right">
            Alterar
          </button>
        </div>
      </Form>
    );
    }
  }

FormEndereco = reduxForm({form: 'formEndereco', keepDirtyOnReinitialize :true,  enableReinitialize: true} )(FormEndereco);

const mapStateToProps = state => {
  return {
    initialValues: state.dados.meusDados,
    user: state.auth.user,
    cidades: state.dados.cidades,
    UFs: state.dados.UFs,
    paises: state.dados.paises,
    loadingImage: state.dados.loadingImage
  };
};

export default connect(
  mapStateToProps
)(FormEndereco);
