/* eslint-disable no-undefined */
import axios from 'axios';
import types from './dadosTypes';

import { BASE_API, USER_TOKEN } from 'config/consts';
import { errorHandler } from 'helpers/errorHandler';
import {toastr} from "react-redux-toastr";

const getUfs = () =>{
    return axios.get(`${BASE_API}/api/endereco/uf`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response)=>{
      return(response);
    })
}

const getPaises = () =>{
  return axios.get(`${BASE_API}/api/endereco/pais`, {
    headers: {
      Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
    }
  }).then((response)=>{
    return(response);
  })
}

const getCidadesFirst = (sg_uf) =>{
  return axios.post(`${BASE_API}/api/endereco/municipio?sg_uf=${sg_uf}`, {},{
    headers: {
      Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
    }
  }).then((response)=>{
    return(response);
  })
}

export const getDados = () => {
  return (dispatch) => {
      return axios.get(`${BASE_API}/api/pessoa/meus-dados`, {
        headers: {
          Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
        }
      }).then((response) => {
        getUfs().then((responseUfs)=>{
          getPaises().then((responsePaises)=>{
            getCidadesFirst(response.data.sg_uf).then((responseCidades)=>{
              dispatch({
                type: 'DADOS_IMAGEM_FIRST',
                payload: response
              });
              dispatch({
                type: types.DADOS_FETCHED,
                payload: {dados: response.data, UFs:responseUfs.data, paises: responsePaises.data.mensagem, cidades: responseCidades.data}
              });
            })
          })
        })
      }).catch((err) => {
        errorHandler(dispatch, err, 'DADOS_ERROR', types.DADOS_ERROR);
      });
    }
};

export const getCidades = (sg_uf) =>{
    return(dispatch)=>{
      return axios.post(`${BASE_API}/api/endereco/municipio?sg_uf=${sg_uf}`, {},
        {
          headers: {
            Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
          }
        }
      ).then((response)=>{
        dispatch({
          type: types.DADOS_CIDADE,
          payload: response
        })
      })
    }
}

export const uploadFoto = (data) =>{
  let dt = new FormData();
  dt.append('usuario_foto', data, data.name);
  return (dispatch) =>{
    return axios.post(`${BASE_API}/api/usuario/uploadFoto`, dt,
      {
        headers: {
          Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
        }
      }).then((response)=>{
      toastr.success('Sucesso', 'Imagem enviada com sucesso')
        axios.get(`${BASE_API}/api/pessoa/meus-dados`, {
          headers: {
            Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
          }
        }).then((response) => {
          dispatch({
            type: types.DADOS_IMAGEM,
            payload: response
          })
        }).catch((err)=>{
        toastr.error('Houve um erro ao carregar a imagem')
        })
    }).catch((err)=>{
      toastr.error('Houve um erro ao salvar a imagem')
    })
  }
}

export const redefinirSenha = (id_usuario, id_entidade, st_senha) =>{
  return (dispatch) =>{
    return axios.post(`${BASE_API}/api/usuario/redefinirSenha`,
      {id_usuario: id_usuario,id_entidade: id_entidade,st_senha: st_senha},
      {
        headers: {
          Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
        }
      }
    ).then((response)=>{
      toastr.success('Sucesso', 'Senha salva com sucesso')
    }).catch((error)=>{
      toastr.error('Houve um erro ao salvar a senha')
    })
  }
}

export const enviarDados = (dados) => {
  return (dispatch) =>{
    return axios.post(`${BASE_API}/api/usuario/atualizarDadosEndereco`, {id_usuario: dados.id_usuario,id_entidade: dados.id_entidade, id_telefone:dados.id_telefone,
      telefone: dados.nu_telefone, st_cep: dados.st_cep, st_endereco: dados.st_endereco, nu_numero: dados.nu_numero, st_complemento: dados.st_complemento, st_bairro: dados.st_bairro,
      sg_uf: dados.sg_uf, st_cidade: dados.st_cidade}, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
        }
      }
    ).then((response)=>{
      toastr.success('Sucesso','Dados enviados com sucesso')
    }).catch((error)=>{
      toastr.error('Houve um erro ao salvar os dados')
    })
  }
}

export const reset = () =>{
  return (dispatch) => {
    dispatch({
      type: types.DADOS_RESET
    });
  };
}
