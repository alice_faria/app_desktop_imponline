/* eslint-disable no-fallthrough */
import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import { setLoading } from 'helpers/redux';
import moment from 'moment';
import 'moment/locale/pt-br';
import {getDados, enviarDados, redefinirSenha, uploadFoto, getCidades, reset} from "./dadosActions";
import imageCompression from 'browser-image-compression';

import Loading from 'common/components/ui/loading/loading';
import FormEndereco from "./formEndereco";
import FormSenha from "./formSenha";
import disconnected from "../../../../assets/images/img_internet.png";
import intl from "react-intl-universal";
import types from './dadosTypes'

moment.locale('pt-br');

export class Dados extends Component {
  constructor(props) {
    super(props);
    if(!this.props.meusDados){
      this.props.getDados();
    }
    this.Ref = React.createRef();
  }

  submitEndereco = (values) => {
    this.props.enviarDados(values);
  }

  submitSenha = (values) =>{
    console.log(values);
    this.props.redefinirSenha(this.props.user.id_usuario, this.props.user.id_entidade, values.senha);
  }

  openInput = () => {
    this.Ref.current.click();
  }

  getCidades = (sg_uf) =>{
    this.props.getCidades(sg_uf);
  }

  uploadFoto = (file) =>{
    let options = {
      maxSizeMB: 1,
      useWebWorker: true
    }
    if(file){
      imageCompression(file, options).then((compressedFile)=>{
        console.log(compressedFile)
        this.props.setLoading('DADOS_IMAGEM_LOADING', true);
        this.props.uploadFoto(compressedFile)
      })
    }
  }

  reset = () => {
    this.props.reset();
    setTimeout(() => {
      this.props.setLoading(types.DADOS_LOADING, true);
      this.props.getDados();
    }, 500);
  }

  render() {
    if (this.props.loading) {
      return <Loading isVisible={this.props.loading} overlay={true} />;
    }else if (this.props.disconnected) {
      return (
        <div className="emptyHome">
          <img src={disconnected} className="emptyHome__img" alt="empty" />
          <span className="emptyHome__title">{intl.getHTML('errorRequest.connection')}</span>
        </div>
      );
    }
    return (
    <div className="meus-dados">
      <div className="meus-dados__header">
        <span className="meus-dados__header--title">Meus Dados</span>
        <i className="icon-refresh meus-dados__header--icon" onClick={() => this.reset()}/>
      </div>
      <div className="meus-dados__body">
        <FormEndereco onSubmit={this.submitEndereco} openInput={this.openInput} Ref={this.Ref} uploadFoto={this.uploadFoto} getCidades={this.getCidades}/>
        {/*<span id="helpBlock" className="col-md-6">Para a alteração de dados cadastrais é necessário a abertura de um atendimento.</span>*/}
        <FormSenha onSubmit={this.submitSenha}/>
      </div>
    </div>);
  }

}

const mapStateToProps = state => {
  return {
    loading: state.dados.loading,
    user: state.auth.user,
    meusDados: state.dados.meusDados,
    disconnected: state.dados.disconnected
  };
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      getDados,
      enviarDados,
      redefinirSenha,
      uploadFoto,
      setLoading,
      getCidades,
      reset
    },
    dispatch
  )
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Dados));
