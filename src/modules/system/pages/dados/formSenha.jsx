import React from "react";
import {Field, Form, reduxForm} from "redux-form";

const validate = values => {

  const errors = {};
  if(values.senha !== values.senha2){
    errors.senha = 'As senhas são diferentes'
  }
  return errors;
}
const renderField = ({
                       input,
                       label,
                       type,
                       meta: { touched, error, warning },
  className
                     }) => (
  <div className="form-group meus-dados__flex-column meus-dados__group">
    <label>{label}</label>
      <input {...input} type={type} className={className}/>

    {touched &&
    (error && <span style={{color: '#cc0000'}}>{error}</span>)
     }
  </div>
)

let FormSenha = props =>{
  const { handleSubmit , pristine, invalid} = props;
  return(
    <Form className="form-senha" name="formAlteraSenha" id="formAlteraSenha" onSubmit={handleSubmit}>
      <h3 className="meus-dados__label">Alterar Senha</h3>
      <div className="meus-dados__space-row">
        <Field component={renderField} label="Nova Senha" className="form-control" name="senha" type="password" required />
        <Field component={renderField} label="Confirmar Senha" className="form-control" name="senha2" type="password" required/>
      </div>
      <div className="form-group pull-right">
        <button disabled={pristine || invalid} type="submit" className="btn btn-pinterest pull-right">
          Alterar
        </button>
      </div>
    </Form>

  );
}
FormSenha = reduxForm({form: 'formSenha',validate} )(FormSenha);

export default FormSenha;
