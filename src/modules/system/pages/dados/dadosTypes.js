export default {
  DADOS_FETCHED: 'DADOS_FETCHED',
  DADOS_CIDADE: 'DADOS_CIDADE',
  DADOS_CHANGED: 'DADOS_CHANGED',
  DADOS_RESET: 'DADOS_RESET',
  DADOS_ERROR: 'DADOS_ERROR',
  DADOS_IMAGEM: 'DADOS_IMAGEM',
  DADOS_IMAGEM_LOADING: 'DADOS_IMAGEM_LOADING',
  DADOS_LOADING: 'DADOS_LOADING'
};
