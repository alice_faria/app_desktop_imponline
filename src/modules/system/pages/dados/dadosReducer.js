import types from './dadosTypes';
import update from 'immutability-helper';

const initialState = {
  loading: true,
  loadingImage: false,
  disconnect: false
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.DADOS_FETCHED:
      payload.dados.nu_telefone = payload.dados.nu_ddd+payload.dados.nu_telefone
      return{
        loading: false,
        meusDados: payload.dados,
        UFs: payload.UFs,
        paises: payload.paises,
        cidades: payload.cidades
      };
    case types.DADOS_CIDADE:
      return update(state, {
        meusDados: {
          st_nomemunicipio: {$set:payload.data[0].st_nomemunicipio}
        },
        cidades: {
          $set:payload.data
        }
      });

    case 'DADOS_IMAGEM':
      return update(state, {
        meusDados: {
          st_urlavatars3: {$set:payload.data.st_urlavatars3}
        },
        loadingImage: {
          $set:false
        }
      });
    case types.DADOS_IMAGEM_LOADING:
      return{
        ...state,
        loadingImage: true
      }
    case types.DADOS_RESET:
      return update(state, {
        meusDados: {$set: null},
        loading: {$set: true}
      });
    case 'LOGOUT':
      return update(state, {
        meusDados: { $set: null },
        loading: { $set: true }
      });
    case 'ERROR_CONNECTION':
      return {
        ...state,
        disconnected: true
      };
    default:
      return state;
  }
};
