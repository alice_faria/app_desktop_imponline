/* eslint-disable no-undefined */
import axios from 'axios';
import types from './downloadsTypes';

import { BASE_API, USER_TOKEN } from 'config/consts';
import { errorHandler } from 'helpers/errorHandler';

/**
 * @function getList - Função utilizada para solicitar os projetos pedagógicos
 * do usuário
 * @param {Object} values
 * @returns {Function} Função para pegar os projetos pedagógicos do usuário
 */
export const getList = () => {
  return (dispatch) => {
    return axios.get(`${BASE_API}/api/usuario/acessos?bl_alunos=1&bl_consultoria=0`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.DOWNLOADS_FETCHED,
        payload: response.data
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'DOWNLOADS_MYCLASSES_ERROR', types.DOWNLOADS_LOADING);
    });
  };
};
