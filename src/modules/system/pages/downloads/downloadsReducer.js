import types from './downloadsTypes';

const initialState = {
  downloads: [],
  loading: false
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.DOWNLOADS_FETCHED:
      return {
        ...state,
        downloads: payload || initialState.downloads,
        loading: false
      };
    case types.DOWNLOADS_LOADING:
      return {
        ...state,
        loading: payload || initialState.loading
      };
    case 'RESET_APPLICATION':
      return {
        ...state,
        ...initialState
      };
    default:
      return state;
  }
};
