/* eslint-disable no-fallthrough */
import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import { setLoading } from 'helpers/redux';
import moment from 'moment';
import 'moment/locale/pt-br';

import intl from 'react-intl-universal';
import ReactTooltip from 'react-tooltip';

import Loading from 'common/components/ui/loading/loading';
import { getList } from './downloadsActions';
import _ from 'lodash';
import Carousel from 'common/components/ui/carousel/carousel';
import emptyDownloads from 'assets/images/empty__curso.png';
import types from './downloadsTypes';
import Switch from 'react-switch';
import imgTeacherEmpty from 'assets/images/imgMockCourse.png';
import Img from 'react-image';
import VideoList from 'common/containers/VideoList/VideoList';
import ModuleList from 'common/containers/ModuleList/ModuleList';

moment.locale('pt-br');

export class Downloads extends Component {
  constructor(props) {
    super(props);
    this.props.setLoading(types.DOWNLOADS_LOADING, true);
    this.props.getList();
    this.state = {
      showSidebar: false,
      player: false,
      openModules: true,
      updated: false,
      courseTitle: '',
      courseModule: 0,
      courseModulePlaying: null,
      courseClass: 0,
      showModules: false,
      checked: false,
      course: {
        course: '',
        image: '',
        modules: [],
        teacher: ''
      },
      projectHash: null,
      videoTitle: ''
    };
  }

  toggleActived = (toggle) => {
    this.setState({ showActived: toggle });
  }

  modulesList = (modules) => (
    <ModuleList
      modules={modules}
      onClick={(index) => this.changeModule(index)}
      courseModule={this.state.courseModule}
    />
  )

  handleChange(checked) {
    this.setState({ checked });
  }

  videosList = (modules) => (
    <VideoList
      modules={modules}
      onClick={(item, index) => this.setState({
        player: true,
        courseModulePlaying: this.state.courseModule,
        courseClass: index, videoTitle: item.st_samba_title.trim()
      }, () => this.loadVideo(item.id_samba_id))}
      courseModule={this.state.courseModule}
      courseModulePlaying={this.state.courseModulePlaying}
      courseClass={this.state.courseClass}
    />
  )

  selectDownload = (item, data) => {
    this.setState({
      courseTitle: (item.st_disciplina).substring(0, (item.st_disciplina).indexOf('_')),
      course: {
        course: data.st_tituloexibicao,
        modules: item.modulos,
        image: item.st_imagemprofessor,
        teacher: item.st_nomeprofessor
      },
      showSidebar: true,
      projectHash: data.st_projecthash
    });
  }

  renderCourses = () => {
    const { downloads } = this.props;
    let listCourses = [];
    for (const item of downloads) {
      const course = (
        <li className="courses" key={item.id_projetopedagogico} id="carousel">
          <div className="courses-header">
            <span className="courses__title">{item.st_tituloexibicao}</span>
          </div>
          <div className="courses__divider"></div>
          <div className="courses__body">
            <Carousel
              items={item.disciplinas}
              id={item.id_projetopedagogico}
              numberItems={0}
              animationDuration={1000}
              renderItems={true}
              classNameNext="courses__carousel--button-next"
              classNamePrev="courses__carousel--button-prev"
              infinite={false}
              slideClass="carousel__slide"
              autoPlay={false}
              onClick={(data) => this.selectDownload(data, item)}
            />
          </div>
        </li>

      );
      if (!_.isUndefined(item.disciplinas)) {
        listCourses = listCourses.concat(course);
      }
    }
    return listCourses;
  }

  handleModulesSidebar = () => {
    this.setState({
      openModules: !this.state.openModules
    });
  }

  changeModule = (index) => {
    this.setState({
      courseModule: index
    }, this.handleAccordion);
  }

  handleAccordion = () => {
    this.setState({
      showModules: !this.state.showModules
    });
  }

  sidebar = (title,) => (
    <React.Fragment>
      <div className="modules-control" onClick={() => this.handleModulesSidebar()}>
        <div className="modules-control__button">
          <i id="control-button" className={`icon-${this.state.openModules ? 'right-arrow' : 'left-arrow'} modules-control__icon`} />
        </div>
      </div>
      <div className={`modules modules__${this.state.openModules ? 'open' : 'close'}`}>
        <div className="modules__course-data">
          <Img src={[this.state.course.image, imgTeacherEmpty]} className="modules__course-data--image" alt="professor" />
          <div className="modules__course-data--info">
            <span className="modules__course-data--info-course">{this.state.course.course}</span>
            <span className="modules__course-data--info-class">{title}</span>
            <span className="modules__course-data--info-teacher">{this.state.course.teacher}</span>
          </div>
        </div>
        <div className="modules__course-footer">
          <div className="modules__course-footer-group">
            <i className="icon-comment2 modules__course-footer--icon-chat" />
            <span className="modules__course-footer--title">{intl.get('course.help')}</span>
          </div>
          <div className="vertical-divider modules__divider"></div>
          <div className="modules__course-footer-group">
            <i className="icon-star modules__course-footer--icon" />
            <span className="modules__course-footer--title">{intl.get('course.rating')}</span>
          </div>
        </div>
        <div className="modules__list">
          <div className="modules__list--item" onClick={() => this.handleAccordion()}>
            <div className="modules__list--item-number">{'Módulo ' + this.state.course.modules[this.state.courseModule].st_modulo_pos.split(' ')[1]}</div>
            <div className="modules__list--item-group">
              <div className="modules__list--item-title">{this.state.course.modules[this.state.courseModule].st_modulomatriz.trim()}</div>
              <i className={`icon-${!this.state.showModules ? 'down-arrow' : 'up-arrow'} modules__list--item-icon${this.state.showModules ? '-open' : ''}`} />
            </div>
          </div>
          <div className="modules__list--content">
            <div className={`modules__list--accordion modules__list--accordion${this.state.showModules ? '-open' : '-close'}`}>
              {this.state.showModules
                ? this.modulesList(this.state.course.modules)
                : null}
            </div>
          </div>
        </div>
        <div className="videos">
          <div className="modules__list--resume">
            <div className="modules__list--resume-counter">
              <i className="icon-video_play modules__list--resume-counter-icon" />{this.state.course.modules[this.state.courseModule].videos.length} vídeos
            </div>
            <div className="modules__list--resume-outlet">
              <span className="modules__list--resume-outlet-title">Baixar módulo</span>
              <Switch
                onChange={this.handleChange}
                checked={this.state.checked}
                onColor="#48d852"
                onHandleColor="#FFF"
                offColor="#1f1f1f"
                offHandleColor="#FFF"
                handleDiameter={20}
                uncheckedIcon={false}
                checkedIcon={false}
                boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                height={20}
                width={35}
              />
            </div>
          </div>
          <div className="videos__list">
            {this.videosList(this.state.course.modules)}
          </div>
        </div>
      </div>
    </React.Fragment>
  )

  render() {
    if (this.props.loading) {
      return <Loading isVisible={this.props.loading} overlay={true} />;
    } else if (_.isEmpty(this.props.downloads)) {
      return (
        <div className="emptyDownloads">
          <img src={emptyDownloads} className="emptyDownloads__img" alt="empty" />
          <span className="emptyDownloads__title">{intl.getHTML('downloads.iconInative')}</span>
        </div>
      );
    }
    const title = this.state.courseTitle;
    const courseClass = this.state.courseClass + 1;
    return (
      <section style={{ display: 'flex', flexDirection: 'row' }}>
        <ReactTooltip />
        <div className="downloads" data-test='downloadsComponent'>
          <div className="downloads__header">
            <span className="downloads__header--title">{intl.get('downloads.title')}</span>
          </div>
          {this.state.player
            ? <div className="course__body">
              <div id="playerST" className="course__body--player"></div>
              <div className="course__footer">
                <span className="course__footer--title">{this.state.videoTitle} <i className="icon-bookmark course__footer--icon" /></span>
                <div className="course__footer--tags">
                  <div className="course__footer--tags-module">{'Módulo ' + this.state.course.modules[this.state.courseModulePlaying].st_modulo_pos.split(' ')[1]}</div>
                  <div className="course__footer--tags-class">{intl.get('course.class')} {courseClass}</div>
                </div>
                <div className="course__footer--divider"></div>
                <div className="course__footer--icons">
                  <div className="course__footer--icons-actions">
                    <i className="icon-course course__icons" />
                    <i className="icon-file-check course__icons" />
                    <i className="icon-presentation course__icons" />
                    <i className="icon-headphones course__icons" />
                    <i className="icon-comment2 course__icons" />
                  </div>
                  <i className="icon-download-button course__icons" />
                </div>
              </div>
            </div>
            : <div className="downloads__body">
              <ul className="downloads__body--courses">
                {this.renderCourses()}
              </ul>
            </div>
          }
          <div className="modules-control" onClick={() => this.handleModulesSidebar()}>
            <div className="modules-control__button">
              <i id="control-button" className={`icon-${this.state.openModules ? 'right-arrow' : 'left-arrow'} modules-control__icon`} />
            </div>
          </div>
        </div>
        {!this.state.showSidebar
          ? null
          : this.sidebar(title)
        }
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.downloads.loading,
    downloads: state.downloads.downloads
  };
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      setLoading,
      getList
    },
    dispatch
  )
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Downloads));
