import types from './courseTypes';

const initialState = {
  data: {
    title: '',
    projectHash: '',
    image: '',
    teacher: '',
    course: '',
    courseID: null,
    modules: []
  },
  questions: [],
  rating: false,
  disconnected: false,
  loading: false,
  loadingRating: false,
  courseSelected: {},
  playing: false
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.COURSE_FETCHED:
      console.log(payload)
      return {
        ...state,
        data: payload || initialState.data,
        loading: false,
        videoPlaying: payload.modules[0].videos[0].id_samba_id
      };
    case types.COURSE_LOADING:
      return {
        ...state,
        loading: payload || initialState.loading
      };
    case 'ERROR_CONNECTION':
      return {
        ...state,
        disconnected: true
      };
    case types.COURSE_QUESTIONS_FETCHED:
      return {
        ...state,
        questions: payload || initialState.questions
      };
    case types.COURSE_SELECTED_FETCHED:
      return {
        ...state,
        courseSelected: payload || initialState.questions
      };
    case types.COURSE_RATING_FETCHED:
      return {
        ...state,
        rating: payload,
        loadingRating: false
      };
    case types.COURSE_RATING_LOADING:
      return {
        ...state,
        loadingRating: payload
      };
    case types.COURSE_EASY_RESET:
      return {
        ...state,
        ...initialState,
        loading: true
      };
    case types.COURSE_RESET || 'RESET_APPLICATION':
      return {
        ...state,
        ...initialState
      };
    default:
      return state;
  }
};
