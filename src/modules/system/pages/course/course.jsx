/* eslint-disable no-fallthrough */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
// import Switch from 'react-switch';
import intl from 'react-intl-universal';
import ReactTooltip from 'react-tooltip';
import _ from 'lodash';
import Img from 'react-image';
import produce from 'immer';

import { setLoading } from 'helpers/redux';
import Loading from 'common/components/ui/loading/loading';
import { getList, reset, getQuestions, rating, setFavorite, unsetFavorite } from './courseActions';
import { getList as getListMessage, getMessages, sendMessage, cleanMessages, reset as resetMessages } from 'modules/system/pages/messages/messagesActions';
import types from './courseTypes';
import imgTeacherEmpty from 'assets/images/imgMockCourse.png';
import VideoList from 'common/containers/VideoList/VideoList';
import { getFiles, saveProgress } from 'common/containers/containersActions';
import ModuleList from 'common/containers/ModuleList/ModuleList';
import DefaultModal from 'common/components/ui/modals/defaultModal/defaultModal';
import MessageModal from 'common/components/ui/modals/messageModal/messageModal';
import VideoFilesBottom from '../../../../common/containers/videoFiles/videoFilesBottom';

export class Course extends Component {
  constructor(props) {
    super(props);
    this.verifyData();
    this.state = {
      showActived: true,
      playerLoading: false,
      playerStart: false,
      playerFinish: false,
      openModules: true,
      updated: false,
      courseTitle: '',
      courseModule: 0,
      courseModulePlaying: 0,
      courseClass: 0,
      lastCourseClass: null,
      lastCourseModule: null,
      classPlayingFavorited: false,
      courseModulePos: 1,
      showModules: false,
      showModalMessage: false,
      checked: false,
      showModal: false,
      videoProgress: null,
      videoTotal: null,
      reloaded: false,
      videoEvent: null,
      closeFiles: true,
      loadingFiles: false
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handlePositionClass = (videoData, modules) => {
    // console.log(modules)
    let classSelect = modules.map((item, index) => {
      const videoValid = item.videos.filter(subitem => {
        if (subitem.id_samba_id === videoData.video_id) {
          return subitem;
        }
        return null;
      });
      if (!_.isEmpty(videoValid)) {
        return { ...item, videos: videoValid };
      }
      return null;
    });
    classSelect = classSelect.filter(n => n);
    if (!_.isEmpty(classSelect)) {
      this.setState({
        courseModulePlaying: parseInt(classSelect[0].st_modulo_pos) - 1,
        courseModule: parseInt(classSelect[0].st_modulo_pos) - 1,
        courseClass: classSelect[0].videos[0].st_aulapos - 1,
        courseTitle: videoData.video_title,
        reloaded: true
      },
      () =>
        this.loadVideo(videoData.video_id, null, videoData.video_time)
      );
    } else {
      this.loadVideo();
    }
  }

  componentDidMount = () => {
    if (!window.SambaPlayer) {
      console.log('criando o script');
      const tag = document.createElement('script');
      tag.src = 'https://player.sambatech.com.br/v3/samba.player.api.js';
      // SambaPlayer will load the video after the script is loaded
      window.SambaPlayer = this.loadVideo;

      const firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    }
  }

  componentDidUpdate(prevProps) {
    if (!_.isEmpty(this.props.course.modules) && !this.state.reloaded && !this.props.loading) {
      if (!_.isEmpty(this.props.videoToOpen)) {
        this.handlePositionClass(this.props.videoToOpen, this.props.course.modules);
      } else {
        this.loadVideo();
      }
    }
  }

  componentWillUnmount() {
    if (!_.isEmpty(this.props.videoToOpen)) {
      return null;
    }
    const { course } = this.props;
    if (!_.isNull(this.state.progress) && !_.isNull(this.state.videoTotal)) {
      const video_id = course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].id_samba_id;
      const matricula_id = this.props.user.id_matricula;
      const modulomatriz_id = course.modules[this.state.courseModulePlaying].id_modulomatriz;
      const progress = this.state.videoProgress;
      let finished;
      if (this.state.videoTotal - this.state.progress < 10) {
        finished = 1;
      } else {
        finished = 0;
      }
      this.props.saveProgress(video_id, matricula_id, modulomatriz_id, progress, finished);
    }
    clearInterval(this.props.eventVideo);
    this.props.reset();
    this.props.resetMessages();
    return undefined;
  }

  verifyData = () => {
    const { id } = this.props.match.params;
    this.props.setLoading(types.COURSE_LOADING, true);
    this.props.getQuestions();
    this.props.getList(id);
  }

  showModal = () => {
    this.setState({ showModal: true });
  }

  hideModal = () => {
    this.setState({ showModal: false });
  }

  showModalMessage = () => {
    this.setState({ showModalMessage: true });
  }

  hideModalMessage = () => {
    this.setState({ showModalMessage: false });
  }

  autoplay() {
    const autoplay = JSON.parse(localStorage.getItem('AUTOPLAY')) || false;
    const { course } = this.props;
    const video_id = course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].id_samba_id;
    const matricula_id = this.props.user.id_matricula;
    const modulomatriz_id = course.modules[this.state.courseModulePlaying].id_modulomatriz;
    const progress = this.state.videoProgress;

    if(autoplay){
      if (!_.isNull(this.state.progress) && !_.isNull(this.state.videoTotal)) {
        const finished = 1;
        this.props.saveProgress(video_id, matricula_id, modulomatriz_id, progress, finished);
      }
      if(course.modules[this.state.courseModulePlaying].videos.length > this.state.courseClass+1){
        clearInterval(this.state.videoEvent);
        return this.setState({
          courseClass: this.state.courseClass+1,
          videoPlaying: course.modules[this.state.courseModulePlaying].videos[this.state.courseClass+1].id_samba_id,
          closeFiles: true,
          loadingFiles: false
        },
        () => this.loadVideo(
          course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].id_samba_id,
          null,
          0)
        );
      }
      if(course.modules[this.state.courseModulePlaying].videos.length === this.state.courseClass + 1){
        clearInterval(this.state.videoEvent);
        if(course.modules.length >= this.state.courseModulePlaying + 1){
          return this.setState({
              courseModulePlaying: this.state.courseModule + 1,
              courseModule: this.state.courseModule + 1,
              courseClass: 0,
              videoPlaying: course.modules[this.state.courseModule+1].videos[0].id_samba_id,
              closeFiles: true,
              loadingFiles: false
            },
            () => this.loadVideo(
              course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].id_samba_id,
              null,
              course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].nu_segundos)
          );
        }
        return this.setState({
          closeFiles: true,
          loadingFiles: false});
      }
    }
  }

  changeModule = (index, module_pos) => {
    this.setState({
      courseModule: index,
      courseModulePos: module_pos
    }, this.handleAccordion);
  }

  handleAccordion = () => {
    this.setState({
      showModules: !this.state.showModules
    });
  }

  modulesList = (modules) => (
    <ModuleList
      modules={modules}
      onClick={(index, module_pos) => this.changeModule(index, module_pos)}
      courseModule={this.state.courseModule}
    />
  )

  handleFiles = (id) => {
    this.props.getFiles(id);
  }

  videosList = (modules) => (
    <VideoList
      modules={modules}
      onClick={(item, index) => this.setState({
        lastCourseClass: this.state.courseClass,
        lastCourseModule: this.state.courseModulePlaying,
        courseModulePlaying: this.state.courseModule,
        courseClass: index,
        courseTitle: item.st_samba_title,
        videoPlaying: item.id_samba_id
      }, () => this.loadVideo(item.id_samba_id, null, item.nu_segundos))}
      loading={this.props.loadingFiles}
      handleFiles={this.handleFiles}
      files={this.props.files}
      courseModule={this.state.courseModule}
      courseModulePos={this.state.courseModulePos}
      courseModulePlaying={this.state.courseModulePlaying}
      courseClass={this.state.courseClass}
      id_projetopedagogico={this.props.course.projetoPedagogico}
      id_disciplina={this.props.course.courseID}
      course={this.props.course}
      setFavorite={this.props.setFavorite}
      unsetFavorite={this.props.unsetFavorite}
      seconds={this.state.videoProgress}
      type={types.COURSE_FETCHED}
      videoPlaying={this.state.videoPlaying || this.props.videoPlaying}
    />
  )

  handleFavorited = (item, itemFav) => {
    const values = {
      st_aulapos: `${this.state.courseClass + 1}`,
      id_disciplina: this.props.course.courseID,
      st_modulopos: `${this.state.courseModulePlaying + 1}`,
      id_samba_id: `${item.id_samba_id}`,
      st_samba_title: item.st_samba_title,
      id_projetopedagogico: this.props.course.projetoPedagogico,
      id_matriz: this.props.course.modules[this.state.courseModulePlaying].id_matriz
    };

    const valuesUnset = {
      id_disciplina: this.props.course.courseID,
      id_samba_id: `${item.id_samba_id}`,
      id_projetopedagogico: this.props.course.projetoPedagogico,
      id_matriz: this.props.course.modules[this.state.courseModulePlaying].id_matriz
    };
    const data = produce(this.props.course, draftState => {
      draftState.modules[this.state.courseModulePlaying].videos[this.state.courseClass].bl_favorito = !itemFav;
    });
    if (itemFav) {
      this.props.unsetFavorite(data, valuesUnset, types.COURSE_FETCHED);
    } else {
      this.props.setFavorite(data, values, types.COURSE_FETCHED);
    }
  };

  archiveTypeHandler = (archive) => {
    const type = archive.split('.')[archive.split('.').length - 1];
    switch (type) {
      case 'pdf':
        return 'PDF';
      case 'mp3':
        return 'Áudio';
      case 'ppt':
        return 'Slides';
      default:
        return null;
    }
  }

  archiveNameHandler = (archive) => {
    let name = archive.split('/')[archive.split('/').length - 1];
    name = decodeURI(name);
    return name;
  }

  archivesList = (modules) => {
    const { courseModule } = this.state;
    return modules[courseModule].arquivos.map((item, index) => (
      <div className="archives__list--item" key={index}>
        <div className="archives__list--item-type"><i className="icon-course archives__list--item-icon" /></div>
        <div className="archives__list--item-info">
          <div className="archives__list--item-tag">
            <div className="archives__list--item-tag-type">{this.archiveTypeHandler(item)}</div>
            <a target="_blank" rel="noopener noreferrer" href={item} ><i className="icon-download archives__list--item-tag-download" /></a></div>
          <div className="archives__list--item-name">{this.archiveNameHandler(item)}</div>
        </div>
      </div>
    ));
  }

  handleModulesSidebar = () => {
    this.setState({
      openModules: !this.state.openModules
    });
  }

  handleChange(checked) {
    this.setState({ checked });
  }

  onProgress = (value) => {
    return this.setState({ videoProgress: value.eventParam, videoTotal: value.duration });
  }

  handleProgressVideo = () => {
    const { course } = this.props;
    const video_id_before = course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].id_samba_id;
    const matricula_id = this.props.user.id_matricula;
    const modulomatriz_id = course.modules[this.state.courseModulePlaying].id_modulomatriz;
    const progress = parseInt(this.state.videoProgress);
    let finished;
    if (!_.isNull(this.state.videoProgress) && !_.isNull(this.state.videoTotal)) {
      if (this.state.videoTotal - this.state.progress < 10) {
        finished = 1;
      } else {
        finished = 0;
      }
      const data = produce(this.props.course, draftState => {
        draftState.modules[this.state.lastCourseModule].videos[this.state.lastCourseClass].nu_segundos = parseInt(this.state.videoProgress);
      });
      this.setState({ videoTotal: null, videoProgress: null }, () => this.props.saveProgress(video_id_before, matricula_id, modulomatriz_id, progress, finished, data));
    }
  }

  loadVideo = (video_id = null, projectHash = null, resume = null) => {
    const { course } = this.props;
    if (document.getElementsByTagName('iframe').length > 0) {
      if (_.isNull(video_id) || _.isNull(this.state.lastCourseModule)) {
        return null;
      } else {
        this.handleProgressVideo();
        const videoFrame = document.getElementById('playerST');
        const child = videoFrame.firstChild;
        videoFrame.removeChild(child);
      }
    }

    this.player = new window.SambaPlayer('playerST', { //player é o ID do elemento html que ele vai inserir o iframe
      ph: projectHash || course.projectHash,//Player Hash do projeto
      m: video_id || course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].id_samba_id,//MidiaID
      playerParams: { //Veja a lista de Parâmetros suportados
        width: 'auto',
        height: 'auto',
        enableShare: false,
        volume: 50,
        startOutput: '720p',
        html5: true,
        scrolling: 'no',
        frameBorder: '0',
        resume: resume || course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].nu_segundos
      },
      events: {
        '*': 'eventListener',
        'onFinish': () => this.autoplay(),
        'onListen': (value) => this.onProgress(value),
        'onStart': (value) => this.sendEvent(value),
        'onPause' :()=>{clearInterval(this.state.videoEvent)},
        'onResume' : (value) => this.sendEvent(value)
      }
    });
    return undefined;
  };

  renderModulePos = (state) => {
    if (this.props.course.modules[state].st_modulo_pos.split(' ')[0] === 'Modulo') {
      return this.props.course.modules[state].st_modulo_pos.split(' ')[1];
    }
    return this.props.course.modules[state].st_modulo_pos;
  }

  sendEvent = (value) => {
    this.setState({
      videoEvent:setInterval(() => {
        this.props.visitor.event("Video", "Assistindo Video").send();
      }, 30000)
    });
  }

  handleClickFiles = (state) =>{
    this.setState({ closeFiles: !this.state.closeFiles, loadingFiles: !this.state.loadingFiles });
  }

  render() {
    if (this.props.loading || _.isEmpty(this.props.course.modules)) {
      return <Loading isVisible={true} overlay={true} />;
    }
    const title = !_.isUndefined(this.props.course.title) ? this.props.course.title.substring(0, (this.props.course.title).indexOf('_')) : null;
    const courseTitle = this.state.courseTitle === '' ? this.props.course.modules[0].videos[0].st_samba_title : this.state.courseTitle;
    const courseModule = 'Módulo ' + this.renderModulePos(this.state.courseModule);
    const courseModulePlaying = 'Módulo ' + this.renderModulePos(this.state.courseModulePlaying);
    const courseModuleTitle = this.props.course.modules[this.state.courseModule].st_modulomatriz.trim();
    const courseClass = this.state.courseClass + 1;
    const favorited = this.props.course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].bl_favorito;
    const item = this.props.course.modules[this.state.courseModulePlaying].videos[this.state.courseClass];
    return (
      <React.Fragment>
        <DefaultModal
          title={intl.get('modal.title')}
          isVisible={this.state.showModal}
          handleClose={this.hideModal}
          titleClass={title}
          image={this.props.course.image}
          background={this.props.course.background}
          course={this.props.course.course}
          courseID={this.props.course.courseID}
          teacher={this.props.course.teacher}
          questionsArray={this.props.questions}
          submit={this.props.rating}
          loading={this.props.loadingRating}
          setLoading={this.props.setLoading}
          success={this.props.success}
        />
        <MessageModal
          title={intl.get('messages.modalTitle')}
          isVisible={this.state.showModalMessage}
          handleClose={this.hideModalMessage}
          titleClass={title}
          image={this.props.course.image}
          background={this.props.course.background}
          course={this.props.course.course}
          courseID={this.props.course.courseID}
          teacher={this.props.course.teacher}
          sendMessage={this.props.sendMessage}
          loading={this.props.loadingMessage}
          loadingModal={this.props.loadingModal}
          setLoading={this.props.setLoading}
          messagesList={this.props.messagesList}
          messages={this.props.messages}
          getList={this.props.getListMessage}
          getMessages={this.props.getMessages}
          cleanMessages={this.props.cleanMessages}
          matrizID={this.props.course.matrizID}
          projetoPedagogico={this.props.course.projetoPedagogico}
          id_matricula={this.props.course.matricula}
          reset={this.props.resetMessages}
        />
        <ReactTooltip />
        <section style={{ display: 'flex', flexDirection: 'row' }}>
          <div className="course" data-test='courseComponent'>
            <div className="course__header">
              <span className="course__header--title">{title}</span>
            </div>
            <div className="course__body">
              <div id="playerST" className="course__body--player"></div>
              <div className="course__footer">
                <span className="course__footer--title">
                  {courseTitle}
                  <i
                    className={`icon-bookmark${favorited ? '_on' : ''} course__footer--icon`}
                    onClick={() => this.handleFavorited(item, favorited)}
                  />
                </span>
                <div className="course__footer--tags">
                  <div className="course__footer--tags-module">{courseModulePlaying}</div>
                  <div className="course__footer--tags-class">{intl.get('course.class')} {courseClass}</div>
                </div>
                <div className="course__footer--divider"></div>
                {<VideoFilesBottom
                  loading={this.props.loadingFiles}
                  handleFiles={this.handleFiles}
                  videoID={this.props.course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].id_samba_id}
                  files={this.props.files}
                  handleClick={this.handleClickFiles}
                  close={this.state.closeFiles}
                />}
              </div>
            </div>
          </div>
          <div className="modules-control" onClick={() => this.handleModulesSidebar()}>
            <div className="modules-control__button">
              <i id="control-button" className={`icon-${this.state.openModules ? 'right-arrow' : 'left-arrow'} modules-control__icon`} />
            </div>
          </div>
          <div className={`modules modules__${this.state.openModules ? 'open' : 'close'}`}>
            <div className="modules__course-data">
              <Img
                src={[this.props.course.image, imgTeacherEmpty]}
                className="modules__course-data--image"
                alt="professor"
                style={{ backgroundImage: `url(${this.props.course.background})`, backgroundSize: 'cover' }}
              />
              <div className="modules__course-data--info">
                <span className="modules__course-data--info-course">{this.props.course.course}</span>
                <span className="modules__course-data--info-class" data-tip={title}>{title}</span>
                <span className="modules__course-data--info-teacher">{this.props.course.teacher}</span>
              </div>
            </div>
            <div className="modules__course-footer">
              <div className="modules__course-footer-group" onClick={() => this.showModalMessage()}>
                <i className="icon-comment2 modules__course-footer--icon-chat" />
                <span className="modules__course-footer--title">{intl.get('course.help')}</span>
              </div>
              <div className="vertical-divider modules__divider"></div>
              <div className="modules__course-footer-group" onClick={() => this.showModal()}>
                <i className="icon-star modules__course-footer--icon" />
                <span className="modules__course-footer--title">{intl.get('course.rating')}</span>
              </div>
              <div className="vertical-divider modules__divider"></div>
              <div className="modules__course-footer-group" style={{ marginLeft: '-.6rem' }}>
                <i className="icon-video_play modules__course-footer--icon-video" />
                <span className="modules__course-footer--title">
                  {this.props.course.qtdVideos}
                  {parseInt(this.props.course.qtdVideos) > 1 ? ' vídeos' : ' vídeo'}
                </span>
              </div>
            </div>
            <div className="modules__list">
              <div className="modules__list--item" onClick={() => this.handleAccordion()}>
                <div className="modules__list--item-number">{courseModule}</div>
                <div className="modules__list--item-group">
                  <div className="modules__list--item-title">{courseModuleTitle}</div>
                  <i className={`icon-${!this.state.showModules ? 'down-arrow' : 'up-arrow'} modules__list--item-icon${this.state.showModules ? '-open' : ''}`} />
                </div>
              </div>
              <div className="modules__list--content">
                <div className={`modules__list--accordion modules__list--accordion${this.state.showModules ? '-open' : '-close'}`}>
                  {this.state.showModules
                    ? this.modulesList(this.props.course.modules)
                    : null}
                </div>
              </div>
            </div>
            <div className="videos">
              <div className="modules__list--resume">
                <div className="modules__list--resume-counter">
                  <i
                    className="icon-video_play modules__list--resume-counter-icon"
                  />
                  {this.props.course.modules[this.state.courseModule].videos.length}
                  {this.props.course.modules[this.state.courseModule].videos.length > 1 ? ' vídeos' : ' vídeo'}
                </div>
                {/* <div className="modules__list--resume-outlet">
                  <span className="modules__list--resume-outlet-title">Baixar módulo</span>
                  <Switch
                    onChange={this.handleChange}
                    checked={this.state.checked}
                    onColor="#48d852"
                    onHandleColor="#FFF"
                    offColor="#1f1f1f"
                    offHandleColor="#FFF"
                    handleDiameter={20}
                    uncheckedIcon={false}
                    checkedIcon={false}
                    boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                    activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                    height={20}
                    width={35}
                  />
                </div> */}
              </div>
              <div className="videos__list">
                {this.videosList(this.props.course.modules, item)}
              </div>
              {!_.isEmpty(this.props.course.modules[this.state.courseModule].arquivos)
                ? <div className="archives">
                  <div className="archives__title">{intl.get('course.complement')}</div>
                  <div className="archives__list">
                    {this.archivesList(this.props.course.modules)}
                  </div>
                </div>
                : null}
            </div>
          </div>
        </section>
      </React.Fragment >
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.course.loading,
    loadingMessage: state.messages.loadingChat,
    loadingModal: state.messages.loading,
    messages: state.messages.messages,
    messagesList: state.messages.list,
    course: state.course.data,
    user: state.auth.user,
    questions: state.course.questions,
    courseSelected: state.course.courseSelected,
    loadingRating: state.course.loadingRating,
    success: state.course.rating,
    videoToOpen: state.containers.courseToOpen,
    files: state.containers.videoFiles,
    loadingFiles: state.containers.loadingFiles,
    visitor: state.analytics,
    videoPlaying: state.course.videoPlaying
  };
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      setLoading,
      getList,
      reset,
      getQuestions,
      rating,
      setFavorite,
      unsetFavorite,
      getFiles,
      saveProgress,
      getListMessage,
      getMessages,
      sendMessage,
      cleanMessages,
      resetMessages
    },
    dispatch
  )
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Course));
