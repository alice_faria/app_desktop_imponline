/* eslint-disable no-undefined */
import React from 'react';
import { shallow } from 'enzyme';

import { Course as UnconnectedCourse } from '../course';
import { findByTestAtrr } from 'helpers/wrappers';

const setup = (getList, setLoading, props, match, loading, getQuestions) => {
  return shallow(
    <UnconnectedCourse
      getList={getList}
      loading={loading}
      setLoading={setLoading}
      course={props.course}
      match={match}
      getQuestions={getQuestions}
      courseSelected={props.courseSelected}
    />
  );
};

//Testing the connected component
describe('Testing Course', () => {
  let wrapper;

  beforeEach(() => {
    const props = {
      course: {
        title: 'testando',
        projectHash: '6169876d5ac3a7d26e69d4471ef941b7',
        modules: [
          {
            id_matriz: '19',
            st_matriz: 'Atualidades_Urani',
            id_modulomatriz: '52',
            st_modulomatriz: 'Atualidades',
            st_modulo_pos: 'Modulo 1',
            videos: [
              {
                id_samba_id: '0639dd96792bd05455e0320c290b4554',
                st_samba_title: 'América Latina - Exercícios'
              }
            ]
          }
        ]
      },
      courseSelected: {
        id_projetopedagogico: 13123,
        id_disciplina: 31434
      }
    };
    const loading = false;
    const match = { params: { id: 'localhost:3000/course/13131+13131+12313' } };
    const geList = jest.fn();
    const setLoading = jest.fn();
    const getQuestions = jest.fn();
    wrapper = setup(geList, setLoading, props, match, loading, getQuestions);
  });

  it('should render the connected component', () => {
    const component = findByTestAtrr(wrapper, 'courseComponent');
    expect(component.length).toBe(1);
  });

  it('should renders all parts', () => {
    expect(wrapper).toMatchSnapshot();
  });

});
