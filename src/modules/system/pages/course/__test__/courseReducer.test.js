/* eslint-disable no-undefined */
import CourseReducers from '../courseReducer';
import types from '../courseTypes';

describe('Testing Auth Reducers', () => {
  const initialState = {
    data: {
      title: '',
      projectHash: '',
      image: '',
      teacher: '',
      course: '',
      courseID: null,
      modules: []
    },
    questions: [],
    rating: false,
    disconnected: false,
    loading: false,
    loadingRating: false,
    courseSelected: {}
  };

  it('should return initial state', () => {
    const action = { type: 'dummy_action' };

    expect(CourseReducers(undefined, action)).toEqual(initialState);
  });

  it('should activate loading component', () => {
    const action = { type: types.COURSE_LOADING, payload: true };
    const expetedState = { ...initialState, loading: true };

    expect(CourseReducers(undefined, action)).toEqual(expetedState);
  });

  it('should deactivate loading component', () => {
    const action = { type: types.COURSE_LOADING, payload: false };
    const expetedState = { ...initialState, loading: false };

    expect(CourseReducers(undefined, action)).toEqual(expetedState);
  });
  it('should save user token in localStorage and return then', () => {
    const token = '[jwt token]';
    const action = { type: types.COURSE_FETCHED, data: token };

    expect(CourseReducers(undefined, action)).toMatchSnapshot();
  });
  it('should reset the course data', () => {
    const action = { type: types.COURSE_RESET, data: '' };

    expect(CourseReducers(undefined, action)).toEqual({ ...initialState, loading: true });
  });

});
