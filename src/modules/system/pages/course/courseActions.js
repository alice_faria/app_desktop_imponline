/* eslint-disable no-undefined */
import axios from 'axios';
import _ from 'lodash';
import types from './courseTypes';

import { BASE_API_MATRIZ, USER_TOKEN, BASE_API } from 'config/consts';
import { errorHandler } from 'helpers/errorHandler';
import { setLoading } from 'helpers/redux';
import randomImages from 'common/containers/randomImages/randomImages';

/**
 * @function getList - Função utilizada para solicitar os projetos pedagógicos
 * do usuário
 * @param {Object} values
 * @returns {Function} Função para pegar os projetos pedagógicos do usuário
 */
export const getList = (id) => {
  const id_matricula = parseInt(id.split('+')[0]);
  const id_course = parseInt(id.split('+')[1]);
  const id_PP = parseInt(id.split('+')[2]);
  return (dispatch) => {
    return axios.get(`${BASE_API_MATRIZ}/api/matriz/projeto/${id_PP}?bl_modulos=1&id_disciplina=${id_course}&id_matricula=${id_matricula}`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      let data;
      //Organizar a resposta
      if (_.isEmpty(response.data.disciplinas)) {
        data = {
          title: '',
          projectHash: '',
          image: '',
          modules: []
        };
      } else {
        data = {
          projetoPedagogico: response.data.id_projetopedagogico,
          title: response.data.disciplinas[0].st_disciplina,
          matrizID: response.data.disciplinas[0].id_matriz,
          matricula: id_matricula,
          projectHash: response.data.st_projecthash,
          image: response.data.disciplinas[0].st_imagemprofessor,
          teacher: response.data.disciplinas[0].st_nomeprofessor,
          background: randomImages(response.data.disciplinas[0].id_disciplina),
          course: response.data.st_tituloexibicao,
          courseID: id_course,
          qtdVideos: response.data.disciplinas[0].nu_videos,
          modules: response.data.disciplinas[0].modulos
        };
      }

      dispatch({
        type: types.COURSE_FETCHED,
        payload: data
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'COURSE_ERROR', types.COURSE_LOADING);
    });
  };
};

export const getQuestions = () => {
  return (dispatch) => {
    return axios.get(`${BASE_API}/api/avaliacao/qualidade-disciplina/questoes`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.COURSE_QUESTIONS_FETCHED,
        payload: response.data
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'COURSE_QUESTIONS_ERROR', types.COURSE_LOADING);
    });
  };
};

export const rating = (values) => {
  let arrayAvaliacoes = [];
  for (const i in values) {
    if (i.replace(/[^a-zA-Z]+/g, '') === 'question') {
      arrayAvaliacoes.push({
        id_avaliacaoqualidadedisciplinapergunta: i.replace(/\D/g, ''),
        nu_nota: values[i]
      });
    }
  }
  const newValues = {
    id_disciplina: values.courseID,
    st_comentario: values.comment,
    avaliacoes: arrayAvaliacoes
  };
  return (dispatch) => {
    return axios({
      method: 'post',
      url: `${BASE_API}/api/avaliacao/qualidade-disciplina`,
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      },
      data: newValues
    }).then((response) => {
      dispatch({ type: 'COURSE_RATING_FETCHED', payload: true });
      dispatch(setLoading(types.COURSE_RATING_LOADING, false));
    }).catch((err) => {
      errorHandler(dispatch, err, 'COURSE_QUESTIONS_ERROR', types.COURSE_RATING_LOADING);
    });
  };
};

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.COURSE_RESET
    });
  };
};

export const easyReset = () => {
  return (dispatch) => {
    dispatch({
      type: types.COURSE_EASY_RESET
    });
  };
};

export const setCourse = (matricula, id_disciplina, id_projetopedagogico) => {
  return (dispatch) => {
    dispatch({
      type: types.COURSE_SELECTED_FETCHED,
      payload: { matricula, id_disciplina, id_projetopedagogico }
    });
  };
};

/**
 * @function setFavorite - Função utilizada para evoluir o atributo de favorito do item
 * @returns {Function} Função que altera o atributo de favorito do item
 */
const favorite = (values) => {
  return (dispatch) => {
    return axios({
      method: 'post',
      url: `${BASE_API_MATRIZ}/api/favorito/registrar`,
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      },
      data: values
    }).then((response) => {
    }).catch((err) => {
      errorHandler(dispatch, err, 'COURSE_SET_FAVORITED_ERROR', types.COURSE_LOADING);
    });
  };
};

/**
 * @function setFavorite - Função utilizada para evoluir o atributo de favorito do item
 * @returns {Function} Função que altera o atributo de favorito do item
 */
export const unFavorite = (values) => {
  return (dispatch) => {
    return axios({
      method: 'put',
      url: `${BASE_API_MATRIZ}/api/favorito/remover`,
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      },
      params: values
    }).then((response) => {
    }).catch((err) => {
      errorHandler(dispatch, err, 'COURSE_SET_FAVORITED_ERROR', types.COURSE_LOADING);
    });
  };
};

export const setFavorite = (data, values, type) => {
  return (dispatch) => {
    dispatch({
      type: type,
      payload: data
    });
    dispatch(favorite(values));
  };
};

export const unsetFavorite = (data, values, type) => {
  return (dispatch) => {
    dispatch({
      type: type,
      payload: data
    });
    dispatch(unFavorite(values));
  };
};
