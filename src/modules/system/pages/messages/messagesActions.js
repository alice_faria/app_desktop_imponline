/* eslint-disable no-undefined */
import axios from 'axios';
import types from './messagesTypes';

import { USER_TOKEN, BASE_API } from 'config/consts';
import { errorHandler } from 'helpers/errorHandler';
const CancelToken = axios.CancelToken;
let cancel;

export const getList = () => {
  return (dispatch) => {
    return axios({
      method: 'get',
      url: `${BASE_API}/api/ocorrencia/listar-conversas-ativas`,
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.MESSAGES_LIST_FETCHED,
        payload: response.data.slice().sort((a,b) => {
          if (a.dt_ultimamensagem < b.dt_ultimamensagem) {
            return 1;
          }
          if (a.dt_ultimamensagem > b.dt_ultimamensagem) {
            return -1;
          }
          // a must be equal to b
          return 0;
        })
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'MESSAGES_LIST_FETCHED_ERROR', types.MESSAGES_LIST_LOADING);
    });
  };
};

export const changeOrder = (dataChanged) => {
  return (dispatch) => {
    dispatch({
      type: types.MESSAGES_LIST_FETCHED,
      payload: dataChanged.slice().sort((a,b) => {
        if (a.dt_ultimamensagem < b.dt_ultimamensagem) {
          return 1;
        }
        if (a.dt_ultimamensagem > b.dt_ultimamensagem) {
          return -1;
        }
        // a must be equal to b
        return 0;
      })
    });
  };
};

const filterMessages = messages => {
  return messages.map(item => ({
    date: item.dt_cadastro,
    message: item.st_mensagem,
    fromMe: item.id_usuarioremetente === item.id_usuariointeressado,
    status: 'sent'
  })
  );
};

/**
 * @function setFavorite - Função utilizada para evoluir o atributo de favorito do item
 * @returns {Function} Função que altera o atributo de favorito do item
 */
export const getItem = (id) => {
  return (dispatch) => {
    return axios({
      method: 'get',
      url: `${BASE_API}/api/ocorrencia/${id}/listar-conversas-ativas`,
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.MESSAGES_ITEM_FETCHED,
        payload: response.data
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'MESSAGES_ITEM_ERROR', types.MESSAGES_LIST_LOADING);
    });
  };
};

/**
 * @function setFavorite - Função utilizada para evoluir o atributo de favorito do item
 * @returns {Function} Função que altera o atributo de favorito do item
 */
export const getMessages = (matriz, matricula) => {
  if (cancel !== undefined) {
    cancel();
  }
  return (dispatch) => {
    return axios.get(`${BASE_API}/api/ocorrencia/${matriz}/${matricula}/listar-conversa`,{
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      },
      cancelToken: new CancelToken(function executor(c) {
        // An executor function receives a cancel function as a parameter
        cancel = c;
      })
    }).then((response) => {
      dispatch({
        type: types.MESSAGES_FETCHED,
        payload: filterMessages(response.data)
      });
    }).catch((err) => {
      if (!axios.isCancel(err)) {
        errorHandler(dispatch, err, 'MESSAGES_FETCHED_ERROR', types.MESSAGES_LIST_LOADING);
      }
    });
  };
};

/**
 * @function saveMessages - Função utilizada para evoluir o atributo de favorito do item
 * @returns {Function} Função que altera o atributo de favorito do item
 */
const saveMessages = (data) => {
  return (dispatch) => {
    return axios({
      method: 'post',
      url: `${BASE_API}/api/ocorrencia/salvar-mensagem`,
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      },
      data
    }).then((response) => {
      dispatch({
        type: types.MESSAGES_FETCHED,
        payload: filterMessages(response.data)
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'SAVE_MESSAGES_ERROR', types.MESSAGES_LOADING);
    });
  };
};

export const sendMessage = (data, dataChanged) => {
  return (dispatch) => {
    dispatch({
      type: types.MESSAGES_FETCHED,
      payload: dataChanged
    });
    dispatch(saveMessages(data));
  };
};

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.MESSAGES_LIST_RESET
    });
  };
};

export const cleanMessages = () => {
  return (dispatch) => {
    dispatch({
      type: types.MESSAGES_RESET
    });
  };
};
