import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import { setLoading } from 'helpers/redux';
import moment from 'moment';
import 'moment/locale/pt-br';

import intl from 'react-intl-universal';
import ReactTooltip from 'react-tooltip';
import _ from 'lodash';
import produce from 'immer';

import 'react-chat-elements/dist/main.css';
import { Input } from 'react-chat-elements';
import ScrollToBottom from 'react-scroll-to-bottom';
import Img from 'react-image';

import Loading from 'common/components/ui/loading/loading';
import { getList, getMessages, sendMessage, reset, cleanMessages, changeOrder } from './messagesActions';
import disconnected from 'assets/images/img_internet.png';
import types from './messagesTypes';
import imgTeacherEmpty from 'assets/images/imgMockCourse.png';
import emptyMessages from 'assets/images/img_message.png';
import randomImages from 'common/containers/randomImages/randomImages';
import { capitalizeFirstLetter } from 'helpers/dataTransformers';
import Button from 'common/components/ui/button/button';
import { handleMessages } from 'helpers/chatElements';

moment.locale('pt-br');

export class Messages extends Component {
  constructor(props) {
    super(props);
    this.props.setLoading(types.MESSAGES_LIST_LOADING, true);
    this.props.getList();
    this.state = {
      selected: {
        index: 0,
        name: '',
        avatar: '',
        project: '',
        id_course: ''
      },
      updated: false,
      text: ''
    };
  }

  componentDidUpdate() {
    if (!_.isEmpty(this.props.messagesList) && !this.state.updated) {
      this.setState({ updated: true },
        () => {
          this.props.getMessages(this.props.messagesList[0].id_matriz, this.props.messagesList[0].id_matricula);
          this.selectChat({
            st_nomeprofessor: this.props.messagesList[0].st_nomeprofessor,
            st_urlavatar: this.props.messagesList[0].st_urlavatar,
            st_disciplina: this.props.messagesList[0].st_disciplina,
            id_disciplina: this.props.messagesList[0].id_disciplina,
            st_projetopedagogico: this.props.messagesList[0].st_projetopedagogico
          }, 0);
        }
      );
    }
  }

  componentWillUnmount() {
    this.props.reset();
  }

  handleSubjectName = (name) => {
    if (name && name.includes('_')) {
      return name.substring(0, name.indexOf('_'));
    }
    return name;
  }

  selectChat = (data, index) => {
    this.props.setLoading(types.MESSAGES_LOADING, true);
    this.setState({
      selected: {
        index,
        teacher: this.handleTeacherName(data.st_nomeprofessor),
        name: this.handleSubjectName(data.st_disciplina),
        project: data.st_projetopedagogico,
        avatar: data.st_urlavatar,
        id_course: data.id_disciplina
      }
    },
    () => {
      this.props.cleanMessages();
      this.props.getMessages(this.props.messagesList[index].id_matriz, this.props.messagesList[index].id_matricula);
    });
  }

  handleTeacherName = (teacher) => {
    const teacherArray = teacher.split(' ');
    let teachersCapitalize = [];
    for (const teacher of teacherArray) {
      teachersCapitalize.push(capitalizeFirstLetter(teacher));
    }
    return teachersCapitalize.join(' ');
  }

  sendMessage = () => {
    const { messages, messagesList } = this.props;
    let text = this.state.text;
    const data = {
      st_mensagem: text,
      id_matricula: this.props.messagesList[this.state.selected.index].id_matricula,
      id_matriz: this.props.messagesList[this.state.selected.index].id_matriz
    };
    const listChanged = produce(messagesList, draftState => {
      draftState[this.state.selected.index].dt_ultimamensagem = moment().format('YYYY-MM-DD HH:MM');
    });
    const dataChanged = produce(messages, draftState => {
      draftState.push({
        date: moment(),
        message: text,
        fromMe: true,
        status: 'waiting'
      });
    });
    this.setState({ text: '' }, () => {
      this.refs.input.clear();
      this.props.sendMessage(data, dataChanged);
      this.props.changeOrder(listChanged);
    });
  }

  keyPressed(event) {
    event.preventDefault();
    if (event.key === 'Enter') {
      this.sendMessage();
    }
  }

  handleText = event => {
    this.setState({
      text: event.target.value
    });
  }

  handleKeyPressed = () => {
    return this.state.text === ''
      ? null
      : (e) => {
        if (e.shiftKey && e.charCode === 13) {
          return true;
        }
        if (e.charCode === 13) {
          this.refs.input.clear();
          this.sendMessage();
          e.preventDefault();
          return false;
        }
        return null;
      };
  }

  chat = () => {
    const { messages } = this.props;
    if (this.props.loadingChat) {
      return (
        <React.Fragment>
          <div className="messages__chat--header">
            <Img
              src={[this.state.selected.avatar, imgTeacherEmpty]}
              className="messages__chat--header-image"
              style={{ backgroundImage: `url(${randomImages(this.state.selected.id_course)})`, backgroundSize: 'cover' }}
            />
            <div className="messages__chat--header-info">
              <span className="messages__chat--header-course">{this.state.selected.project}</span>
              <span className="messages__chat--header-name">{this.state.selected.name}</span>
              <span className="messages__chat--header-teacher">{this.state.selected.teacher}</span>
            </div>
          </div>
          <Loading isVisible={this.props.loadingChat} overlay={false} className='messages__chat--loading' />
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        <div className="messages__chat--header">
          <Img
            src={[this.state.selected.avatar, imgTeacherEmpty]}
            className="messages__chat--header-image"
            style={{ backgroundImage: `url(${randomImages(this.state.selected.id_course)})`, backgroundSize: 'cover' }}
          />
          <div className="messages__chat--header-info">
            <span className="messages__chat--header-course">{this.state.selected.project}</span>
            <span className="messages__chat--header-name">{this.state.selected.name}</span>
            <span className="messages__chat--header-teacher">{this.state.selected.teacher}</span>
          </div>
        </div>
        <ScrollToBottom className="messages__chat--body" refs="scroll">
          {_.isEmpty(messages)
            ? <div className="emptyHome">
              <img src={emptyMessages} className="emptyHome__img" alt="empty" />
              <span className="emptyHome__title">{intl.getHTML('messages.emptyPage')}</span>
            </div>
            : handleMessages(messages)
          }
        </ScrollToBottom>
        <Input
          placeholder={intl.get('messages.placeholder')}
          multiline
          autofocus
          ref='input'
          className="messages__chat--footer"
          onChange={this.handleText}
          onKeyPress={this.handleKeyPressed()}
          rightButtons={
            this.state.text === ''
              ? null
              : <Button
                color='white'
                backgroundColor='black'
                disabled={this.state.text === ''}
                disabledClass='messages__chat--footer-button-disabled'
                style={{ width: '4rem' }}
                onClick={() => this.sendMessage()}
              >
                <i className="icon-send messages__chat--footer-icon" />
              </Button>
          } />
      </React.Fragment>
    );
  }

  sidebar = () => (
    <React.Fragment>
      <div className="messages__sidebar messages__sidebar__open">
        {this.props.messagesList.map((item, index) => (
          <div
            className={`${this.state.selected.index === index ? 'selected' : ''} messages__sidebar--item`}
            onClick={this.state.selected.index === index ? null : () => this.selectChat(item, index)}
          >
            <Img
              src={[item.st_urlavatar, imgTeacherEmpty]}
              className="messages__sidebar--image"
              style={{ backgroundImage: `url(${randomImages(item.id_disciplina)})`, backgroundSize: 'cover' }}
            />
            <div className="messages__chat--header-info">
              <span className="messages__sidebar--course" data-tip={item.st_projetopedagogico}>{item.st_projetopedagogico}</span>
              <span className="messages__sidebar--name" data-tip={this.handleSubjectName(item.st_disciplina)}>{this.handleSubjectName(item.st_disciplina)}</span>
              <span className="messages__sidebar--teacher" data-tip={this.handleTeacherName(item.st_nomeprofessor)}>{this.handleTeacherName(item.st_nomeprofessor)}</span>
            </div>
            <div className="messages__sidebar--time">{moment(item.dt_ultimamensagem).format('DD/MM HH:mm')}</div>
          </div>
        ))}
      </div>
    </React.Fragment>
  )

  render() {
    if (this.props.loading) {
      return <Loading isVisible={this.props.loading} overlay={true} />;
    } else if (this.props.disconnected) {
      return (
        <div className="emptyHome">
          <img src={disconnected} className="emptyHome__img" alt="empty" />
          <span className="emptyHome__title">{intl.getHTML('errorRequest.connection')}</span>
        </div>
      );
    } else if (_.isEmpty(this.props.messages) && !this.props.loading && !this.props.loadingChat) {
      return (
        <div className="emptyHome">
          <img src={emptyMessages} className="emptyHome__img" alt="empty" />
          <span className="emptyHome__title">{intl.getHTML('messages.empty')}</span>
        </div>
      );
    }
    return (
      <section style={{ display: 'flex', flexDirection: 'row' }}>
        <ReactTooltip />
        <div className="messages" data-test='messagesComponent'>
          <div className="messages__header">
            <span className="messages__header--title">{intl.get('messages.title')}</span>
          </div>
          <div className="messages__chat">
            {this.chat()}
          </div>
        </div>
        {this.sidebar()}
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.messages.loading,
    loadingChat: state.messages.loadingChat,
    messages: state.messages.messages,
    messagesList: state.messages.list,
    disconnected: state.messages.disconnected
  };
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      setLoading,
      getList,
      sendMessage,
      getMessages,
      reset,
      cleanMessages,
      changeOrder
    },
    dispatch
  )
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Messages));
