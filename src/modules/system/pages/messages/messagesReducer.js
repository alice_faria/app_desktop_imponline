import types from './messagesTypes';

const initialState = {
  messages: [],
  list: [],
  disconnected: false,
  loading: false,
  loadingChat: false
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.MESSAGES_FETCHED:
      return {
        ...state,
        messages: payload || initialState.messages,
        loading: false,
        loadingChat: false
      };
    case types.MESSAGES_LIST_FETCHED:
      return {
        ...state,
        list: payload || initialState.list,
        loading: false
      };
    case types.MESSAGES_LOADING:
      return {
        ...state,
        loadingChat: payload || initialState.loading
      };
    case types.MESSAGES_LIST_LOADING:
      return {
        ...state,
        loading: payload || initialState.loading
      };
    case 'ERROR_CONNECTION':
      return {
        ...state,
        disconnected: true
      };
    case types.MESSAGES_RESET:
      return {
        ...state,
        messages: initialState.messages
      };
    case types.MESSAGES_LIST_RESET || 'RESET_APPLICATION':
      return {
        ...state,
        ...initialState
      };
    default:
      return state;
  }
};
