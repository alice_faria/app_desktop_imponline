/* eslint-disable no-undefined */
import CourseReducers from '../messagesReducer';
import types from '../messagesTypes';

describe('Testing Auth Reducers', () => {
  const initialState = {
    messages: [],
    list: [],
    disconnected: false,
    loading: false,
    loadingChat: false
  };

  it('should return initial state', () => {
    const action = { type: 'dummy_action' };

    expect(CourseReducers(undefined, action)).toEqual(initialState);
  });

  it('should activate loadingChat component', () => {
    const action = { type: types.MESSAGES_LOADING, payload: true };
    const expetedState = { ...initialState, loadingChat: true };

    expect(CourseReducers(undefined, action)).toEqual(expetedState);
  });

  it('should deactivate loadingChat component', () => {
    const action = { type: types.MESSAGES_LOADING, payload: false };
    const expetedState = { ...initialState, loadingChat: false };

    expect(CourseReducers(undefined, action)).toEqual(expetedState);
  });
  it('should activate loading component', () => {
    const action = { type: types.MESSAGES_LIST_LOADING, payload: true };
    const expetedState = { ...initialState, loading: true };

    expect(CourseReducers(undefined, action)).toEqual(expetedState);
  });

  it('should deactivate loading component', () => {
    const action = { type: types.MESSAGES_LIST_LOADING, payload: false };
    const expetedState = { ...initialState, loading: false };

    expect(CourseReducers(undefined, action)).toEqual(expetedState);
  });
  it('should save messages and return then', () => {
    const token = '[jwt token]';
    const action = { type: types.MESSAGES_FETCHED, data: token };

    expect(CourseReducers(undefined, action)).toMatchSnapshot();
  });
  it('should save messages list and return then', () => {
    const token = '[jwt token]';
    const action = { type: types.MESSAGES_LIST_FETCHED, data: token };

    expect(CourseReducers(undefined, action)).toMatchSnapshot();
  });
  it('should reset the component', () => {
    const action = { type: types.COURSE_RESET, data: '' };

    expect(CourseReducers(undefined, action)).toEqual(initialState);
  });

});
