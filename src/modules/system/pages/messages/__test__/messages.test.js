/* eslint-disable no-undefined */
import React from 'react';
import { shallow } from 'enzyme';

import { Messages as UnconnectedMessages } from '../messages';
import { findByTestAtrr } from 'helpers/wrappers';

const setup = (functions) => {
  return shallow(
    <UnconnectedMessages
      getList={functions.getList}
      loading={functions.loading}
      setLoading={functions.setLoading}
      messages={functions.props.messages}
      sendMessage={functions.sendMessage}
      getMessages={functions.getMessages}
      reset={functions.reset}
      changeOrder={functions.changeOrder}
      cleanMessages={functions.cleanMessages}
      messagesList={functions.props.messagesList}
    />
  );
};

//Testing the connected component
describe('Testing Messages', () => {
  let wrapper;

  beforeEach(() => {
    const props = {
      messages: [{
        date: '2019-09-11 11:01:07.4600000',
        message: '[IMP Flex | Administração Financeira e Orçamentária_José Wesley | Introdução ao Orçamento Público | Conceito e Introdução] Jdjsjshdh',
        fromMe: true,
        status: 'sent'
      }],
      messagesList: [{
        id_ocorrencia: 968711,
        st_ocorrencia: 'Conversa iniciada na Disciplina Administração Financeira e Orçamentária_José Wesley',
        id_matricula: 712514,
        id_entidade: 12,
        id_matriz: 303,
        st_matriz: 'Administração Financeira e Orçamentária_José Wesley',
        id_disciplina: 13856,
        st_disciplina: 'Administração Financeira e Orçamentária_José Wesley',
        id_projetopedagogico: 3622577,
        st_projetopedagogico: 'IMP Flex',
        id_assuntoco: 3145,
        st_assuntoco: 'JOSE WESLEY ROCHA FERNANDES',
        id_usuarioprofessor: 10064687,
        st_imagemprofessor: 'https://conteudo-imp.s3.sa-east-1.amazonaws.com/web/img/fotos/10064687.png',
        st_urlavatar: 'https://conteudo-imp.s3.sa-east-1.amazonaws.com/web/img/fotos/10064687.png',
        st_nomeprofessor: 'JOSE WESLEY ROCHA FERNANDES',
        st_ultimamensagem: 'dadadas',
        dt_ultimamensagem: '2019-09-24 10:19:30.3000000'
      }]
    };
    const loading = false;
    const getList = jest.fn();
    const setLoading = jest.fn();
    const sendMessage = jest.fn();
    const getMessages = jest.fn();
    const reset = jest.fn();
    const changeOrder = jest.fn();
    const cleanMessages = jest.fn();
    const functions = {
      getList,
      setLoading,
      props,
      loading,
      sendMessage,
      getMessages,
      reset,
      changeOrder,
      cleanMessages
    }
    wrapper = setup(functions);
  });

  it('should render the connected component', () => {
    const component = findByTestAtrr(wrapper, 'messagesComponent');
    expect(component.length).toBe(1);
  });

  it('should renders all parts', () => {
    expect(wrapper).toMatchSnapshot();
  });

});
