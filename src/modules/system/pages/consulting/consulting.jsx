/* eslint-disable no-fallthrough */
import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
// import Switch from 'react-switch';
import intl from 'react-intl-universal';
import ReactTooltip from 'react-tooltip';
import _ from 'lodash';
import Img from 'react-image';

import { setLoading } from 'helpers/redux';
import Loading from 'common/components/ui/loading/loading';
import { reset } from './consultingActions';
import types from './consultingTypes';
import { getFiles } from 'common/containers/containersActions';
import imgTeacherEmpty from 'assets/images/imgMockCourse.png';
import VideoList from 'common/containers/VideoList/VideoList';
import ModuleList from 'common/containers/ModuleList/ModuleList';
import VideoFiles from 'common/containers/videoFiles/videoFiles';
import { setFavoriteConsulting, unsetFavoriteConsulting } from './consultingActions';
import { getList as getListMessage, getMessages, sendMessage, cleanMessages, reset as resetMessages } from 'modules/system/pages/messages/messagesActions';
import { getQuestions, rating } from 'modules/system/pages/course/courseActions';
import { urlMaximize } from 'config/consts';
import parse from 'html-react-parser';
import { handleTags } from 'helpers/handleTags';
import DefaultModal from 'common/components/ui/modals/defaultModal/defaultModal';
import MessageModal from 'common/components/ui/modals/messageModal/messageModal';
import VideoFilesBottom from '../../../../common/containers/videoFiles/videoFilesBottom';

export class Consulting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showActived: true,
      playerLoading: false,
      playerStart: false,
      playerFinish: false,
      openModules: true,
      updated: false,
      consultingTitle: '',
      consultingModule: 0,
      consultingModulePlaying: 0,
      consultingClass: 0,
      lastConsultingClass: null,
      lastConsultingModule: null,
      consultingModulePos: 1,
      showModules: false,
      showModalMessage: false,
      checked: false,
      showModal: false,
      videoProgress: null,
      videoTotal: null,
      showDescription: false,
      playing: false,
      closeFiles: true,
      loadingFiles: false
    };
    this.props.getQuestions();
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount = () => {
    if (!window.SambaPlayer) {
      console.log('criando o script');
      const tag = document.createElement('script');
      tag.src = 'https://player.sambatech.com.br/v3/samba.player.api.js';
      // SambaPlayer will load the video after the script is loaded
      window.SambaPlayer = this.loadVideo;

      const firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    }
    if (!_.isEmpty(this.props.consulting)) {
      this.loadVideo();
    }
  }

  componentDidUpdate() {
    if (!_.isEmpty(this.props.consulting)) {
      this.loadVideo();
    }
  }

  componentWillUnmount() {
    clearInterval(this.state.eventVideo);
    this.props.reset();
  }

  showModal = () => {
    this.setState({ showModal: true });
  }

  hideModal = () => {
    this.setState({ showModal: false });
  }

  showDescription = () => {
    this.setState({ showDescription: true });
  }

  hideDescription = () => {
    this.setState({ showDescription: false });
  }

  showModalMessage = () => {
    this.setState({ showModalMessage: true });
  }

  hideModalMessage = () => {
    this.setState({ showModalMessage: false });
  }

  autoplay() {
    const autoplay = JSON.parse(localStorage.getItem('AUTOPLAY')) || false;
    const { consulting } = this.props;
    if(autoplay){
      if(consulting.modulos[this.state.consultingModulePlaying].videos.length > this.state.consultingClass+1){
        clearInterval(this.state.videoEvent);
        return this.setState({
            consultingClass: this.state.consultingClass+1,
            videoPlaying: consulting.modulos[this.state.consultingModulePlaying].videos[this.state.consultingClass+1].id_samba_id,
            closeFiles: true,
            loadingFiles: false
          },
          () => this.loadVideo(
            consulting.modulos[this.state.consultingModulePlaying].videos[this.state.consultingClass].id_samba_id,
            null)
        );
      }
      if(consulting.modulos[this.state.consultingModulePlaying].videos.length === this.state.consultingClass + 1){
        clearInterval(this.state.videoEvent);
        if(consulting.modulos.length >= this.state.consultingModulePlaying + 1){
          return this.setState({
              consultingModulePlaying: this.state.consultingModule + 1,
              consultingModule: this.state.consultingModule + 1,
              consultingClass: 0,
              videoPlaying: consulting.modulos[this.state.consultingModule+1].videos[0].id_samba_id,
              closeFiles: true,
              loadingFiles: false
            },
            () => this.loadVideo(
              consulting.modulos[this.state.consultingModulePlaying].videos[this.state.consultingClass].id_samba_id,
              null)
          );
        }
        return this.setState({
          closeFiles: true,
          loadingFiles: false});
      }
    }
  }

  changeModule = (index, module_pos) => {
    this.setState({
      consultingModule: index,
      consultingModulePos: module_pos
    }, this.handleAccordion);
  }

  handleAccordion = () => {
    this.setState({
      showModules: !this.state.showModules
    });
  }

  modulesList = (modules) => (
    <ModuleList
      modules={modules}
      onClick={(index, module_pos) => this.changeModule(index, module_pos)}
      consultingModule={this.state.consultingModule}
    />
  )

  handleFiles = (id) => {
    this.props.getFiles(id);
  }

  videosList = (modules) => (
    <VideoList
      modules={modules}
      onClick={(item, index) => this.setState({
        lastConsultingClass: this.state.consultingClass,
        lastConsultingModule: this.state.consultingModulePlaying,
        consultingModulePlaying: this.state.consultingModule,
        consultingClass: index,
        consultingTitle: item.st_samba_title,
        videoPlaying: item.id_samba_id
      }, () => this.loadVideo(item.id_samba_id, null))}
      loading={this.props.loadingFiles}
      consulting
      handleFiles={this.handleFiles}
      files={this.props.files}
      courseModule={this.state.consultingModule}
      courseModulePos={this.state.consultingModulePos}
      courseModulePlaying={this.state.consultingModulePlaying}
      courseClass={this.state.consultingClass}
      course={this.props.consulting}
      setFavorite={this.props.setFavoriteConsulting}
      unsetFavorite={this.props.unsetFavoriteConsulting}
      id_projetopedagogico={this.props.consulting.id_projetopedagogico}
      type={types.CONSULTING_FETCHED}
      videoPlaying={this.state.videoPlaying || this.props.videoPlaying}
    />
  )

  archiveTypeHandler = (archive) => {
    const type = archive && archive.split('.')[archive.split('.').length - 1];
    switch (type) {
      case 'pdf':
        return 'PDF';
      case 'mp3':
        return 'Áudio';
      case 'ppt':
        return 'Slides';
      default:
        return 'Simulado';
    }
  }

  archiveIconHandler = (archive) => {
    const type = archive && archive.split('.')[archive.split('.').length - 1];
    switch (type) {
      case 'pdf':
        return 'course';
      case 'mp3':
        return 'headphones';
      case 'ppt':
        return 'presentation';
      default:
        return 'simulado';
    }
  }

  archiveNameHandler = (archive) => {
    let name = archive && archive.split('/')[archive.split('/').length - 1];
    name = decodeURI(name);
    return name;
  }

  archivesList = () => {
    let archives = [];
    archives.push(this.props.consulting.teoria);
    archives.push(this.props.consulting.questoes);
    if (!_.isEmpty(this.props.consulting.simulados)) {
      archives.push({
        url: this.props.consulting.simulados[0].url,
        name: 'Semana ' + this.props.consulting.simulados[0].semana
      });
    }
    return archives.flat().map((item, index) => {
      if (_.isObject(item)) {
        return (
          <div className="archives__list--item" key={index}>
            <div className={`archives__list--item-type archives__list--item-type-${this.archiveIconHandler(item.url)}`}>
              <i className={`icon-${this.archiveIconHandler(item.url)}  archives__list--item-icon`} />
            </div>
            <div className="archives__list--item-info">
              <div className="archives__list--item-tag">
                <div className="archives__list--item-tag-type">{this.archiveTypeHandler(item.url)}</div>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href={`${urlMaximize}/c/login/autenticar?chaveDeAcesso=${this.props.tokenMaximize}#/tia/${item.url.split('/')[item.url.split('/').length - 1]}`} >
                  <i className="icon-download archives__list--item-tag-download" />
                </a>
              </div>
              <div className="archives__list--item-name">{item.name}</div>
            </div>
          </div>
        );
      }
      return (
        <div className="archives__list--item" key={index}>
          <div className={`archives__list--item-type archives__list--item-type-${this.archiveIconHandler(item)}`}>
            <i className={`icon-${this.archiveIconHandler(item)}  archives__list--item-icon`} />
          </div><div className="archives__list--item-info">
          <div className="archives__list--item-tag">
            <div className="archives__list--item-tag-type">{this.archiveTypeHandler(item)}</div>
            <a target="_blank" rel="noopener noreferrer" href={item} ><i className="icon-download archives__list--item-tag-download" /></a></div>
          <div className="archives__list--item-name">{this.archiveNameHandler(item)}</div>
        </div>
        </div>
      );
    });
  }

  handleModulesSidebar = () => {
    this.setState({
      openModules: !this.state.openModules
    });
  }

  handleChange(checked) {
    this.setState({ checked });
  }

  loadVideo = (video_id = null, projectHash = null) => {
    const { consulting } = this.props;
    if (document.getElementsByTagName('iframe').length > 0) {
      if (video_id === null) {
        return null;
      } else {
        const videoFrame = document.getElementById('playerST');
        const child = videoFrame.firstChild;
        videoFrame.removeChild(child);
      }
    }

    this.player = new window.SambaPlayer('playerST', { //player é o ID do elemento html que ele vai inserir o iframe
      ph: projectHash || '6169876d5ac3a7d26e69d4471ef941b7',//Player Hash do projeto
      m: video_id || consulting.modulos[this.state.consultingModulePlaying].videos[this.state.consultingClass].id_samba_id,//MidiaID
      playerParams: { //Veja a lista de Parâmetros suportados
        width: 'auto',
        height: 'auto',
        enableShare: false,
        volume: 50,
        startOutput: '720p',
        html5: true,
        scrolling: 'no',
        frameBorder: '0'
      },
      events: {
        '*': 'eventListener',
        'onFinish': () => this.autoplay(),
        'onStart': (value) => this.sendEvent(value),
        'onPause' :()=>clearInterval(this.state.videoEvent),
        'onResume' : (value) => this.sendEvent(value)
      }
    });
    return undefined;
  };

  handlingArchives = () => {
    if (!_.isEmpty(this.props.consulting.teoria) || !_.isEmpty(this.props.consulting.questoes)) {
      return (<div className="archives">
          <div className="archives__title">{intl.get('course.complement')}</div>
          <div className="archives__list">
            {this.archivesList()}
          </div>
        </div>
      );
    }
    return null;
  }

  descriptionToggle = () => (
    <span
      className={`modules__course--description-toogle${this.state.showDescription ? '-open' : ''}`}
      style={{ display: this.props.consulting.description.length < 200 ? 'none' : '' }}
      onClick={() => this.state.showDescription ? this.hideDescription() : this.showDescription()}
    >
      {this.state.showDescription ? intl.get('course.descriptionOpen') : intl.get('course.description')}
    </span>
  )

  sendEvent = (value) => {
    if(!this.state.videoEvent){
      this.setState({
        videoEvent:setInterval(() => {
          this.props.visitor.event("Video", "Assistindo Video").send();
        }, 30000)
      });
    }
  }

  handleClickFiles = (state) =>{
    this.setState({ closeFiles: !this.state.closeFiles, loadingFiles: !this.state.loadingFiles });
  }

  render() {
    if (this.props.loading || _.isEmpty(this.props.consulting)) {
      return <Loading isVisible={true} overlay={true} />;
    }
    const title = this.props.consulting.st_disciplina;
    const teacher = this.props.consulting.st_nomeprofessor.split('[')[0];
    const consultingTitle = this.state.consultingTitle === '' ? this.props.consulting.modulos[0].videos[0].st_samba_title : this.state.consultingTitle;
    const consultingModule = `Módulo ${this.props.consulting.modulos[this.state.consultingModule].st_modulo_pos}`;
    const courseModuleTitle = this.props.consulting.modulos[this.state.consultingModule].st_modulomatriz.trim();
    const consultingClass = this.state.consultingClass + 1;
    return (
      <React.Fragment>
        <DefaultModal
          title={intl.get('modal.title')}
          isVisible={this.state.showModal}
          handleClose={this.hideModal}
          titleClass={title}
          image={this.props.consulting.st_imagemprofessor}
          background={this.props.consulting.background}
          course={this.props.consulting.course}
          courseID={this.props.consulting.id}
          teacher={this.props.consulting.st_nomeprofessor}
          questionsArray={this.props.questions}
          submit={this.props.rating}
          loading={this.props.loadingRating}
          setLoading={this.props.setLoading}
          success={this.props.success}
        />
        <MessageModal
          title={intl.get('messages.modalTitle')}
          isVisible={this.state.showModalMessage}
          handleClose={this.hideModalMessage}
          titleClass={title}
          image={this.props.consulting.st_imagemprofessor}
          background={this.props.consulting.background}
          course={this.props.consulting.course}
          courseID={this.props.consulting.id}
          teacher={this.props.consulting.st_nomeprofessor}
          sendMessage={this.props.sendMessage}
          loading={this.props.loadingMessage}
          loadingModal={this.props.loadingModal}
          setLoading={this.props.setLoading}
          messagesList={this.props.messagesList}
          messages={this.props.messages}
          getList={this.props.getListMessage}
          getMessages={this.props.getMessages}
          cleanMessages={this.props.cleanMessages}
          matrizID={this.props.consulting.id_matriz}
          projetoPedagogico={this.props.consulting.projetoPedagogico}
          id_matricula={this.props.consulting.id_matricula}
          reset={this.props.resetMessages}
        />
        <ReactTooltip />
        <section style={{ display: 'flex', flexDirection: 'row' }}>
          <div className="consulting" data-test='courseComponent'>
            <div className="consulting__header">
              <span className="consulting__header--title">{title}</span>
            </div>
            <div className="consulting__body">
              <div id="playerST" className="consulting__body--player"></div>
              <div className="consulting__footer">
                <span className="consulting__footer--title">
                  {consultingTitle}
                  {/* <i
                    className={'icon-bookmark consulting__footer--icon'}
                  /> */}
                </span>
                <div className="consulting__footer--tags">
                  <div className="consulting__footer--tags-module">{consultingModule}</div>
                  <div className="consulting__footer--tags-class">{intl.get('course.class')} {consultingClass}</div>
                </div>
                <div className="consulting__footer--divider"></div>
                {<VideoFilesBottom
                  loading={this.state.loadingFiles}
                  handleFiles={this.handleFiles}
                  videoID={this.props.consulting.modulos[this.state.consultingModulePlaying].videos[this.state.consultingClass].id_samba_id}
                  files={this.props.files}
                  handleClick={this.handleClickFiles}
                  close={this.state.closeFiles}
                />}
              </div>
            </div>
          </div>
          <div className="modules-control" onClick={() => this.handleModulesSidebar()}>
            <div className="modules-control__button">
              <i id="control-button" className={`icon-${this.state.openModules ? 'right-arrow' : 'left-arrow'} modules-control__icon`} />
            </div>
          </div>
          <div className={`modules modules__${this.state.openModules ? 'open' : 'close'}`}>
            <div className="modules__course-data">
              <Img
                src={[this.props.consulting.st_imagemprofessor, imgTeacherEmpty]}
                className="modules__course-data--image"
                alt="professor"
                style={{ backgroundImage: `url(${this.props.consulting.background})`, backgroundSize: 'cover' }}
              />
              <div className="modules__course-data--info">
                <span className="modules__course-data--info-course">{this.props.consulting.course}</span>
                <span className="modules__course-data--info-class" data-tip={title}>{title}</span>
                <span className="modules__course-data--info-teacher">{teacher}</span>
              </div>
            </div>
            <div className="modules__course-footer-consulting">
              <div className="modules__course-footer-group" onClick={() => this.showModalMessage()}>
                <i className="icon-comment2 modules__course-footer--icon-chat" />
                <span className="modules__course-footer--title">{intl.get('course.help')}</span>
              </div>
              <div className="vertical-divider modules__divider"></div>
              <div className="modules__course-footer-group" onClick={() => this.showModal()}>
                <i className="icon-star modules__course-footer--icon" />
                <span className="modules__course-footer--title">{intl.get('course.rating')}</span>
              </div>
              {/* <div className="vertical-divider modules__divider"></div>
              <div className="modules__course-footer-group" style={{ marginLeft: '-.6rem' }}>
                <i className="icon-video_play modules__course-footer--icon-video" />
                <span className="modules__course-footer--title">
                  {this.props.course.qtdVideos}
                  {parseInt(this.props.course.qtdVideos) > 1 ? ' vídeos' : ' vídeo'}
                </span>
              </div> */}
            </div>
            <div className={`modules__course--description${this.state.showDescription ? '-open' : ''}`}>
              {parse(handleTags(this.props.consulting.description))}
            </div>
            {this.descriptionToggle()}
            <div className="modules__list">
              <div className="modules__list--item" onClick={() => this.handleAccordion()}>
                <div className="modules__list--item-number">{consultingModule}</div>
                <div className="modules__list--item-group">
                  <div className="modules__list--item-title">{courseModuleTitle}</div>
                  <i className={`icon-${!this.state.showModules ? 'down-arrow' : 'up-arrow'} modules__list--item-icon${this.state.showModules ? '-open' : ''}`} />
                </div>
              </div>
              <div className="modules__list--content">
                <div className={`modules__list--accordion modules__list--accordion${this.state.showModules ? '-open' : '-close'}`}>
                  {this.state.showModules
                    ? this.modulesList(this.props.consulting.modulos)
                    : null}
                </div>
              </div>
            </div>
            <div className={`consulting__videos--list${this.state.showDescription ? '-open' : ''} videos`}>
              <div className="modules__list--resume">
                <div className="modules__list--resume-counter">
                  <i
                    className="icon-video_play modules__list--resume-counter-icon"
                  />
                  {this.props.consulting.modulos[this.state.consultingModule].videos.length}
                  {this.props.consulting.modulos[this.state.consultingModule].videos.length > 1 ? ' vídeos' : ' vídeo'}
                </div>
                {/* <div className="modules__list--resume-outlet">
                  <span className="modules__list--resume-outlet-title">Baixar módulo</span>
                  <Switch
                    onChange={this.handleChange}
                    checked={this.state.checked}
                    onColor="#48d852"
                    onHandleColor="#FFF"
                    offColor="#1f1f1f"
                    offHandleColor="#FFF"
                    handleDiameter={20}
                    uncheckedIcon={false}
                    checkedIcon={false}
                    boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                    activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                    height={20}
                    width={35}
                  />
                </div> */}
              </div>
              <div className="videos__list">
                {this.videosList(this.props.consulting.modulos)}
              </div>
              {this.handlingArchives()}
            </div>
          </div>
        </section>
      </React.Fragment >
    );
  }
}

const mapStateToProps = state => {
  return {
    visitor : state.analytics,
    loading: state.consulting.loading,
    loadingMessage: state.messages.loadingChat,
    loadingModal: state.messages.loading,
    messages: state.messages.messages,
    messagesList: state.messages.list,
    questions: state.course.questions,
    loadingRating: state.course.loadingRating,
    success: state.course.rating,
    consulting: state.consulting.consulting,
    tokenMaximize: state.consulting.tokenMaximize,
    user: state.auth.user,
    courseSelected: state.course.courseSelected,
    files: state.containers.videoFiles,
    loadingFiles: state.containers.loadingFiles,
    videoPlaying: state.consulting.videoPlaying
  };
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      setLoading,
      reset,
      setFavoriteConsulting,
      unsetFavoriteConsulting,
      getQuestions,
      rating,
      getFiles,
      getListMessage,
      getMessages,
      sendMessage,
      cleanMessages,
      resetMessages
    },
    dispatch
  )
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Consulting));
