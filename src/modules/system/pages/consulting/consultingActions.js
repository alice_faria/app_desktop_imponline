import types from './consultingTypes';
import axios from 'axios';
import { USER_TOKEN, BASE_API_MATRIZ } from 'config/consts';
import { toastr } from 'react-redux-toastr';
import intl from 'react-intl-universal';
import { errorHandler } from 'helpers/errorHandler';

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.CONSULTING_RESET
    });
  };
};

export const setConsulting = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.CONSULTING_FETCHED,
      payload: data
    });
  };
};

export const loginMaximize = (id_matricula, url) => {
  return async(dispatch) => {
    return axios.get(`${BASE_API_MATRIZ}/api/maximize/${id_matricula}/autenticar`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      window.open(url.replace('[st_token]', response.data.st_token), '_blank');
      dispatch({
        type: types.TOKEN_MAXIMIZE,
        payload: response.data.st_token
      });
    }).catch((err) => {
      toastr.error('Erro!', intl.get('errorRequest.default'));
      dispatch({ type: 'TOKEN_MAXIMIZE_ERROR' });
    });
  };
};

/**
 * @function setFavorite - Função utilizada para evoluir o atributo de favorito do item
 * @returns {Function} Função que altera o atributo de favorito do item
 */
const favoriteConsulting = (values) => {
  return (dispatch) => {
    return axios({
      method: 'post',
      url: `${BASE_API_MATRIZ}/api/favorito-consultoria/registrar`,
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      },
      data: values
    }).then((response) => {
    }).catch((err) => {
      errorHandler(dispatch, err, 'CONSULTING_SET_FAVORITED_ERROR', types.CONSULTING_LOADING);
    });
  };
};

/**
 * @function setFavorite - Função utilizada para evoluir o atributo de favorito do item
 * @returns {Function} Função que altera o atributo de favorito do item
 */
const unFavoriteConsulting = (values) => {
  return (dispatch) => {
    return axios({
      method: 'put',
      url: `${BASE_API_MATRIZ}/api/favorito-consultoria/remover?id_samba_id=${values.id_samba_id}&id_projetopedagogico=${values.id_projetopedagogico}`,
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
    }).catch((err) => {
      errorHandler(dispatch, err, 'CONSULTING_SET_FAVORITED_ERROR', types.CONSULTING_LOADING);
    });
  };
};

export const setFavoriteConsulting = (data, values, type) => {
  return (dispatch) => {
    dispatch({
      type: type,
      payload: data
    });
    dispatch(favoriteConsulting(values));
  };
};

export const unsetFavoriteConsulting = (data, values, type) => {
  return (dispatch) => {
    dispatch({
      type: type,
      payload: data
    });
    dispatch(unFavoriteConsulting(values));
  };
};
