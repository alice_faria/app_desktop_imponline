import types from './consultingTypes';

const initialState = {
  consulting: {},
  tokenMaximize: '',
  loading: false,
  playing: false
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.CONSULTING_FETCHED:
      return {
        ...state,
        consulting: payload || initialState.consulting,
        loading: false,
        videoPlaying:payload.modulos[0].videos[0].id_samba_id
      };
    case types.CONSULTING_LOADING:
      return {
        ...state,
        loading: payload || initialState.loading
      };
    case types.TOKEN_MAXIMIZE:
      return {
        ...state,
        tokenMaximize: payload || initialState.tokenMaximize
      };
    case types.CONSULTING_RESET || 'RESET_APPLICATION':
      return {
        ...state,
        ...initialState
      };
    default:
      return state;
  }
};
