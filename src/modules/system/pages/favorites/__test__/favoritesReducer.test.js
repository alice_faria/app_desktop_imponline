/* eslint-disable no-undefined */
import FavoritesReducers from '../favoritesReducer';
import types from '../favoritesTypes';

describe('Testing Home Reducers', () => {
  const initialState = {
    favorites: [],
    disconnected: false,
    loading: false
  };

  it('should return initial state', () => {
    const action = { type: 'dummy_action' };

    expect(FavoritesReducers(undefined, action)).toEqual(initialState);
  });

  it('should activate loading component', () => {
    const action = { type: types.FAVORITES_LOADING, payload: true };
    const expetedState = { ...initialState, loading: true };

    expect(FavoritesReducers(undefined, action)).toEqual(expetedState);
  });

  it('should deactivate loading component', () => {
    const action = { type: types.FAVORITES_LOADING, payload: false };
    const expetedState = { ...initialState, loading: false };

    expect(FavoritesReducers(undefined, action)).toEqual(expetedState);
  });
  it('should save user token in localStorage and return then', () => {
    const token = '[jwt token]';
    const action = { type: types.FAVORITES_FETCHED, payload: token };

    expect(FavoritesReducers(undefined, action)).toMatchSnapshot();
  });

});
