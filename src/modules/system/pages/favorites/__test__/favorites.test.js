/* eslint-disable no-undefined */
import React from 'react';
import { shallow } from 'enzyme';

import { Favorites as UnconnectedFavorites } from '../favorites';
import { findByTestAtrr } from 'helpers/wrappers';

const setup = (getList, setLoading, favorites, getQuestions, course) => {
  return shallow(<UnconnectedFavorites getList={getList} setLoading={setLoading} favorites={favorites} getQuestions={getQuestions} course={course}/>);
};

//Testing the connected component
describe('Testing Home', () => {
  let wrapper;

  beforeEach(() => {
    const course = {
      title: 'testando',
      projectHash: '6169876d5ac3a7d26e69d4471ef941b7',
      modules: [
        {
          id_matriz: '19',
          st_matriz: 'Atualidades_Urani',
          id_modulomatriz: '52',
          st_modulomatriz: 'Atualidades',
          st_modulo_pos: 'Modulo 1',
          videos: [
            {
              id_samba_id: '0639dd96792bd05455e0320c290b4554',
              st_samba_title: 'América Latina - Exercícios'
            }
          ]
        }
      ]
    };
    const favorites = [{
      id_projetopedagogico: '3622463',
      st_tituloexibicao: 'Senado Federal - Policial Legislativo',
      bl_consultoria: '0',
      id_entidadecadastro: '12',
      st_imagem: 'http://g2evolutiva.unyleya.xyz/upload/projetopedagogico/projeto_pedagogico_36224635d12516baf9ac.jpeg',
      id_coordenador: '195',
      st_coordenador: 'João Trindade Cavalcante Filho',
      st_projecthash: 'ca1269ad-32cc-436a-b465-15bc06d9a54d',
      st_accesstoken: 'ek_test_1HOvaiOvPg1w2VdNSDQtVAqkGs1Nlj',
      st_urlsamba: 'http://api.sambavideos.sambatech.com',
      disciplinas: [
        {
          id_disciplina: '6613',
          st_disciplina: 'Direito Constitucional_João Trindade',
          st_nomeprofessor: 'João Trindade Cavalcante Filho',
          st_imagemprofessor: 'https://conteudo-imp.s3.sa-east-1.amazonaws.com/web/img/fotos/195.png',
          modulos: [
            {
              id_matriz: '3',
              st_matriz: 'Direito Constitucional_ João Trindade',
              id_modulomatriz: '53',
              st_modulomatriz: ' Teoria da Constituição ',
              videos: [
                {
                  id_samba_id: '17b272ed1e11f4b7530fcb8afa6683fa',
                  st_samba_title: 'Aplicabilidade das Normas Constitucionais - Parte I '
                },
                {
                  id_samba_id: '0a8fe013a59e9bf550fbe6ecd2a907b9',
                  st_samba_title: 'Aplicabilidade das Normas Constitucionais - Parte II '
                }
              ],
              st_modulo_pos: 'Modulo 1',
              arquivos: []
            }
          ]
        }]
    }];

    const geList = jest.fn();
    const setLoading = jest.fn();
    const getQuestions = jest.fn();
    wrapper = setup(geList, setLoading, favorites, getQuestions, course);
  });

  it('should render the connected component', () => {
    const component = findByTestAtrr(wrapper, 'favoritesComponent');

    expect(component.length).toBe(1);
  });

  it('should renders all parts', () => {
    expect(wrapper).toMatchSnapshot();
  });

});
