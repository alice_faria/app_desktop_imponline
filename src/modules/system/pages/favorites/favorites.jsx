/* eslint-disable no-fallthrough */
import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import { setLoading } from 'helpers/redux';
import moment from 'moment';
import 'moment/locale/pt-br';

import intl from 'react-intl-universal';
import ReactTooltip from 'react-tooltip';
import Img from 'react-image';
import _ from 'lodash';
import produce from 'immer';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import 'react-dropdown/style.css';

import Loading from 'common/components/ui/loading/loading';
import { getList } from './favoritesActions';
import { getQuestions, rating, setFavorite, unsetFavorite } from '../course/courseActions';
import { setFavoriteConsulting, unsetFavoriteConsulting } from 'modules/system/pages/consulting/consultingActions';
import { resetConsulting } from 'modules/system/pages/home/homeActions';
import { getList as getListMessage, getMessages, sendMessage, cleanMessages, reset as resetMessages } from 'modules/system/pages/messages/messagesActions';
import Carousel from 'common/components/ui/carousel/carousel';
import emptyFavorites from 'assets/images/img_favorite.png';
import disconnected from 'assets/images/img_internet.png';
import types from './favoritesTypes';
import { getFiles, saveProgress } from 'common/containers/containersActions';
import imgTeacherEmpty from 'assets/images/imgMockCourse.png';
import VideoList from 'common/containers/VideoList/VideoList';
import ModuleList from 'common/containers/ModuleList/ModuleList';
import DefaultModal from 'common/components/ui/modals/defaultModal/defaultModal';
import VideoFilesBottom from 'common/containers/videoFiles/videoFilesBottom';
import MessageModal from 'common/components/ui/modals/messageModal/messageModal';
import { getFirstValidElement } from 'helpers/getFirstValidElement';

moment.locale('pt-br');

export class Favorites extends Component {
  constructor(props) {
    super(props);
    this.props.setLoading(types.FAVORITES_LOADING, true);
    this.props.getList();
    this.props.getQuestions();
    this.state = {
      showSidebar: false,
      consulting: [],
      player: false,
      openModules: true,
      updated: false,
      courseTitle: '',
      courseModule: 0,
      courseModulePos: 1,
      courseModulePlaying: null,
      courseClass: 0,
      showModules: false,
      checked: false,
      course: {
        course: '',
        image: '',
        background: '',
        modules: [],
        teacher: '',
        courseID: '',
        videosQtd: null,
        matrizID: null,
        projetoPedagogico: null,
        matricula: null
      },
      courseID: '',
      projetoPedagogico: '',
      indexProjetoPedagogico: null,
      indexSubject: null,
      projectHash: null,
      videoTitle: '',
      showModal: false,
      showModalMessage: false,
      analytics: false,
      closeFiles:true
    };
  }

  componentWillUnmount() {
    // this.props.reset();
    this.props.resetMessages();
  }

  toggleActived = (toggle) => {
    this.setState({ showActived: toggle });
  }

  showModal = () => {
    this.setState({ showModal: true });
  }

  hideModal = () => {
    this.setState({ showModal: false });
  }

  showModalMessage = () => {
    this.setState({ showModalMessage: true });
  }

  hideModalMessage = () => {
    this.setState({ showModalMessage: false });
  }

  selectVideo = (item, index) => {
    this.setState({
      player: true,
      courseModulePlaying: this.state.courseModule,
      courseClass: index,
      videoTitle: item.st_samba_title.trim(),
      videoPlaying:item.id_samba_id
    }, () => this.loadVideo(item.id_samba_id, item.nu_segundos))
  }

  loadVideo = (video_id, resume = 0) => {
    if (document.getElementsByTagName('iframe').length > 0) {
      if (video_id === null) {
        return null;
      } else {
        const videoFrame = document.getElementById('playerST');
        const child = videoFrame.firstChild;
        videoFrame.removeChild(child);
      }
    }
    this.player = new window.SambaPlayer('playerST', { //player é o ID do elemento html que ele vai inserir o iframe
      ph: this.state.projectHash,//Player Hash do projeto
      m: video_id,//MidiaID
      playerParams: { //Veja a lista de Parâmetros suportados
        width: 'auto',
        height: 'auto',
        enableShare: false,
        volume: 50,
        startOutput: '720p',
        html5: true,
        scrolling: 'no',
        frameBorder: '0',
        resume: resume
      },
      events: {
        '*': 'eventListener',
        'onFinish': () => this.autoplay(),
        'onListen': (value) => this.onProgress(value),
        'onStart': (value) => this.sendEvent(value),
        'onPause' :()=>clearInterval(this.state.videoEvent),
        'onResume' : (value) => this.sendEvent(value)
      }
    });
    return undefined;
  };

  sendEvent = (value) => {
    if(!this.state.videoEvent){
      this.setState({
        videoEvent:setInterval(() => {
          this.props.visitor.event("Video", "Assistindo Video").send();
        }, 30000)
      });
    }
  }

  modulesList = (modules) => (
    <ModuleList
      modules={modules}
      onClick={(index, module_pos) => this.changeModule(index, module_pos)}
      courseModule={this.state.courseModule}
    />
  )

  handleChange(checked) {
    this.setState({ checked });
  }

  handleFiles = (id) => {
    this.props.getFiles(id);
  }

  removeVideo = (index, item) => {
    let arr = [...this.state.course.modules];
    arr = [...arr, arr[this.state.courseModule].videos.splice(index, 1)];
    this.setState({
      course: {
        ...this.state.course,
        modules: [
          ...arr
        ]
      }
    });
  }

  videosList = (modules) => (
    <CSSTransition timeout={1} key={this.state.indexSubject * moment()} classNames="favorite__item">
      <VideoList
        modules={modules}
        onClick={this.selectVideo}
        videoPlaying={this.state.videoPlaying}
        loading={this.props.loadingFiles}
        handleFiles={this.handleFiles}
        files={this.props.files}
        courseModule={this.state.courseModule}
        courseModulePos={this.state.courseModulePos}
        courseModulePlaying={this.state.courseModulePlaying}
        courseClass={this.state.courseClass}
        favorite
        id_projetopedagogico={this.state.projetoPedagogico}
        id_disciplina={this.state.courseID}
        course={this.props.course}
        resetConsulting={this.props.resetConsulting}
        setFavorite={this.props.favorites[this.state.indexProjetoPedagogico].bl_consultoria === '1' ? this.props.setFavoriteConsulting : this.props.setFavorite}
        unsetFavorite={this.props.favorites[this.state.indexProjetoPedagogico].bl_consultoria === '1' ? this.props.unsetFavoriteConsulting : this.props.unsetFavorite}
        reduceFavorite={this.props.favorites}
        indexProjetoPedagogico={this.state.indexProjetoPedagogico}
        indexSubject={this.state.indexSubject}
        type={types.FAVORITES_FETCHED}
        setLoading={() => this.props.setLoading(types.FAVORITES_LOADING, true)}
        excludeVideo={(index, item) => this.removeVideo(index, item)}
        disablePlayer={() => this.setState({ player: false })}
        closeSidebar={() => this.setState({
          showSidebar: false,
          courseTitle: '',
          courseModule: 0,
          courseModulePos: 1,
          courseModulePlaying: null,
          courseClass: 0,
          courseID: '',
          projetoPedagogico: '',
          indexProjetoPedagogico: null,
          indexSubject: null,
          projectHash: null,
          videoTitle: '',
          course: {
            course: '',
            image: '',
            background: '',
            modules: [],
            teacher: '',
            courseID: '',
            videosQtd: null,
            matrizID: null,
            projetoPedagogico: null,
            matricula: null
          }
        })}
      />
    </CSSTransition>
  )

  videoNumber = (modules) => {
    if (modules) {
      return modules.reduce(((total, item) => total + item.videos.length), 0);
    }
    return null;
  }

  selectFavorite = (data, item, index, indexSubject) => {
    this.setState({
      courseTitle: data.st_disciplina.includes('_') ? (data.st_disciplina).substring(0, (data.st_disciplina).indexOf('_')) : data.st_disciplina,
      course: {
        course: item.st_tituloexibicao,
        modules: data.modulos,
        background: data.background,
        image: data.st_imagemprofessor,
        teacher: data.st_nomeprofessor,
        videosQtd: this.videoNumber(data.modulos),
        matrizID: null,
        projetoPedagogico: data.id_projetopedagogico,
        matricula: null
      },
      indexProjetoPedagogico: index,
      indexSubject: indexSubject,
      showSidebar: true,
      projectHash: item.st_projecthash,
      courseID: data.id_disciplina,
      projetoPedagogico: item.id_projetopedagogico,
      courseModule: getFirstValidElement(data.modulos)
    });
  }

  handlingCoursesValids = item => {
    if (_.isUndefined(item)) {
      return undefined;
    }
    const disciplinasValid = item.disciplinas.map(disciplina => {
      if (!_.isUndefined(disciplina.modulos) && disciplina.modulos.length > 0) {
        return disciplina;
      }
      return undefined;
    });
    const disciplinasFiltered = disciplinasValid.filter(disciplina => {
      if (!_.isUndefined(disciplina)) {
        return disciplina;
      }
      return undefined;
    });
    return disciplinasFiltered;
  }

  autoplay() {
    const autoplay = JSON.parse(localStorage.getItem('AUTOPLAY')) || false;
    if(autoplay){
      if(this.state.course.modules[this.state.courseModulePlaying].videos.length > this.state.courseClass+1){
        clearInterval(this.state.videoEvent);
        return this.setState({
            courseClass: this.state.courseClass+1,
            videoPlaying: this.state.course.modules[this.state.courseModulePlaying].videos[this.state.courseClass+1].id_samba_id,
            closeFiles: true,
            loadingFiles: false
          },
          () => this.loadVideo(
            this.state.course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].id_samba_id,
            null)
        );
      }
      if(this.state.course.modules[this.state.courseModulePlaying].videos.length === this.state.courseClass + 1){
        clearInterval(this.state.videoEvent);
      }
    }
  }

  renderCourses = () => {
    const { favorites } = this.props;
    let listCourses = [];
    for (const [index, item] of favorites.entries()) {
      const disciplinasFiltered = this.handlingCoursesValids(item);
      if (!_.isUndefined(disciplinasFiltered) && disciplinasFiltered.length > 0) {
        if (item.bl_consultoria === '0') {
          const course = (
            <li className="courses" key={item.id_projetopedagogico} id="carousel">
              <div className="courses-header">
                <span className="courses__title">{item.st_tituloexibicao}</span>
              </div>
              <div className="courses__divider"></div>
              <div className="courses__body">
                <Carousel
                  items={disciplinasFiltered}
                  id={item.id_projetopedagogico}
                  numberItems={0}
                  animationDuration={1000}
                  renderItems={true}
                  classNameNext="courses__carousel--button-next"
                  classNamePrev="courses__carousel--button-prev"
                  infinite={false}
                  slideClass="carousel__slide"
                  autoPlay={false}
                  onClick={(data, indexSubject) => this.selectFavorite(data, item, index, indexSubject)}
                />
              </div>
            </li>
          );
          listCourses = listCourses.concat(course);
        } else {
          const course = (
            <li className="courses" key={item.id_projetopedagogico * moment().format('s')} id="carousel">
              <div className="courses-header--titles">
                <div className="courses-header">
                  <span className="courses__title">{item.st_tituloexibicao}</span>
                </div>
              </div>
              <div className="courses__divider"></div>
              <div className="courses__body">
                <Carousel
                  items={item.disciplinas}
                  id={item.id_projetopedagogico}
                  numberItems={0}
                  animationDuration={1000}
                  renderItems={true}
                  classNameNext="courses__carousel--button-next"
                  classNamePrev="courses__carousel--button-prev"
                  infinite={false}
                  slideClass="carousel__slide"
                  autoPlay={false}
                  onClick={(data, indexSubject) => this.selectFavorite(data, item, index, indexSubject)}
                />
              </div>
            </li>
          );
          listCourses = listCourses.concat(course);
        }
      }
    }
    if (this.props.disconnected) {
      return (
        <div className="emptyHome">
          <img src={disconnected} className="emptyHome__img" alt="empty" />
          <span className="emptyHome__title">{intl.getHTML('errorRequest.connection')}</span>
        </div>
      );
    } else if (_.isEmpty(this.props.favorites) || _.isEmpty(listCourses) || listCourses.every(element => element === null)) {
      return (
        <div className="emptyFavorites">
          <img src={emptyFavorites} className="emptyFavorites__img" alt="empty" />
          <span className="emptyFavorites__title">{intl.getHTML('favorites.emptyPage')}</span>
        </div>
      );
    }
    return listCourses;
  }

  handleModulesSidebar = () => {
    this.setState({
      openModules: !this.state.openModules
    });
  }

  changeModule = (index, module_pos) => {
    this.setState({
      courseModule: index,
      courseModulePos: module_pos
    }, this.handleAccordion);
  }

  handleAccordion = () => {
    this.setState({
      showModules: !this.state.showModules
    });
  }

  renderModulePos = () => {
    if (this.state.course.modules[this.state.courseModule].st_modulo_pos.split(' ')[0] === 'Modulo') {
      return this.state.course.modules[this.state.courseModule].st_modulo_pos.split(' ')[1];
    }
    return this.state.course.modules[this.state.courseModule].st_modulo_pos;
  }

  sidebar = (title) => (
    <React.Fragment>
      <div className="modules-control" onClick={() => this.handleModulesSidebar()}>
        <div className="modules-control__button">
          <i id="control-button" className={`icon-${this.state.openModules ? 'right-arrow' : 'left-arrow'} modules-control__icon`} />
        </div>
      </div>
      <div className={`modules modules__${this.state.openModules ? 'open' : 'close'}`}>
        <div className="modules__course-data">
          <Img
            src={[this.state.course.image, imgTeacherEmpty]}
            className="modules__course-data--image"
            alt="professor"
            style={{ backgroundImage: `url(${this.state.course.background})`, backgroundSize: 'cover' }}
          />
          <div className="modules__course-data--info">
            <span className="modules__course-data--info-course">{this.state.course.course}</span>
            <span className="modules__course-data--info-class">{title}</span>
            <span className="modules__course-data--info-teacher">{this.state.course.teacher}</span>
          </div>
        </div>
        <div className="modules__course-footer">
          <div className="modules__course-footer-group" onClick={this.state.courseID ? () => this.showModalMessage() : null}>
            <i className="icon-comment2 modules__course-footer--icon-chat" />
            <span className="modules__course-footer--title">{intl.get('course.help')}</span>
          </div>
          <div className="vertical-divider modules__divider"></div>
          <div className="modules__course-footer-group" onClick={this.state.courseID ? () => this.showModal() : null}>
            <i className="icon-star modules__course-footer--icon" />
            <span className="modules__course-footer--title">{intl.get('course.rating')}</span>
          </div>
          <div className="vertical-divider modules__divider"></div>
          <div className="modules__course-footer-group" style={{ marginLeft: '-.6rem' }}>
            <i className="icon-video_play modules__course-footer--icon-video" />
            <span className="modules__course-footer--title">
              {this.state.course.videosQtd}
              {parseInt(this.state.course.videosQtd) > 1 ? ' vídeos' : ' vídeo'}
            </span>
          </div>
        </div>
        <div className="modules__list">
          <div className="modules__list--item" onClick={() => this.handleAccordion()}>
            <div className="modules__list--item-number">
              {'Módulo ' + this.props.favorites[this.state.indexProjetoPedagogico].disciplinas[this.state.indexSubject].modulos[this.state.courseModule].st_modulo_pos}
            </div>
            <div className="modules__list--item-group">
              <div className="modules__list--item-title">
                {this.props.favorites[this.state.indexProjetoPedagogico].disciplinas[this.state.indexSubject].modulos[this.state.courseModule].st_modulomatriz.trim()}
              </div>
              <i className={`icon-${!this.state.showModules ? 'down-arrow' : 'up-arrow'} modules__list--item-icon${this.state.showModules ? '-open' : ''}`} />
            </div>
          </div>
          <div className="modules__list--content">
            <div className={`modules__list--accordion modules__list--accordion${this.state.showModules ? '-open' : '-close'}`}>
              {this.state.showModules
                ? this.modulesList(this.props.favorites[this.state.indexProjetoPedagogico].disciplinas[this.state.indexSubject].modulos)
                : null}
            </div>
          </div>
        </div>
        <div className="videos">
          <div className="modules__list--resume">
            <div className="modules__list--resume-counter">
              <i
                className="icon-video_play modules__list--resume-counter-icon"
              />
              {this.state.course.modules[this.state.courseModule].videos.length}
              {this.state.course.modules[this.state.courseModule].videos.length > 1 ? ' vídeos' : ' vídeo'}
            </div>
            <div className="modules__list--resume-outlet">
              {/* <span className="modules__list--resume-outlet-title">Baixar módulo</span>
              <Switch
                onChange={this.handleChange}
                checked={this.state.checked}
                onColor="#48d852"
                onHandleColor="#FFF"
                offColor="#1f1f1f"
                offHandleColor="#FFF"
                handleDiameter={20}
                uncheckedIcon={false}
                checkedIcon={false}
                boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                height={20}
                width={35}
              /> */}
            </div>
          </div>
          <TransitionGroup className="videos__list">
            {this.videosList(this.state.course.modules)}
          </TransitionGroup>
        </div>
      </div>
    </React.Fragment>
  )

  handleUnfavorite = () => {
    const valuesUnset = {
      id_disciplina: this.state.courseID,
      id_samba_id: `${this.state.course.modules[this.state.courseModulePlaying].videos[this.state.courseClass].id_samba_id}`,
      id_projetopedagogico: this.state.projetoPedagogico,
      id_matriz: this.state.course.modules[this.state.courseModule].id_matriz
    };

    const dataFavorite = produce(this.props.favorites, draftState => {
      delete draftState[this.state.indexProjetoPedagogico].disciplinas[this.state.indexSubject].modulos[this.state.courseModule].videos[this.state.courseClass];
      if (_.isEmpty(draftState[this.state.indexProjetoPedagogico].disciplinas[this.state.indexSubject].modulos[this.state.courseModule].videos)) {
        delete draftState[this.state.indexProjetoPedagogico].disciplinas[this.state.indexSubject].modulos[this.state.courseModule];
      }
      if (_.isEmpty(draftState[this.state.indexProjetoPedagogico].disciplinas[this.state.indexSubject].modulos)) {
        delete draftState[this.state.indexProjetoPedagogico];
      }
    });
    this.props.unsetFavorite(dataFavorite, valuesUnset, types.FAVORITES_FETCHED);
  }

  handleClickFiles = (state) =>{
    this.setState({ closeFiles: !this.state.closeFiles, loadingFiles: !this.state.loadingFiles });
  }

  render() {
    if (this.props.loading) {
      return <Loading isVisible={this.props.loading} overlay={true} />;
    }
    const title = this.state.courseTitle;
    return (
      <section style={{ display: 'flex', flexDirection: 'row' }}>
        <DefaultModal
          title={intl.get('modal.title')}
          isVisible={this.state.showModal}
          handleClose={this.hideModal}
          titleClass={title}
          image={this.state.course.image}
          background={this.state.course.background}
          course={this.state.course.course}
          courseID={this.state.courseID}
          teacher={this.state.course.teacher}
          questionsArray={this.props.questions}
          submit={this.props.rating}
          loading={this.props.loadingRating}
          setLoading={this.props.setLoading}
          success={this.props.success}
        />
        <MessageModal
          title={intl.get('messages.modalTitle')}
          isVisible={this.state.showModalMessage}
          handleClose={this.hideModalMessage}
          titleClass={title}
          image={this.state.course.image}
          background={this.state.course.background}
          course={this.state.course.course}
          courseID={this.state.course.courseID}
          teacher={this.state.course.teacher}
          sendMessage={this.props.sendMessage}
          loading={this.props.loadingMessage}
          loadingModal={this.props.loadingModal}
          setLoading={this.props.setLoading}
          messagesList={this.props.messagesList}
          messages={this.props.messages}
          getList={this.props.getListMessage}
          getMessages={this.props.getMessages}
          cleanMessages={this.props.cleanMessages}
          matrizID={this.state.course.matrizID}
          projetoPedagogico={this.state.course.projetoPedagogico}
          id_matricula={this.state.course.matricula}
          reset={this.props.resetMessages}
        />
        <ReactTooltip />
        <div className="favorites" data-test='favoritesComponent'>
          <div className="favorites__header">
            <span className="favorites__header--title">{intl.get('favorites.title')}</span>
          </div>
          {this.state.player
            ? <div className="course__body">
              <div id="playerST" className="course__body--player"></div>
              <div className="course__footer">
                <span className="course__footer--title">
                  {this.state.videoTitle} <i className="icon-bookmark_on course__footer--icon" onClick={() => this.setState({ player: false }, () => this.handleUnfavorite())} />
                </span>
                <div className="course__footer--tags">
                  <div className="course__footer--tags-module">{'Módulo ' + this.state.courseModulePos}</div>
                  <div className="course__footer--tags-class">
                    {intl.get('course.class')} {this.state.videoPlaying.st_aula_pos}
                  </div>
                </div>
                <div className="course__footer--divider"></div>
                {<VideoFilesBottom
                  loading={this.props.loadingFiles}
                  handleFiles={this.handleFiles}
                  videoID={this.state.videoPlaying.id_samba_id}
                  files={this.props.files}
                  handleClick={this.handleClickFiles}
                  close={this.state.closeFiles}
                />}
              </div>
            </div>
            : <div className="favorites__body">
              <ul className="favorites__body--courses">
                {this.renderCourses()}
              </ul>
            </div>
          }
          <div className="modules-control" onClick={() => this.handleModulesSidebar()}>
            <div className="modules-control__button">
              <i id="control-button" className={`icon-${this.state.openModules ? 'right-arrow' : 'left-arrow'} modules-control__icon`} />
            </div>
          </div>
        </div>
        {!this.state.showSidebar
          ? null
          : this.sidebar(title)
        }
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.favorites.loading,
    user: state.auth.user,
    loadingMessage: state.messages.loadingChat,
    loadingModal: state.messages.loading,
    messages: state.messages.messages,
    messagesList: state.messages.list,
    favorites: state.favorites.favorites,
    disconnected: state.favorites.disconnected,
    course: state.course.data,
    home: state.homePage.myClasses.active,
    questions: state.course.questions,
    loadingRating: state.course.loadingRating,
    success: state.course.rating,
    files: state.containers.videoFiles,
    loadingFiles: state.containers.loadingFiles,
    visitor : state.analytics
  };
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      setLoading,
      getList,
      getQuestions,
      setFavoriteConsulting,
      unsetFavoriteConsulting,
      rating,
      setFavorite,
      unsetFavorite,
      getFiles,
      saveProgress,
      getListMessage,
      getMessages,
      sendMessage,
      cleanMessages,
      resetMessages,
      resetConsulting
    },
    dispatch
  )
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Favorites));
