/* eslint-disable no-undefined */
import axios from 'axios';
import _ from 'lodash';
import types from './favoritesTypes';

import { USER_TOKEN, BASE_API_MATRIZ } from 'config/consts';
import { errorHandler } from 'helpers/errorHandler';
import randomImages from 'common/containers/randomImages/randomImages';

const handleVideos = videos => {
  const newVideos = videos.map(item => ({ ...item, st_aula_pos: item.st_aula_pos ? item.st_aula_pos : item.st_aulapos }));
  return newVideos.sort((a, b) => {
    if (parseInt(a.st_aula_pos) > parseInt(b.st_aula_pos)) {
      return 1;
    }
    if (parseInt(a.st_aula_pos) < parseInt(b.st_aula_pos)) {
      return -1;
    }
    // a must be equal to b
    return 0;
  });
};

const handleModules = modules => {
  return modules.map(item => ({
    ...item,
    st_modulo_pos: item.st_modulo_pos ? item.st_modulo_pos : item.st_modulopos,
    videos: handleVideos(item.videos)
  }));
};

const insertBackground = disciplinas => {
  const filterCourse = disciplinas.filter(item => {
    if (!_.isEmpty(item.modulos)) {
      return item;
    }
    return null;
  });
  return filterCourse.map(item => ({
    ...item,
    background: randomImages(item.id_disciplina ? item.id_disciplina : Math.floor(Math.random() * 20)),
    modulos: handleModules(item.modulos, item.st_semana),
    st_semana: item.st_semana
  }));
};

const handleBackground = favorite => {
  const filterFavorite = favorite.filter(item => {
    if (!_.isEmpty(item.disciplinas)) {
      return item;
    }
    return null;
  });
  return filterFavorite.map(item => ({
    ...item,
    disciplinas: insertBackground(item.disciplinas)
  }));
};

/**
 * @function getList - Função utilizada para solicitar os projetos pedagógicos
 * favoritos do usuário
 * @param {Object} values
 * @returns {Function} Função para pegar os projetos pedagógicos do usuário
 */
export const getList = () => {
  return (dispatch) => {
    return axios.get(`${BASE_API_MATRIZ}/api/favorito/meus-favoritos`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.FAVORITES_FETCHED,
        payload: handleBackground(response.data)
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'FAVORITES_MYCLASSES_ERROR', types.FAVORITES_LOADING);
    });
  };
};
