import types from './favoritesTypes';

const initialState = {
  favorites: [],
  disconnected: false,
  loading: false
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.FAVORITES_FETCHED:
      return {
        ...state,
        favorites: payload || initialState.favorites,
        loading: false
      };
    case types.FAVORITES_LOADING:
      return {
        ...state,
        loading: payload || initialState.loading
      };
    case 'ERROR_CONNECTION':
      return {
        ...state,
        disconnected: true
      };
    case 'RESET_APPLICATION':
      return {
        ...state,
        ...initialState
      };
    default:
      return state;
  }
};
