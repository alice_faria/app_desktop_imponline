import packageJson from '../../package.json';

export const SYSTEM_VERSION = packageJson.version; //"1.3.4.3";
export const SYSTEM_NAME = packageJson.name; //"1.3.4.3";

export const USER_KEY = '_UniLeyaUser';
export const USER_TOKEN = '_UniLeyaUserToken';

const HTTP = 'https://';

// URL do ambiente de testes
// const HOST_USERS = 'dev14_api_usuario.unyleya.xyz';
// const HOST_MATRIZ = 'dev14_api_matriz.unyleya.xyz';
// const HOST_G2 = 'dev11g2.unyleya.xyz';

const HOST_USERS = 'api-usuario.unyleya.com.br';
const HOST_MATRIZ = 'api-matriz.unyleya.com.br';
const HOST_COMERCIAL = 'api-comercial.unyleya.com.br';
const HOST_LOJA = 'loja.unyleya.com.br';

// const HOST_USERS = 'api-usuario-testing.unyleya.xyz';
// const HOST_MATRIZ = 'api-matriz-testing.unyleya.xyz';
// const HOST_COMERCIAL = 'api-comercial-testing.unyleya.xyz';
// const HOST_LOJA = 'lojamanutencao.unyleya.xyz';

// const HOST_USERS = 'api_usuario_manutencao.unyleya.xyz';
// const HOST_MATRIZ = 'api-matriz-manutencao.unyleya.xyz';

// const HOST_MATRIZ = 'api-matriz-manutencao.unyleya.xyz';

// const HOST_USERS = 'api_usuario_manutencao.unyleya.xyz';


export const BASE_API_MATRIZ = HTTP + HOST_MATRIZ;
export const BASE_API_COMERCIAL = HTTP + HOST_COMERCIAL;
export const BASE_API = HTTP + HOST_USERS;
export const URL_LOJA = HTTP+HOST_LOJA;
// export const urlMaximize = 'https://impconcursosonline.fabricadeprovas.com.br';
export const urlMaximize = 'https://impconcursosonlineh.fabricadeprovas.com.br';
export const tokenMaximize = 'wWt2r8M4-Fxje-Y676-7qgs-Bpx634Lo';
