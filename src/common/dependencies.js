import './loader';

import 'fastclick/lib/fastclick';
import 'admin-lte/plugins/timepicker/bootstrap-timepicker.min.css';

import 'admin-lte/plugins/jQueryUI/jquery-ui.min';
import 'admin-lte/plugins/timepicker/bootstrap-timepicker.min';
import 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js';

import 'font-awesome/css/font-awesome.min.css';
import 'ionicons/dist/css/ionicons.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'admin-lte/dist/css/AdminLTE.min.css';
import 'admin-lte/dist/css/skins/_all-skins.min.css';
import 'admin-lte/plugins/iCheck/flat/green.css';

import 'admin-lte/dist/js/adminlte.min';
