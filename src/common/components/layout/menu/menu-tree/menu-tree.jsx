import React, { Component } from 'react';

export default class MenuTree extends Component {
  render() {
    const styleColor = (this.props.color ? { color: this.props.color } : {});
    return (
      <li className="treeview">
        <a href={window.location.href}>
          <i className={`icon-${this.props.icon}`} style={styleColor}></i>
          <span style={styleColor}>{this.props.label}</span>
          <span className="pull-right-container">
            <i className="fa fa-angle-left pull-right" style={styleColor}></i>
          </span>
        </a>
        <ul className="treeview-menu">
          {this.props.children}
        </ul>
      </li>
    );
  }
}
