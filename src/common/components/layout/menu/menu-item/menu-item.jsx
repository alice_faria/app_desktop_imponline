import React, { Component } from 'react';

import _ from 'lodash';
import $ from 'jquery';

import { Link, withRouter } from 'react-router-dom';

class MenuItem extends Component {
  handleActive = (location) => {
    if (location === this.props.path) {
      return true;
    } else if (location.split('/')[1] === 'courses' && this.props.path === '/' || location.split('/')[1] === 'consulting' && this.props.path === '/') {
      return true;
    }
    return false;
  }

  render() {
    const location = this.props.location.pathname;
    let open;
    if ($('#bodyRoot').hasClass('sidebar-collapse')) {
      open = false;
    } else {
      open = true;
    }
    return (
      <React.Fragment>
        <li className={this.handleActive(location) ? 'active menu-item' : 'menu-item'}>
          <Link to={this.props.path} className={this.handleActive(location) ? 'active_item' : ''}>
            {!_.isUndefined(this.props.icon) ? (
              <i className={`icon-${this.props.icon} menu-item__icon ${this.handleActive(location) ? 'menu-item__icon-active' : ''}`}/>
            ) : null}
            { open
              ? <span className={`menu-item__label ${this.handleActive(location) ? 'menu-item__label-active' : ''}`}>{this.props.label}</span>
              : null}
          </Link>
        </li>
        <div className={`divider-horizontal${open ? '-open' : ''}`}></div>
      </React.Fragment>
    );
  }
}

export default withRouter(MenuItem);
