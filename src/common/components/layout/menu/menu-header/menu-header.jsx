import React, { Component } from 'react';

export default class MenuHeader extends Component {
  render() {
    return <li className="header">{this.props.name}</li>;
  }
}
