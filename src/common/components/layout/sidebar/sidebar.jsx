import React, { useState, useEffect } from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom';

import logoImp from './img/logo_white.png';
import logoImpMini from 'assets/images/logo_icon_white.png';
import backgroundImp from 'assets/images/grafismo.svg';

const manageClass = (element, classAttr) => {
  if ($(element).hasClass(classAttr)) {
    $(element).removeClass(classAttr);
    return;
  }

  $(element).addClass(classAttr);
};

const Sidebar = props => {
  const [open, setOpen] = useState(true);

  useEffect(() => {
    manageClass('#bodyRoot','sidebar-collapse');
    manageClass('#bodyRoot','sidebar-open');

    manageClass('#contentRoot','content-collapse');
    manageClass('#contentRoot','content-open');

    props.toggleSidebar(open);
  }, [open]);

  return (
    <React.Fragment>
      <aside className="main-sidebar sidebar-collapse" id="bodyRoot">
        <section className="sidebar" >
          <Link to="/" className="logo">
            {open
              ? <span className="sidebar__logo-lg" style={{ marginRight: 44 }}>
                <img src={logoImp} alt="go-to-home" className="sidebar__icon animated-entry--left"></img>
              </span>
              : <span className="sidebar__logo-mini" style={{ marginRight: 44 }}>
                <img src={logoImpMini} alt="go-to-home" className="sidebar__icon-mini animated-entry--right"></img>
              </span>
            }
          </Link>
          {props.children}
        </section>
        <i className={`icon-${open ? 'left' : 'right'}-arrow sidebar__close-icon`} data-toggle='offcanvas' onClick={() => setOpen(!open)} />
        <img src={backgroundImp} alt="sidebar" className="sidebar__image-background" />
      </aside>
    </React.Fragment>
  );
};

export default Sidebar;
