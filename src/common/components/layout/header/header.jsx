import React from 'react';

import Navbar from 'common/components/layout/navbar/navbar';
import SearchBar from 'common/containers/SearchBar/SearchBar';

const Header = props => {

  return (
    <header className="main-header" style={{ zIndex: 10 }}>
      <nav className='navbar navbar-static-top' style={{ height: '4.5rem' }}>
        <SearchBar />
        <Navbar />
      </nav>
    </header>
  );
};

export default Header;
