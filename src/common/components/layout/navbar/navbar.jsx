import React, { useState, useEffect } from 'react';

import TransmissionMenu from 'common/containers/TransmissionMenu/TransmissionMenu';
// import NotificationMenu from 'common/containers/NotificationMenu/NotificationMenu';
import SettingsMenu from 'common/containers/SettingsMenu/SettingsMenu';
import UserMenu from 'common/containers/UserMenu/UserMenu';
import MaximizeMenu from 'common/containers/MaximizeMenu/MaximizeMenu';
// import CastMenu from 'common/containers/CastMenu/CastMenu';

const Navbar = props => {
  const [openTransmission, setopenTransmission] = useState(false);
  const [openSettings, setopenSettings] = useState(false);
  const [openUser, setopenUser] = useState(false);
  const [openCast, setopenCast] = useState(false);
  const [openMaximize, setOpenMaximize] = useState(false);

  useEffect(() => {
    if (openTransmission) {
      setopenSettings(false);
      setopenUser(false);
      setopenCast(false);
    }
  },[openTransmission]);
  useEffect(() => {
    if (openSettings) {
      setopenTransmission(false);
      setopenUser(false);
      setopenCast(false);
    }
  },[openSettings]);
  useEffect(() => {
    if (openUser) {
      setopenTransmission(false);
      setopenSettings(false);
      setopenCast(false);
    }
  },[openUser]);
  useEffect(() => {
    if (openCast) {
      setopenTransmission(false);
      setopenUser(false);
      setopenSettings(false);
    }
  },[openCast]);
  useEffect(() => {
    if (openMaximize) {
      setopenTransmission(false);
      setopenUser(false);
      setopenSettings(false);
    }
  },[openMaximize]);
  return (
    <div className="navbar-custom-menu">
      <ul className="nav navbar-nav">
        {/* <NotificationMenu />
        <div className="vertical-divider"></div> */}
        <MaximizeMenu changeOpen={setOpenMaximize} open={openMaximize}/>
        <div className="vertical-divider"/>
        <TransmissionMenu changeOpen={setopenTransmission} open={openTransmission}/>
        <div className="vertical-divider"/>
        {/* <CastMenu changeOpen={setopenCast} open={openCast}/>
        <div className="vertical-divider"></div> */}
        <SettingsMenu changeOpen={setopenSettings} open={openSettings}/>
        <UserMenu changeOpen={setopenUser} open={openUser}/>
      </ul>
    </div>
  );
};

export default Navbar;
