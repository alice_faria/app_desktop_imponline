import React from 'react';
import PropTypes from 'prop-types';

const toCssClass = (numbers) => {
  const cols = numbers ? numbers.split(' ') : [];
  let classes = '';

  if (cols[0]) {classes += `col-xs-${cols[0]} `;}
  if (cols[1]) {classes += `col-sm-${cols[1]} `;}
  if (cols[2]) {classes += `col-md-${cols[2]} `;}
  if (cols[3]) {classes += `col-lg-${cols[3]} `;}
  if (cols[4]) {classes += `col-xl-${cols[4]} `;}

  return classes;
};

const Grid = (props) => {
  const colClasses = toCssClass(props.cols || '12');

  return (
    <div className={colClasses} style={props.style}>
      {props.children}
    </div>
  );
};

Grid.defaultProps = {
  cols: null,
  style: null
};

Grid.propTypes = {
  cols: PropTypes.string,
  style: PropTypes.object

};

export default Grid;
