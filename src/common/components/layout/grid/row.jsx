import React from 'react';
import propTypes from 'prop-types';

const Row = props => {
	return (
  		<div className="row" {...props}>
  			{props.children}
		</div>
	)
	};

Row.defaultProps = {
	children: null
};

Row.propTypes = {
	children: propTypes.object
};

export default Row;
