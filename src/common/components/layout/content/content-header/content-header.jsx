import React, { Component } from 'react';

export default class ContentHeader extends Component {
  render() {
    return (
      <section className="content-header">
        <h1>
          {this.props.title}&nbsp;
          <small>
            {this.props.small}
          </small>
        </h1>
        {this.props.children}
      </section>
    );
  }
}
