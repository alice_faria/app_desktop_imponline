import propTypes from 'prop-types';

const IF = (props) => {
  if (props.test) {
    return props.children;
  } else {
    return null;
  }
};

IF.defaultProps = {
  test: false,
  children: null
};

IF.propTypes = {
  test: propTypes.bool,
  children: propTypes.object
};

export default IF;
