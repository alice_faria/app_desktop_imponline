import React, { Component } from 'react';
import Proptypes from 'prop-types';
import { animateScroll as scroll } from 'react-scroll';
import types from 'modules/system/pages/course/courseTypes';
import circuloDesconto from 'assets/images/CirculoVermelho.svg';

export default class RenovacaoModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dadosRenovacao: props.dadosRenovacao
    };
    this.changeRating = this.changeRating.bind(this);
  }


  changeRating(item, newValue) {
    this.setState({
      [item]: newValue
    }, item === 'question3' ? () => scroll.scrollToBottom({ smooth: true, containerId: 'rating-scroll' }) : null);
  }

  handleText = (e) => {
    this.setState({
      comment: e.target.value
    });
  }


  submit = () => {
    const courseID = this.props.courseID;
    const values = { ...this.state, courseID };
    this.props.setLoading(types.COURSE_RATING_LOADING, true);
    // this.props.handleClose();
    this.props.submit(values);
  }

  render() {
    const { id, style, isVisible, styleContent} = this.props;
    return (
      <div
        className="modal modal-default fade in"
        id={id}
        style={{
          display: isVisible ? 'flex' : 'none',
          ...style
        }}
      >
        <div className="modal-dialog modal-sm">
          <div className="modal-content" style={{ ...styleContent , paddingBottom: '1rem'}}>
            <div className="modal-header" >
              <h4 className="modal-title center-text" style={{ color: 'white' }}>
                Loja IMP Online
              </h4>
              <i className="icon-close modal-close" onClick={() => this.props.closeModal()} />
            </div>
            <div className="modal--renovacao-body">
              {this.props.dadosRenovacao.map((item, index)=>{
                return(
                  <span className="modal--renovacao-item" key={index}>
                    {item.nu_desconto > 0 && item.valor_cheio > 0?
                      <span className="modal--renovacao-text">
                        <span><span style={{fontSize: '1.7rem'}}>+ {item.nu_dias} dias</span><br/>de acesso</span>
                        <span style={{backgroundImage:"url("+circuloDesconto+")"}} className="modal--renovacao-text-desconto"><span className="modal--renovacao-text-desconto-inside">-{parseInt(item.nu_desconto)+"%"}</span></span><br/>
                        <span className="modal--renovacao-text-valorcheio">R${Number(item.valor_cheio).toFixed(2)}</span><br/>
                        <span style={{fontSize: '1.8rem'}}>R$ {Number(item.valor).toFixed(2)}</span>
                      </span> :
                      <span className="modal--renovacao-text">
                        <span style={{paddingBottom:'4rem'}}><span style={{fontSize: '1.7rem'}}>+ {item.nu_dias} dias</span><br/>de acesso</span>
                        <span style={{fontSize: '1.8rem'}}>R$ {Number(item.valor).toFixed(2)}</span>
                      </span>
                    }
                    <button onClick={() => this.props.efetuarRenovacao(item.id_produto, this.props.dadosRenovacao.id_matricula)} className="modal--renovacao-button">Renovar</button>
                  </span>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

RenovacaoModal.defaultProps = {
  title: 'Modal',
  id: 'default-modal',
  success: false,
  loading: false,
  style: '',
  isVisible: false,
  styleContent: '',
  handleClose: undefined,
  image: '',
  titleClass: undefined,
  course: '',
  teacher: '',
  questions: []
};

RenovacaoModal.propTypes = {
  title: Proptypes.string,
  id: Proptypes.string,
  success: Proptypes.bool,
  loading: Proptypes.bool,
  style: Proptypes.string,
  isVisible: Proptypes.bool,
  styleContent: Proptypes.string,
  handleClose: Proptypes.func,
  image: Proptypes.string,
  titleClass: Proptypes.string,
  course: Proptypes.string,
  teacher: Proptypes.string,
  questions: Proptypes.array.isRequired

};

