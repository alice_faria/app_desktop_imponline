import React, { Component } from 'react';
import imgTeacherEmpty from 'assets/images/imgMockCourse.png';
import Img from 'react-image';
import intl from 'react-intl-universal';
import Proptypes from 'prop-types';
import ScrollToBottom from 'react-scroll-to-bottom';
import _ from 'lodash';
import { Input } from 'react-chat-elements';

import Button from 'common/components/ui/button/button';
import Loading from 'common/components/ui/loading/loading';
import emptyMessages from 'assets/images/img_favorite.png';
import types from 'modules/system/pages/messages/messagesTypes';
import produce from 'immer';
import moment from 'moment';
import { handleMessages } from 'helpers/chatElements';

export default class MessageModal extends Component {
  constructor(props) {
    super(props);
    this.props.setLoading(types.MESSAGES_LIST_LOADING, true);
    this.props.setLoading(types.MESSAGES_LOADING, true);
    this.props.getList();
    this.state = {
      updated: false,
      text: ''
    };
  }

  componentDidUpdate() {
    if (!_.isEmpty(this.props.messagesList) && !this.state.updated) {
      const talk = this.verifyData();
      this.setState({ updated: true },
        _.isUndefined(talk[0]) ? () => this.props.setLoading(types.MESSAGES_LOADING, false) : () => this.props.getMessages(talk[0].id_matriz, talk[0].id_matricula)
      );
    }
  }

  componentWillUnmount() {
    this.props.reset();
  }

  verifyData = () => {
    const talk = this.props.messagesList.filter(item => {
      if (item.id_disciplina === this.props.courseID && item.id_projetopedagogico === parseInt(this.props.projetoPedagogico)) { return item; }
      return null;
    });
    if (!_.isNull(talk)) {
      return talk;
    }
    return undefined;
  }

  closeModal = () => {
    this.setState({ text: '' }, () => { this.props.handleClose(); this.props.reset(); });
  }

  sendMessage = () => {
    const { messages } = this.props;
    let text = this.state.text;
    const data = {
      st_mensagem: text,
      id_matricula: parseInt(this.props.id_matricula),
      id_matriz: parseInt(this.props.matrizID)
    };
    const dataChanged = produce(messages, draftState => {
      draftState.push({
        date: moment(),
        message: text,
        fromMe: true,
        status: 'waiting'
      });
    });
    this.setState({ text: '' }, () => {
      this.refs.input.clear();
      this.props.sendMessage(data, dataChanged);
    });
  }

  handleKeyPressed = () => {
    return this.state.text === ''
      ? null
      : (e) => {
        if (e.shiftKey && e.charCode === 13) {
          return true;
        }
        if (e.charCode === 13) {
          this.refs.input.clear();
          this.sendMessage();
          e.preventDefault();
          return false;
        }
        return null;
      };
  }

  chat = () => {
    const { messages } = this.props;
    if (this.props.loadingModal) {
      return (
        <Loading isVisible={this.props.loading} height={200} width={200} overlay={false} className='modal__messages__chat--loading' />
      );
    }
    return (
      <React.Fragment>
        <ScrollToBottom className="modal__messages__chat--body" refs="scroll">
          {_.isEmpty(messages)
            ? <div className="emptyMessagesModal">
              <img src={emptyMessages} className="emptyMessagesModal__img" alt="empty" />
              <span className="emptyHome__title">{intl.getHTML('messages.emptyPage')}</span>
            </div>
            : handleMessages(messages)
          }
        </ScrollToBottom>
        <Input
          placeholder={intl.get('messages.placeholder')}
          multiline={true}
          ref='input'
          className="modal__messages__chat--footer"
          onChange={(event) => this.setState({ text: event.target.value })}
          onKeyPress={this.handleKeyPressed()}
          value={this.state.text}
          rightButtons={
            this.state.text === ''
              ? null
              : <Button
                color='white'
                backgroundColor='black'
                disabled={this.state.text === ''}
                disabledClass='messages__chat--footer-button-disabled'
                style={{ width: '4rem' }}
                onClick={() => this.sendMessage()}
              >
                <i className="icon-send messages__chat--footer-icon" />
              </Button>
          } />
      </React.Fragment>
    );
  }

  render() {
    const { id, style, title, isVisible, styleContent,
      image, titleClass, course, teacher, loadingModal, background } = this.props;

    return (
      <div className="modal modal-default fade in" id={id} style={{ display: isVisible ? 'flex' : 'none', ...style }}
      >
        <div className="modal-dialog modal-sm">
          <div className="modal-content" style={{ ...styleContent }}>
            <div className="modal-header" >
              <h4 className="modal-title center-text" style={{ color: 'white' }}>{title}</h4>
              <i className="icon-close modal-close" onClick={() => this.closeModal()} />
            </div>
            {!loadingModal
              ? <div className="modal-body">
                <div className="modules__course-data modal-body__course">
                  <Img
                    src={[image, imgTeacherEmpty]}
                    className="modules__course-data--image"
                    alt="professor"
                    style={{ backgroundImage: `url(${background})`, backgroundSize: 'cover' }}

                  />
                  <div className="modules__course-data--info">
                    <span className="modules__course-data--info-course">{course}</span>
                    <span className="modules__course-data--info-class">{titleClass}</span>
                    <span className="modules__course-data--info-teacher">{teacher}</span>
                  </div>
                </div>
                <div className="modal-body__divider"></div>
                <div className="modal__messages__chat">
                  {this.chat()}
                </div>
              </div>
              : <Loading isVisible={true} height={200} width={200} overlay={false} />}
          </div>
        </div>
      </div>
    );
  }
}

MessageModal.defaultProps = {
  title: 'Modal',
  id: 'default-modal',
  success: false,
  loading: false,
  style: '',
  isVisible: false,
  styleContent: '',
  handleClose: undefined,
  image: '',
  titleClass: undefined,
  course: '',
  teacher: '',
  questions: []
};

MessageModal.propTypes = {
  title: Proptypes.string,
  id: Proptypes.string,
  success: Proptypes.bool,
  loading: Proptypes.bool,
  style: Proptypes.string,
  isVisible: Proptypes.bool,
  styleContent: Proptypes.string,
  handleClose: Proptypes.func,
  image: Proptypes.string,
  titleClass: Proptypes.string,
  course: Proptypes.string,
  teacher: Proptypes.string,
  questions: Proptypes.array.isRequired

};

