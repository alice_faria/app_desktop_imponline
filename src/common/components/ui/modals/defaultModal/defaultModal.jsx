import React, { Component } from 'react';
import imgTeacherEmpty from 'assets/images/imgMockCourse.png';
import Img from 'react-image';
import Rating from '@material-ui/lab/Rating';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import intl from 'react-intl-universal';
import Proptypes from 'prop-types';
import { animateScroll as scroll } from 'react-scroll';

import { TextLabel } from 'common/components/form/fields/textLabel/textLabel';
import Button from 'common/components/ui/button/button';
import Loading from 'common/components/ui/loading/loading';
import types from 'modules/system/pages/course/courseTypes';
import modal__sended from 'assets/images/empty__curso.png';

export default class DefaultModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: ''
    };
    this.changeRating = this.changeRating.bind(this);
  }

  componentDidMount() {
    this.props.questions.map(item => {
      this.setState(state => {
        const questions = [...state, { [`question${item.id_avaliacaoqualidadedisciplinapergunta}`]: 0 }];
        return questions;
      });
      return undefined;
    });
  }

  closeModal = () => {
    this.setState({ questions: [], comment: '' }, () => scroll.scrollToTop({ containerId: 'rating-scroll' }), this.props.handleClose());
  }

  changeRating(item, newValue) {
    this.setState({
      [item]: newValue
    }, item === 'question3' ? () => scroll.scrollToBottom({ smooth: true, containerId: 'rating-scroll' }) : null);
  }

  handleText = (e) => {
    this.setState({
      comment: e.target.value
    });
  }

  renderQuestions = (questions) => {
    return questions.map(item => {
      if (item.bl_ativo === 0) {
        return null;
      }
      if (item.bl_comentario === 1) {
        return (
          <li
            className="modal-body__item-text"
            key={`question${item.id_avaliacaoqualidadedisciplinapergunta}`}
            ref={this.refs[item.id_avaliacaoqualidadedisciplinapergunta]}
          >
            <span className="modal-body__item--question">{intl.get('modal.ask')}</span>
            <TextLabel
              placeholder={intl.get('modal.placeholder')}
              className="modal-body__item-text-box"
              onChange={(e) => this.handleText(e)}
              input={{ value: this.state.comment }}
            />
          </li>
        );
      }
      return (
        <li
          className="modal-body__item"
          key={`question${item.id_avaliacaoqualidadedisciplinapergunta}`}
          ref={this.refs[item.id_avaliacaoqualidadedisciplinapergunta]}
        >
          <span className="modal-body__item--question">{item.st_descricaopergunta}</span>
          <Rating
            value={this.state[`question${item.id_avaliacaoqualidadedisciplinapergunta}`]}
            onChange={(event, value) => this.changeRating(event.target.id.split('-')[0], value)}
            emptyIcon={<StarBorderIcon fontSize="inherit" />}
            name={`question${item.id_avaliacaoqualidadedisciplinapergunta}`}
          />
        </li>
      );
    });
  }

  submit = () => {
    const courseID = this.props.courseID;
    const values = { ...this.state, courseID };
    this.props.setLoading(types.COURSE_RATING_LOADING, true);
    // this.props.handleClose();
    this.props.submit(values);
  }

  render() {
    const { id, style, title, isVisible, styleContent,
      image, titleClass, course, teacher, questionsArray, success, loading,
      background } = this.props;

    return (
      <div
        className="modal modal-default fade in"
        id={id}
        style={{
          display: isVisible ? 'flex' : 'none',
          ...style
        }}
      >
        <div className="modal-dialog modal-sm">
          <div className="modal-content" style={{ ...styleContent }}>
            <div className="modal-header" >
              <h4 className="modal-title center-text" style={{ color: 'white' }}>
                {title}
              </h4>
              <i className="icon-close modal-close" onClick={() => this.closeModal()} />
            </div>
            {!success && !loading
              ? <div className="modal-body">
                <div className="modules__course-data modal-body__course">
                  <Img
                    src={[image, imgTeacherEmpty]}
                    className="modules__course-data--image"
                    alt="professor"
                    style={{ backgroundImage: `url(${background})`, backgroundSize: 'cover' }}

                  />
                  <div className="modules__course-data--info">
                    <span className="modules__course-data--info-course">{course}</span>
                    <span className="modules__course-data--info-class">{titleClass}</span>
                    <span className="modules__course-data--info-teacher">{teacher}</span>
                  </div>
                </div>
                <div className="modal-body__divider"></div>
                <ul className="modal-body__list" ref={this.myRef} id="rating-scroll">
                  {this.renderQuestions(questionsArray)}
                  <li>
                    <Button
                      text='Enviar'
                      type="button"
                      style={{ width: '20rem', height: '4rem', padding: 5, marginTop: '0rem' }}
                      rounded
                      disabled={!this.state.question1 || !this.state.question2 || !this.state.question3 || !this.state.question4}
                      buttonType='primary'
                      colors='rgba(204,0,0,1)'
                      radius={8}
                      onClick={() => this.submit()}
                    />˝
                  </li>
                </ul>
              </div>
              : loading
                ? <Loading isVisible={true} overlay={false} height={200} width={200} className='messages__chat--loading' />
                : <div className="modal__sended">
                  <img src={modal__sended} className="modal__sended--img" alt="empty" />
                  <span className="modal__sended--title">{intl.getHTML('modal.send')}</span>
                  <span className="modal__sended--body">{intl.getHTML('modal.sendBody')}</span>
                </div>}
          </div>
        </div>
      </div>
    );
  }
}

DefaultModal.defaultProps = {
  title: 'Modal',
  id: 'default-modal',
  success: false,
  loading: false,
  style: '',
  isVisible: false,
  styleContent: '',
  handleClose: undefined,
  image: '',
  titleClass: undefined,
  course: '',
  teacher: '',
  questions: []
};

DefaultModal.propTypes = {
  title: Proptypes.string,
  id: Proptypes.string,
  success: Proptypes.bool,
  loading: Proptypes.bool,
  style: Proptypes.string,
  isVisible: Proptypes.bool,
  styleContent: Proptypes.string,
  handleClose: Proptypes.func,
  image: Proptypes.string,
  titleClass: Proptypes.string,
  course: Proptypes.string,
  teacher: Proptypes.string,
  questions: Proptypes.array.isRequired

};

