import React, { Component } from 'react';
import Button from '../../button/button';

export default class ErrorModal extends Component {
  render() {
    return (
      <div
        className="modal modal-danger fade in"
        id="modal-danger"
        style={{ display: (this.props.isVisible ? 'block' : 'none'), paddingRight: '1.5rem', ...this.props.style }}
      >
        <div className="modal-dialog modal-sm">
          <div className="modal-content" style={{ borderRadius: 10 }}>
            <div className="modal-header modal-header-danger">
              <h4 className="modal-title center-text">
                <i className={this.props.icon} style={{ fontSize: 64 }}></i>
              </h4>
            </div>
            <div className="modal-body center-text" style={{ paddingTop: 0 }}>
              <h4 className="modal-title center-text" style={{ fontSize: 22 }}>
                {this.props.title}
              </h4>
              <p style={{ fontSize: 16, padding: 10 }} >{this.props.text}</p>
            </div>
            <div className="modal-footer" style={{ display: 'flex', justifyContent: 'center' }}>
              <Button
                onClick={this.props.btnCancel.onClick}
                // colors={"#f1c40f"}
                text={this.props.btnCancel.text}
                rounded
                buttonType='primary'
                textStyle={{ color: '#555' }}
                outline
              />
              <Button
                onClick={this.props.btnOk.onClick}
                text={this.props.btnOk.text}
                colors={'#dd4b39'}
                rounded
                buttonType='primary'
                textStyle={{ color: '#555' }}
                outline
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ErrorModal.defaultProps = {
  title: 'Confirmação de exclusão',
  text: 'Você tem certeza que deseja excluir o item?',
  icon: 'fa fa-trash'
};
