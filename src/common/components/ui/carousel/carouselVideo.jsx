import React from 'react';
import Proptypes from 'prop-types';
import Whirligig from 'react-whirligig';
import _ from 'lodash';
import { withRouter, Redirect } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';
import { capitalizeFirstLetter } from 'helpers/dataTransformers';
import moment from 'moment';
import { Line } from 'rc-progress';

export class CarouselVideos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSlide: 0,
      prevCard: false,
      nextCard: this.props.items.length > 5 ? true : false,
      navigate: undefined
    };
  }

  componentDidMount() {
    const homeWidth = document.getElementById('carousel').offsetWidth;
    const numberOfCards = parseInt(homeWidth / 145);
    this.setState({
      currentSlide: numberOfCards - 2,
      nextCard: this.props.items.length > numberOfCards ? true : false
    });
  }

  componentWillUnmount() {
    clearInterval(this.autoplayInteval);
  }

  handleTeacherName = (teacher) => {
    const teacherArray = teacher.split(' ');
    let teachersCapitalize = [];
    for (const teacher of teacherArray) {
      teachersCapitalize.push(capitalizeFirstLetter(teacher));
    }
    return teachersCapitalize.join(' ');
  }

  handleSubjectName = (name) => {
    if (name && name.includes('_')) {
      return name.substring(0, name.indexOf('_'));
    }
    return name;
  }
  handlePercent = (nu_segundos, nu_duracao) => {
    return parseInt(nu_segundos) * 100 / parseInt(nu_duracao);
  };

  renderSubjects = (subject) => {
    return subject.map((item, index) => {
      const data = {
        video_id: item.id_samba_id,
        module: item.id_modulomatriz,
        video_title: item.st_samba_title,
        video_time: item.nu_segundos
      };
      const secondsStatic = moment.duration(parseInt(item.nu_segundos), 'seconds').format('mm:ss', { trim: false });
      const duration = moment.duration(parseInt(item.nu_duracao), 'seconds').format();
      const percent = this.handlePercent(item.nu_segundos, item.nu_duracao);
      return (
        <div
          className="lastVideos__carousel"
          key={item.id_disciplina * moment().format('s')}
          id="lastVideos-container"
          onClick={this.props.active
            ? this.props.onClick === undefined
              ? () => this.setState({ navigate: `${item.id_matricula}+${item.id_disciplina}+${item.id_projetopedagogico}` },
                () => {
                  this.props.setCourse(item.id_matricula, item.id_disciplina, item.id_projetopedagogico);
                  this.props.videoToOpen(data);
                })
              : () => this.props.onClick(item)
            : null}
        >
          <div className="lastVideos__content">
            <div
              className="videos__list--item-header-play lastVideos__icon"
            >
              <i className="icon-play videos__list--item-header-icon" />
            </div>
            <div className="lastVideos__subtitle">
              <span
                className="lastVideos__subtitle--project"
                data-tip={item.st_projetopedagogico}>{item.st_projetopedagogico}
              </span>
              <span
                className="lastVideos__subtitle--class"
                data-tip={item.st_samba_title.trim()}>{item.st_samba_title.trim().length > 60 ? item.st_samba_title.trim().slice(0, 56) + '...' : item.st_samba_title.trim()}
              </span>
              <span
                className="lastVideos__subtitle--subject"
                data-tip={this.handleSubjectName(item.st_disciplina)}>{this.handleSubjectName(item.st_disciplina)}
              </span>
              <span className="lastVideos__subtitle--teacher">{this.handleTeacherName(item.st_nomeprofessor)}</span>
            </div>
          </div>
          <div className="consulting__footer--divider"></div>
          <div className="videos__list--item-progress">
            <div className="videos__list--item-progress-value">
              {`${secondsStatic}/${duration}`}
            </div>
            <Line percent={String(percent)} className="videos__list--item-progress-bar" strokeWidth="1" strokeColor="#CC0000" trailColor="#FFF" />
          </div>
        </div>
      );
    }
    );
  }

  interval = (autoPlay, duration, next) => {
    if (autoPlay) {
      this.autoplayInteval = setInterval(() => {
        if (this.props.active) {
          return next();
        }
        return null;
      }, duration);
    }
    return null;
  }

  handleAfterSlide = (currentSlide) => {
    if (currentSlide > 0) {
      this.setState({ prevCard: true });
    } else {
      this.setState({ prevCard: false });
    }
  }

  handleRightButton = () => {
    if (this.state.currentSlide >= this.props.items.length) {
      this.setState({ currentSlide: this.props.items.length, nextCard: false });
    }
  }

  render() {
    const arrayItems = this.renderSubjects(this.props.items);
    const { numberItems, autoPlay, animationDuration, infinite,
      customButtons, classNameNext, classNamePrev, duration,
      slideClass } = this.props;
    let whirligig;
    const next = () => [whirligig.next(), this.setState({ currentSlide: this.state.currentSlide + 3 }), this.handleRightButton()];
    const prev = () => [whirligig.prev(), this.setState({ currentSlide: this.state.currentSlide - 3, nextCard: true })];
    const after = (idx) => customButtons ? this.handleAfterSlide(idx) : null;
    if (!_.isUndefined(this.state.navigate)) {
      return <Redirect to={`/${this.props.navigate ? this.props.navigate : 'courses'}/${this.state.navigate}`} push={true} />;
    }
    return (
      <React.Fragment>
        <ReactTooltip />
        <Whirligig
          data-test='carouselComponent'
          visibleSlides={numberItems}
          gutter=".5em"
          infinite={infinite}
          className={slideClass}
          animationDuration={animationDuration}
          preventScroll={true}
          startAt={0}
          slideTo={0}
          beforeSlide={after}
          snapToSlide
          slideBy={2}
          ref={(_whirligigInstance) => { whirligig = _whirligigInstance; }}
        >
          {arrayItems}
        </Whirligig>
        {customButtons
          ? <React.Fragment>
            <button className={`${classNameNext} ${this.state.nextCard ? '' : 'hideToRight'}`} onClick={next}><i className={'icon-right-arrow courses__carousel__icon'} /></button>
            <button className={`${classNamePrev} ${this.state.prevCard ? '' : 'hideToLeft'}`} onClick={prev}><i className={'icon-left-arrow courses__carousel__icon'} /></button>
          </React.Fragment>
          : null}
        {this.interval(autoPlay, duration, next)}
      </React.Fragment>
    );
  }
}

CarouselVideos.defaultProps = {
  items: [],
  dotsDisabled: true,
  buttonsDisabled: true,
  numberItems: 0,
  duration: 1500,
  classNamePrev: '',
  classNameNext: '',
  active: true,
  renderItems: true,
  customButtons: true,
  autoPlay: false,
  itemsWithoutRender: [],
  onClick: undefined
};

CarouselVideos.propTypes = {
  items: Proptypes.array.isRequired,
  numberItems: Proptypes.number,
  duration: Proptypes.number,
  classNamePrev: Proptypes.string,
  classNameNext: Proptypes.string,
  active: Proptypes.bool,
  renderItems: Proptypes.bool,
  customButtons: Proptypes.bool,
  autoPlay: Proptypes.bool,
  itemsWithoutRender: Proptypes.array,
  onClick: Proptypes.func
};

export default withRouter(CarouselVideos);
