import React from 'react';
import { shallow } from 'enzyme';
import { Carousel } from './carousel';
import { findByTestAtrr } from 'helpers/wrappers';

describe('Carousel component', () => {
  it('should render correctly with no props', () => {
    const component = shallow(<Carousel />);
    expect(component).toMatchSnapshot();
  });

  it('should render the connected component', () => {
    const wrapper = shallow(<Carousel />);
    const component = findByTestAtrr(wrapper, 'carouselComponent');
    expect(component.length).toBe(1);
  });
});
