import React from 'react';
import Proptypes from 'prop-types';
import Whirligig from 'react-whirligig';
import _ from 'lodash';
import { withRouter, Redirect } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';
import { capitalizeFirstLetter } from 'helpers/dataTransformers';
import imgTeacherEmpty from 'assets/images/imgMockCourse.png';
import Img from 'react-image';
import intl from 'react-intl-universal';
import { Circle } from 'rc-progress';
import moment from 'moment';

export class Carousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSlide: 0,
      prevCard: false,
      nextCard: this.props.items.length > 5 ? true : false,
      navigate: undefined
    };
  }

  componentDidMount() {
    const homeWidth = document.getElementById('carousel').offsetWidth;
    const numberOfCards = parseInt(homeWidth / 145);
    this.setState({
      currentSlide: numberOfCards - 2,
      nextCard: this.props.items.length > numberOfCards ? true : false
    });
  }

  componentWillUnmount() {
    clearInterval(this.autoplayInteval);
  }

  handleTeacherName = (teacher) => {
    const teacherArray = teacher.split(' ');
    let teachersCapitalize = [];
    for (const teacher of teacherArray) {
      teachersCapitalize.push(capitalizeFirstLetter(teacher));
    }
    return teachersCapitalize.join(' ');
  }

  handleSubjectName = (name) => {
    if (name && name.includes('_')) {
      return name.substring(0, name.indexOf('_'));
    }
    return name;
  }

  renderSubjects = (subject) => {
    return subject.map((item, indexSubject) => (
      <div
        className="courses__carousel"
        key={item.id_disciplina ? item.id_disciplina * moment().format('s') : Math.floor(Math.random() * 100000)}
        id="courses-container"
        onClick={this.props.active
          ? this.props.onClick === undefined
            ? () => this.setState({ navigate: `${this.props.matricula}+${item.id_disciplina}+${this.props.id}` },
              () => this.props.setCourse(this.props.matricula, item.id_disciplina, this.props.id))
            : () => this.props.onClick(item, indexSubject)
          : null}
      >
        <Img
          src={[item.st_imagemprofessor, imgTeacherEmpty]}
          className={`courses__img ${this.props.active ? '' : 'courses__img-deactived'}`}
          alt={item.st_nomeprofessor}
          style={{ backgroundImage: `url(${item.background})`, backgroundSize: 'cover' }}
        />
        {item.nu_percentualassistido
          ? <div className="courses__carousel--complete">
            <Circle percent={item.nu_percentualassistido} strokeWidth="8" strokeColor="#FFF" trailWidth="7" trailColor="rgba(255, 255, 255, 0.4)" />
            <span className="courses__carousel--complete-percent">{parseInt(item.nu_percentualassistido)} %</span>
          </div>
          : null}
        {!this.props.active
          ? <div className="courses__img--title">{intl.get('home.inative')}</div>
          : null}
        <div className="courses__subtitle">
          <span
            className="courses__subtitle--subject"
            data-tip={this.handleSubjectName(item.st_disciplina)}>{this.handleSubjectName(item.st_disciplina)}
          </span>
          <span className="courses__subtitle--teacher">{this.handleTeacherName(item.st_nomeprofessor)}</span>
        </div>
      </div>
    ));
  }

  interval = (autoPlay, duration, next) => {
    if (autoPlay) {
      this.autoplayInteval = setInterval(() => {
        if (this.props.active) {
          return next();
        }
        return null;
      }, duration);
    }
    return null;
  }

  handleAfterSlide = currentSlide => {
    if (currentSlide > 0) {
      this.setState({ prevCard: true });
    } else {
      this.setState({ prevCard: false });
    }
  }

  handleRightButton = () => {
    if (this.state.currentSlide >= this.props.items.length) {
      this.setState({ currentSlide: this.props.items.length, nextCard: false });
    }
  }

  render() {
    const arrayItems = this.renderSubjects(this.props.items);
    const { numberItems, autoPlay, animationDuration, infinite,
      customButtons, classNameNext, classNamePrev, duration,
      slideClass } = this.props;
    let whirligig;
    const next = () => [whirligig.next(), this.setState({ currentSlide: this.state.currentSlide + 3 }), this.handleRightButton()];
    const prev = () => [whirligig.prev(), this.setState({ currentSlide: this.state.currentSlide - 3, nextCard: true })];
    const after = (idx) => customButtons ? this.handleAfterSlide(idx) : null;
    if (!_.isUndefined(this.state.navigate)) {
      return <Redirect to={`/${this.props.navigate ? this.props.navigate : 'courses'}/${this.state.navigate}`} push={true} />;
    }

    return (
      <React.Fragment>
        <ReactTooltip />
        <Whirligig
          data-test='carouselComponent'
          visibleSlides={numberItems}
          gutter=".5em"
          infinite={infinite}
          className={slideClass}
          animationDuration={animationDuration}
          preventScroll={true}
          startAt={0}
          slideTo={0}
          beforeSlide={after}
          snapToSlide
          slideBy={3}
          ref={(_whirligigInstance) => { whirligig = _whirligigInstance; }}
        >
          {arrayItems}
        </Whirligig>
        {customButtons
          ? <React.Fragment>
            <button className={`${classNameNext} ${this.state.nextCard ? '' : 'hideToRight'}`} onClick={next}><i className={'icon-right-arrow courses__carousel__icon'} /></button>
            <button className={`${classNamePrev} ${this.state.prevCard ? '' : 'hideToLeft'}`} onClick={prev}><i className={'icon-left-arrow courses__carousel__icon'} /></button>
          </React.Fragment>
          : null}
        {this.interval(autoPlay, duration, next)}
      </React.Fragment>
    );
  }
}

Carousel.defaultProps = {
  items: [],
  dotsDisabled: true,
  buttonsDisabled: true,
  numberItems: 0,
  duration: 1500,
  classNamePrev: '',
  classNameNext: '',
  active: true,
  renderItems: true,
  customButtons: true,
  autoPlay: false,
  itemsWithoutRender: [],
  onClick: undefined
};

Carousel.propTypes = {
  items: Proptypes.array.isRequired,
  numberItems: Proptypes.number,
  duration: Proptypes.number,
  classNamePrev: Proptypes.string,
  classNameNext: Proptypes.string,
  active: Proptypes.bool,
  renderItems: Proptypes.bool,
  customButtons: Proptypes.bool,
  autoPlay: Proptypes.bool,
  itemsWithoutRender: Proptypes.array,
  onClick: Proptypes.func
};

export default withRouter(Carousel);
