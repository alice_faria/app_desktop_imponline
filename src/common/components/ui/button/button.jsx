import React, { Component } from 'react';

import _ from 'lodash';

class Button extends Component {

    getStyle = () => {
      const {
        rounded, radius, outline, colors, gradientBegin,
        style
      } = this.props;
      const customButtonStyle = { ...style };
      const customTextStyle = {};

      if (rounded) {customButtonStyle['borderRadius'] = radius;}
      if (outline) {
        customButtonStyle['border'] = '.2rem solid';
        customButtonStyle['background'] = 'transparent';
        if (_.isUndefined(colors)) {
          customButtonStyle['borderColor'] = gradientBegin;
          customTextStyle['color'] = gradientBegin;
        } else if (_.isString(colors)) {
          customButtonStyle['borderColor'] = colors;
          customTextStyle['color'] = colors;
        } else {
          customButtonStyle['borderColor'] = colors[0];
          customTextStyle['color'] = colors[0];
        }
      }

      return { customButtonStyle, customTextStyle };
    }

    render() {
      const {
        onClick, children, text, textStyle, disabled, disabledClass,
        outline, buttonType, type, animated
      } = this.props;
      const { customButtonStyle, customTextStyle } = this.getStyle();
      return (
        <button
          className={
            `button ${animated ? 'button--animated' : ''}
            button${buttonType ? '__' + buttonType : ''}
            ${outline ? 'button--outline' : ''}
            ${disabled ? disabledClass ? disabledClass : 'disabled' : ''}`}
          style={customButtonStyle}
          disabled={disabled}
          onClick={onClick}
          type={type || 'button'}
        >
          {text ? <span className={`button__text${disabled ? '--disabled' : ''}`} style={{ ...customTextStyle,...textStyle }}>{text}</span> : children}
        </button>
      );
    }
}

Button.defaultProps = {
  disabled: false,
  gradientBegin: '#C21F35',
  gradientEnd: '#BE0304',
  gradientDirection: 'diagonal',
  height: 50,
  radius: 50
};

export default Button;
