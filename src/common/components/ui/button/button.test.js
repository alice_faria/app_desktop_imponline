import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import Button from './button';

describe('Button component', () => {
  it('should render correctly with no props', () => {
    const component = shallow(<Button />);
    expect(component).toMatchSnapshot();
  });

  it('matches the snapshot', () => {
    const tree = renderer.create(<Button />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
