import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Lottie from 'react-lottie';

import LoadingData from 'assets/animations/loading.json';

import './loading.scss';
import IF from '../operators/if';

class Loading extends Component {

  render() {
    const { overlay, className, width, height } = this.props;
    return (
      <IF test={this.props.isVisible}>
        <div className={`loading ${overlay === false ? 'loading__no-overlay' : ''} ${className ? className : ''}`}>
          <div className="loading__mask" />
          <Lottie
            options={this.props.defaultOptions}
            height={height}
            width={width}
            isStopped={this.props.isStopped}
            isPaused={this.props.isPaused}
            speed={1.2}
          />
        </div>
      </IF>
    );
  }
}

Loading.defaultProps = {
  className: '',
  overlay: false,
  isVisible: false,
  isStopped: false,
  isPaused: false,
  defaultOptions: {
    loop: true,
    autoplay: true,
    animationData: LoadingData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  },
  height: 300,
  width: 300
};

Loading.propTypes = {
  className: PropTypes.string,
  overlay: PropTypes.bool,
  isVisible: PropTypes.bool,
  isStopped: PropTypes.bool,
  isPaused: PropTypes.bool,
  defaultOptions: PropTypes.object,
  height: PropTypes.number,
  width: PropTypes.number
};

export default Loading;
