import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import Loading from './loading';

describe('Loading component', () => {
  it('should render correctly with no props', () => {
    const component = shallow(<Loading />);
    expect(component).toMatchSnapshot();
  });

  // it("control the visible props", () => {
  //     const props = {
  //         isVisible: true
  //     }
  //     const component = shallow(<Loading {...props}/>);
  //     expect(component)
  // })

  it('matches the snapshot', () => {
    const tree = renderer.create(<Loading />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
