import React from 'react';

import Grid from 'common/components/layout/grid/grid';

export class SearchLabel extends React.PureComponent {
	state = {
	  type: this.props.type
	}

	changeType = () => {
	  if (this.state.type === this.props.iconChangeType[0]) {
	    this.setState({
	      type: this.props.iconChangeType[1]
	    },() => this.props.iconClick());
	  } else {
	    this.setState({
	      type: this.props.iconChangeType[0]
	    },() => this.props.iconClick());
	  }
	}

	render() {
	  const {
	    meta: { touched, error },
	    inputClass, iconStyle,
	    reference, noHelpBlock
	  } = this.props;
	  const iconSide = (this.props.iconRight ? { right: 10 } : { left: 0 });
	  const topLabel = (this.props.label ? { top: 70 } : {});
	  return (
	    <Grid cols={this.props.cols} style={{ paddingBottom: '1.2rem', ...this.props.style, paddingLeft: 0,
	      paddingRight: 0 }}>
	      <div
	        className={`form-group ${touched && error && 'has-error'}`}
	      >
	        {this.props.label
	          ? touched && error ? (
	            <label className="control-label">
	              <i className="fa fa-times-circle-o" />
								&nbsp;
	              {this.props.label}
	            </label>
	          ) : (
	            <label htmlFor={this.props.name}>
	              {this.props.label}
	            </label>
	          )
	          : null
	        }
	        {this.props.icon
	          ? (
	            <div style={{ position: 'relative' }}>
	              <input
	                {...this.props.input}
	                ref={reference}
	                style={{ ...this.props.inputStyle }}
	                className={'form-control ' + inputClass}
	                placeholder={this.props.placeholder}
	                readOnly={this.props.readOnly}
	                type={this.state.type}
	              />
	              <i
	                className={this.props.icon + ' searchBar-icon'}
	                onClick={this.props.iconChangeType ? this.changeType : this.props.iconOnClick}
	                style={{ ...iconSide, ...iconStyle }}
	              ></i>
	            </div>
	          )
	          : (
	            <input
	              {...this.props.input}
	              ref={reference}
	              style={this.props.inputStyle}
	              className={'form-control ' + inputClass}
	              placeholder={this.props.placeholder}
	              readOnly={this.props.readOnly}
	              type={this.props.type}
	            />
	          )
	        }
	        {touched && error && !noHelpBlock && (
	          <span className="help-block" style={topLabel}>{error}</span>
	        )}
	      </div>
	    </Grid>
	  );
	}
}
