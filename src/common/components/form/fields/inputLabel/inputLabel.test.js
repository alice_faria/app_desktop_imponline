import React from 'react';
import { mount } from 'enzyme';
import { create } from 'react-test-renderer';
import { InputLabel } from './inputLabel';

const defaultProps = {
  meta: {
    touched: null,
    error: null
  },
  input: {
    name: 'field-name'
  },
  inputComponent: () => { return 'test case'; }
};
const BaseFieldLayout = (props) => <InputLabel {...defaultProps} {...props} />;

describe('InputLabel component', () => {
  it('render correctly BaseFieldLayout component', () => {
    const BaseFieldLayoutComponent = create(<BaseFieldLayout />).toJSON();
    expect(BaseFieldLayoutComponent).toMatchSnapshot();
  });

  it('check label prop is rendered correctly', () => {
    const props = {
      label: 'custom label'
    };
    const BaseFieldLayoutComponent = mount(<BaseFieldLayout {...props} />);
    expect(BaseFieldLayoutComponent.find('label').hasClass('field__label')).toBeTruthy();
  });

  it('matches the snapshot', () => {
    const tree = create(<InputLabel />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
