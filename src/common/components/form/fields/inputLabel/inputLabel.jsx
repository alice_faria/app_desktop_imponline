import React from 'react';
import PropTypes from 'prop-types';
import Grid from 'common/components/layout/grid/grid';
import _ from 'lodash';

export class InputLabel extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
      value: ''
    };
  }

  renderBottom = (touched, error, isFocused, value) => {
    if (touched && error) {
      if (!isFocused && _.isEmpty(value)) { return '2.4rem'; } else { return '4.4rem'; }
    } else { if (!isFocused && _.isEmpty(value)) {return '.8rem';} else { return '2.8rem'; }}
  }

  renderBottomWithIcon = (touched, error, isFocused, value) => {
    if (touched && error) {
      if (!isFocused && _.isEmpty(value)) { return '2.8rem'; } else { return '4.8rem'; }
    } else if (!isFocused && _.isEmpty(value)) {return '1.2rem';} else { return '3.2rem'; }
  }

  render() {
    const { meta: { touched, error }, inputClass, iconStyle, reference, isFocused, autoFocus } = this.props;
    const iconSide = this.props.iconRight ? { right: 12.2 } : { left: 0 };
    const { value } = this.props.input;
    const labelStyle = {
      position: 'absolute',
      left: '.85rem',
      bottom: this.renderBottom(touched, error, isFocused, value)
    };
    return (
      <Grid cols={this.props.cols} style={{ ...this.props.style }}>
        <div className={`form-group ${touched && error && 'has-error'}`}>
          {this.props.icon ? (
            <div style={{ position: 'relative' }}>
              <label className={'field__label'} style={labelStyle} htmlFor={this.props.name}>{this.props.label}</label>
              <input
                {...this.props.input}
                name={this.props.name}
                id={this.props.name}
                disabled={this.props.disabled}
                ref={reference}
                style={this.props.inputStyle}
                className={'form-control ' + inputClass}
                placeholder={this.props.placeholder}
                readOnly={this.props.readOnly}
                type={this.props.type}
                autoFocus={autoFocus}
              />
              <i
                className={this.props.icon + ' input--icon'}
                onClick={this.props.iconChangeType ? this.changeType : this.props.iconClick}
                style={{ ...iconSide, ...iconStyle }}
              />
            </div>
          ) : (
            <React.Fragment>
              <label
                className="field__label"
                style={{
                  ...labelStyle,
                  bottom: this.renderBottomWithIcon(touched, error, isFocused, value)
                }}
                htmlFor={this.props.name}>
                {this.props.label}
              </label>
              <input
                {...this.props.input}
                name={this.props.name}
                id={this.props.name}
                disabled={this.props.disabled}
                ref={reference}
                style={this.props.inputStyle}
                className={'form-control ' + inputClass}
                placeholder={this.props.placeholder}
                readOnly={this.props.readOnly}
                type={this.props.type}
                autoFocus={autoFocus}
              />
            </React.Fragment>
          )}
          {touched && error && (
            <span className="help-block">{error}</span>
          )}
        </div>
      </Grid>
    );
  }
}
InputLabel.defaultProps = {
  cols: null,
  readOnly: false,
  meta: {},
  input: {}
};

InputLabel.propTypes = {
  cols: PropTypes.string,
  readOnly: PropTypes.bool,
  meta: PropTypes.object,
  input: PropTypes.object,
  required: PropTypes.bool
};
