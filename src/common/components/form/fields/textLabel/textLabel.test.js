/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { TextLabel } from './textLabel';

describe('TextLabel component', () => {
  it('should render correctly with no props', () => {
    const component = shallow(<TextLabel />);
    expect(component).toMatchSnapshot();
  });

  // it('Should change value when onChange was called', () => {
  //     const onChange = jest.fn();
  //     const props = {
  //         label: 'Test Label',
  //         type: 'text',
  //         input: {
  //           onChange
  //         },
  //         value: 'Hello world',
  //         meta: {
  //             touched: true,
  //             error: 'error'
  //         }
  //     }
  //     const wrapper = mount(<TextLabel {...props}/>);
  //     const event = {
  //             target: {
  //                 value: 'This is just for test'
  //             }
  //         }
  //     wrapper.find('textarea').simulate('change', event)
  //     expect(onChange).toHaveBeenCalledWith('This is just for test');
  // })

  // it("control the visible props", () => {
  //     const props = {
  //         isVisible: true
  //     }
  //     const component = shallow(<TextLabel {...props}/>);
  //     expect(component)
  // })

  it('matches the snapshot', () => {
    const tree = renderer.create(<TextLabel />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
