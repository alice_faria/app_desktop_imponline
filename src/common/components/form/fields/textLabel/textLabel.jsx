import React from 'react';

import PropTypes from 'prop-types';
import Grid from 'common/components/layout/grid/grid';

export class TextLabel extends React.PureComponent {
  render() {
    const { meta: { touched, error } } = this.props;
    return (
      <Grid cols={this.props.cols} style={this.props.style}>
        <div className={`form-group ${touched && error && 'has-error'}`}>
          {touched && error ? (
            <label className="control-label">
              <i className="fa fa-times-circle-o" />
							&nbsp;
              {this.props.label}
            </label>
          ) : (
            <label htmlFor={this.props.name}>{this.props.label}</label>
          )}
          <textarea
            {...this.props.input}
            style={this.props.inputStyle}
            className={this.props.className ? this.props.className : 'form-group'}
            placeholder={this.props.placeholder}
            readOnly={this.props.readOnly}
            type={this.props.type}
            onChange={this.props.onChange}
          >
            {this.props.input.value}
          </textarea>
          {touched && error && <span className="help-block">{error}</span>}
        </div>
      </Grid>
    );
  }
}

TextLabel.defaultProps = {
  cols: null,
  readOnly: false,
  label: '',
  type: '',
  meta: {},
  input: {}
};

TextLabel.propTypes = {
  cols: PropTypes.string,
  readOnly: PropTypes.bool,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  input: PropTypes.shape({}).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string
  }).isRequired
};
