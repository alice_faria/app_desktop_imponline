import React from 'react';
import ReduxToastr from 'react-redux-toastr';

const Message = () => (
  <ReduxToastr
    timeOut={4000}
    newestOnTop={true}
    preventDuplicates={true}
    position="top-right"
    transitionOut="fadeOut"
    progressBar
  />
);

export default Message;
