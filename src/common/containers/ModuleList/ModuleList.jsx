import React from 'react';
import intl from 'react-intl-universal';
import _ from 'lodash';

export default (props) => {
  return props.modules.map((item, index) => {
    if (_.isNull(item)) {
      return undefined;
    }
    return (
      <div className="modules__list--item-accordion" onClick={() => props.onClick(index, item.st_modulo_pos)} key={index}>
        {props.courseModule === index
          ? <div className="modules__list--item-circle"></div>
          : null}
        <div className="modules__list--item-group">
          <div className="modules__list--item-number">{intl.get('course.module')} {item.st_modulo_pos}</div>
          <div className="modules__list--item-count">
            <i className="icon-video_play modules__list--item-count-icon" />{item.videos.length} {item.videos.length > 1 ? ' vídeos' : ' vídeo'}
          </div>
        </div>
        <div className="modules__list--item-title" data-tip={item.st_modulomatriz.trim()}>
          {item.st_modulomatriz.trim().length > 50 ? item.st_modulomatriz.trim().slice(0, 43) + '...' : item.st_modulomatriz.trim()}
        </div>
      </div>
    )
  });
};
