import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import intl from 'react-intl-universal';
import Img from 'react-image';
import { verifyTransmission, getTransmission } from '../containersActions';
import ReactTooltip from 'react-tooltip';
import moment from 'moment';
import _ from 'lodash';
import Loading from 'common/components/ui/loading/loading';

moment.locale('pt-br');

export class TransmissionMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      transmission: true,
      showUpcoming: true
    };
    this.props.verifyTransmission();
    this.props.getTransmission();
  }

  componentDidMount() {
    this.setState({
      transmission: this.props.hasTransmission
    });
  }

  toggleActived = (toggle) => {
    this.setState({ showUpcoming: toggle });
  }

  handleDate = (date) => {
    const diffDate = moment(date).fromNow();
    return diffDate;
  }

  renderTransmissions = (data) => {
    return data.map((item, index) => (
      <li className="transmission-menu__list--item" key={index}>
        <div className="transmission-menu__list--item-content">
          <Img src={item.thumbnail} className="transmission-menu__list--item-image" />
          <div className="transmission-menu__list--item-data">
            <span className="transmission-menu__list--item-title" data-tip={item.title}>{item.title}</span>
            <span className="transmission-menu__list--item-time">
              {this.state.showUpcoming ? intl.get('transmission.upcomingTransmission') : this.handleDate(item.date)}
            </span>
          </div>
        </div>
        <div className="transmission-menu__list-divider"></div>
        <a target="_blank" rel="noopener noreferrer" href={`https://www.youtube.com/watch?v=${item.videoId}`} className="transmission-menu__list-footer">
          {intl.get('transmission.watchTransmission')}
          <i className="icon-youtube transmission-menu__list-footer-play" />
        </a>
      </li>
    ));
  }

  handleOpen = () => {
    if (this.props.open) {
      return 'open';
    }
    return '';
  }

  handleActive = (inversed = false) => {
    if (inversed) {
      if (this.state.showUpcoming) {
        return '';
      }
      return '-active';
    }
    if (this.state.showUpcoming) {
      return '-active';
    }
    return '';
  }

  render() {
    const { live, upcoming, none } = this.props.transmissions;
    const data = this.state.showUpcoming ? upcoming : none;
    return (
      <li
        className={`dropdown transmission-menu ${this.handleOpen()}`}
        data-test="TransmissionComponent"
      >
        <ReactTooltip />
        <span
          onClick={() => this.props.open ? this.props.changeOpen(false) : this.props.changeOpen(true)}
          aria-expanded={'false'}
          className={`transmission-menu__dropdown-toggle${this.props.open ? '-open' : ''}`}
          data-toggle="dropdown"
          data-tip="Abrir Transmissões"
          data-effect="solid"
        >
          <i className="icon-signal transmission-menu__icon" />
          <div className={`transmission-menu__icon${this.state.transmission ? '-on' : '-off'}`}></div>
        </span>
        {_.isEmpty(this.props.transmissions)
          ? <Loading isVisible={this.props.loading} overlay={true} />
          : <ul className={`transmission-menu__dropdown-menu ${this.handleOpen()}`}>
            {this.state.transmission
              ? <li className="transmission-menu__thumb">
                <Img src={'https://i.ytimg.com/vi/AqxZngWH3yE/hqdefault.jpg'} alt="user" className="transmission-menu__image" />
                <div className="transmission-menu__thumb--tag">
                  <div className="transmission-menu__thumb--tag-icon"></div>
                  {intl.get('transmission.live')}
                </div>
                <a target="_blank" rel="noopener noreferrer" href={`https://www.youtube.com/watch?v=${live[0].videoId}`}>
                  <i className="icon-youtube transmission-menu__thumb--tag-play" />
                </a>
                <Img src={this.props.transmissions.live[0].thumbnail} alt="user" className="transmission-menu__image" />
                <div className="transmission-menu__thumb-title">
                  <span className="transmission-menu__thumb-title-name" data-tip={live[0].title}>{live[0].title}</span>
                  <span className="transmission-menu__thumb-title-subname" data-tip={live[0].subtitle}>{live[0].subtitle}</span>
                </div>
              </li>
              : null}
            <li className="transmission-menu__list" >
              <div className="transmission-menu__list--header">
                <span className="transmission-menu__list--header-title">{intl.get('transmission.title')}</span>
                <span className={`transmission-menu__list--header--select${this.handleActive(true)}`}>
                  <span
                    className={`transmission-menu__list--header--select-label${this.handleActive()}`}
                    onClick={() => this.toggleActived(true)}
                  >
                    {intl.get('transmission.upcoming')}
                  </span>
                  <span
                    className={`transmission-menu__list--header--select-label${this.handleActive(true)}`}
                    onClick={() => this.toggleActived(false)}
                  >
                    {intl.get('transmission.none')}
                  </span>
                </span>
              </div>
              <ul className={`transmission-menu__list--body${this.state.transmission ? '' : '-no'}`}>
                {this.renderTransmissions(data)}
                <li className="transmission-menu__footer">
                  <a
                    target="_blank" rel="noopener noreferrer" href={'https://www.youtube.com/channel/UCDpWPw86wtFNIZZdnfi7_Ng'}
                    className="transmission-menu__footer--watchMore"
                  >
                    {intl.get('transmission.watchMore')}
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        }
      </li>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.containers.loading,
    transmissions: state.containers.transmission,
    hasTransmission: state.containers.hasTransmission
  };
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      verifyTransmission,
      getTransmission
    },
    dispatch
  )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TransmissionMenu);
