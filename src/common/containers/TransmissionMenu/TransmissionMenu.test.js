/* eslint-disable no-undefined */
import React from 'react';
import { shallow } from 'enzyme';

import { TransmissionMenu as UnconnectedTransmissionMenu } from './TransmissionMenu';
import { findByTestAtrr } from 'helpers/wrappers';

const setup = (verifyTransmission, getTransmission, loading, transmissions) => {
  return shallow(<UnconnectedTransmissionMenu loading={loading} verifyTransmission={verifyTransmission} getTransmission={getTransmission} transmissions={transmissions}/>);
};

//Testing the connected component
describe('Testing UserMenu', () => {
  let wrapper;

  beforeEach(() => {
    const transmissions = {
      live: [
        {
          title: 'Redação para o BRB | Possíveis temas e dicas para gabaritar',
          description: '(Início 06:50) Os professores Raquel Cesário (Redação), Juca Siade (Conhecimentos Bancários) e Emannuelle Gouveia (Informática) discutem suas apostas ...',
          date: '2019-08-15T16:44:57.000Z',
          channelTitle: 'IMP Concursos Online',
          videoId: 'AqxZngWH3yE',
          channelId: 'UCDpWPw86wtFNIZZdnfi7_Ng',
          thumbnail: 'https://i.ytimg.com/vi/AqxZngWH3yE/hqdefault.jpg'
        }
      ],
      upcoming: [
        {
          title: 'Redação para o BRB | Possíveis temas e dicas para gabaritar',
          description: '(Início 06:50) Os professores Raquel Cesário (Redação), Juca Siade (Conhecimentos Bancários) e Emannuelle Gouveia (Informática) discutem suas apostas ...',
          date: '2019-08-15T16:44:57.000Z',
          channelTitle: 'IMP Concursos Online',
          videoId: 'AqxZngWH3yE',
          channelId: 'UCDpWPw86wtFNIZZdnfi7_Ng',
          thumbnail: 'https://i.ytimg.com/vi/AqxZngWH3yE/hqdefault.jpg'
        }
      ],
      none: [
        {
          title: 'Redação para o BRB | Possíveis temas e dicas para gabaritar',
          description: '(Início 06:50) Os professores Raquel Cesário (Redação), Juca Siade (Conhecimentos Bancários) e Emannuelle Gouveia (Informática) discutem suas apostas ...',
          date: '2019-08-15T16:44:57.000Z',
          channelTitle: 'IMP Concursos Online',
          videoId: 'AqxZngWH3yE',
          channelId: 'UCDpWPw86wtFNIZZdnfi7_Ng',
          thumbnail: 'https://i.ytimg.com/vi/AqxZngWH3yE/hqdefault.jpg'
        }
      ]
    };

    const loading = false;

    const verifyTransmission = jest.fn();
    const getTransmission = jest.fn();
    wrapper = setup(verifyTransmission, getTransmission, loading, transmissions);
  });

  it('should render the connected component', () => {
    const component = findByTestAtrr(wrapper, 'TransmissionComponent');
    expect(component.length).toBe(1);
  });

  it('should renders all parts', () => {
    expect(wrapper).toMatchSnapshot();
  });

});
