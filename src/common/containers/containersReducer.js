import types from './containersTypes';

const initialState = {
  transmission: [],
  hasTransmission: false,
  videoFiles: [],
  searchResult: {
    disciplinas: [],
    videos: []
  },
  loadingFiles: false,
  courseToOpen: {},
  loadingSearch: false,
  loading: false
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.LIVE_TRANSMISSION_FETCHED:
      return {
        ...state,
        transmission: payload,
        loading: false
      };
    case types.SEARCH_FETCHED:
      return {
        ...state,
        searchResult: payload || initialState.searchResult,
        loadingSearch: false
      };
    case types.HAS_LIVE_TRANSMISSION_FETCHED:
      return {
        ...state,
        hasTransmission: payload || initialState.hasTransmission
      };
    case types.LIVE_TRANSMISSION_LOADING:
      return {
        ...state,
        loading: payload || initialState.loading
      };
    case types.VIDEO_FILES_FETCHED:
      return {
        ...state,
        videoFiles: state.videoFiles.concat(payload) || initialState.videoFiles,
        loadingFiles: false
      };
    case types.SAVE_PROGRESS_FETCHED:
      return {
        ...state,
        loading: false
      };
    case types.VIDEO_FILES_LOADING:
      return {
        ...state,
        loadingFiles: payload
      };
    case types.COURSE_TO_OPEN:
      return {
        ...state,
        courseToOpen: payload
      };
    case types.SEARCH_LOADING:
      return {
        ...state,
        loadingSearch: payload
      };
    case types.MAXIMIZE_LOADING:
      return{
        ...state,
        loading: payload
      };
    case types.MAXIMIZE_OPENED:
      return{
        ...state,
        loading: payload
      };
    case 'RESET_APPLICATION':
      return {
        ...state,
        ...initialState
      };
    default:
      return state;
  }
};
