import React from 'react'
import ReactTooltip from "react-tooltip";
import {withRouter} from "react-router";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import { openMaximize } from 'common/containers/containersActions';
import {setLoading} from "../../../helpers/redux";
import types from "../containersTypes";

class MaximizeMenu extends React.Component {
  handleClick = () => {
    this.props.openMaximize(this.props.user);
    this.props.setLoading(types.MAXIMIZE_LOADING, true);
  }
  render(){
    return (
      <li
        className={`maximize-menu`}
        data-test="MaximizeComponent"
      >

        <ReactTooltip/>
        <button
          onClick={this.handleClick}
          className="maximize-menu__botao"
          data-tip="Acessar banco de questões"
          data-effect="solid"
          disabled={this.props.loading}
        >
          <i className='icon-maximize maximize-menu__icon'>
          </i>
        </button>
      </li>
    );

  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.auth.user.id_matricula,
    loading: state.containers.loading
  };
};

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({
    openMaximize,
    setLoading
  }, dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MaximizeMenu));
