/* eslint-disable no-undefined */
import axios from 'axios';
import types from './containersTypes';
import courseType from 'modules/system/pages/course/courseTypes';
import { USER_TOKEN, BASE_API, BASE_API_MATRIZ, urlMaximize } from 'config/consts';

import { errorHandler } from 'helpers/errorHandler';
import randomImages from './randomImages/randomImages';
const {shell} = window.require('electron');

const filterHandle = (transmissions) => {
  return transmissions.map(item => ({
    title: item.snippet.title,
    description: item.snippet.description,
    date: item.snippet.publishedAt,
    channelTitle: item.snippet.channelTitle,
    videoId: item.id.videoId,
    channelId: item.snippet.channelId,
    thumbnail: item.snippet.thumbnails.high.url
  })
  );
};

const filterTransmission = (transmission) => {
  const transmissionLive = transmission.filter(value => {
    if (value.snippet.liveBroadcastContent === 'live') {
      return value;
    }
    return null;
  });
  const transmissionNone = transmission.filter(value => {
    if (value.snippet.liveBroadcastContent === 'none') {
      return value;
    }
    return null;
  });
  const transmissionUpcoming = transmission.filter(value => {
    if (value.snippet.liveBroadcastContent === 'upcoming') {
      return value;
    }
    return null;
  });
  return { live: filterHandle(transmissionLive), none: filterHandle(transmissionNone), upcoming: filterHandle(transmissionUpcoming) };
};

/**
 * @function verifyTransmission - Função utilizada para verificar se há transmissões ao vivo
 * @returns {Function} Função que salva a flag no reducer
 */
export const verifyTransmission = () => {
  return (dispatch) => {
    return axios.get(`${BASE_API}/api/integracao/youtube/tem-transmissao-ao-vivo`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.HAS_LIVE_TRANSMISSION_FETCHED,
        payload: response.data.yt_has_live_broadcasts
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'HAS_LIVE_TRANSMISSION_ERROR', null);
    });
  };
};

/**
 * @function getTransmission - Função utilizada para verificar se há transmissões ao vivo
 * @returns {Function} Função que salva a flag no reducer
 */
export const getTransmission = () => {
  return (dispatch) => {
    return axios.get(`${BASE_API}/api/integracao/youtube?maxResults=15`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.LIVE_TRANSMISSION_FETCHED,
        payload: filterTransmission(response.data.yt_data)
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'VERIFY_TRANSMISSION_ERROR', null);
    });
  };
};

/**
 * @function getFiles - Função utilizada para evoluir o atributo de favorito do item
 * @returns {Function} Função que altera o atributo de favorito do item
 */
export const getFiles = (video_id) => {
  return (dispatch) => {
    return axios({
      method: 'get',
      url: `${BASE_API_MATRIZ}/api/matriz/video/${video_id}/arquivos`,
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.VIDEO_FILES_FETCHED,
        payload: response.data
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'COURSE_FILES_FETCHED_ERROR', types.VIDEO_FILES_LOADING);
    });
  };
};

/**
 * @function saveProgress - Função utilizada para evoluir o atributo de favorito do item
 * @returns {Function} Função que altera o atributo de favorito do item
 */
export const saveProgress = (video_id, matricula_id, modulomatriz_id, progress, finished, data) => {
  return (dispatch) => {
    return axios({
      method: 'post',
      url: `${BASE_API_MATRIZ}/api/matriz/video/${video_id}/${matricula_id}/periodo`,
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      },
      data: {
        nu_segundos: parseInt(progress),
        id_modulomatriz: modulomatriz_id,
        bl_assistido: finished
      }
    }).catch((err) => {
      errorHandler(dispatch, err, 'SAVE_PROGRESS_ERROR', types.VIDEO_FILES_LOADING);
    });
  };
};

const handleBackground = data => {
  return data.map(item => {
    return { ...item, background: randomImages(item.id_disciplina) };
  });
};

/**
 * @function search - Função utilizada para pesquisar por disciplinas e
 * vídeos
 * @param {Object} string
 * @returns {Function} Função que salva os resultados no reducer
 */
export const search = (string) => {
  return (dispatch) => {
    return axios.get(`${BASE_API_MATRIZ}/api/matriz/disciplinas-videos/pesquisar?st_pesquisa=${string}`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.SEARCH_FETCHED,
        payload: {
          courses: handleBackground(response.data.disciplinas),
          classes: handleBackground(response.data.videos)
        }
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'SEARCH_ERROR', types.SEARCH_LOADING);
    });
  };
};

export const videoToOpen = (data) => {
  return (dispatch) => {
    dispatch({
      type: types.COURSE_TO_OPEN,
      payload: data
    });
  };
};

export const openMaximize = (id_matricula) => {
  return (dispatch) => {
    return axios.get(`${BASE_API_MATRIZ}/api/maximize/${id_matricula}/autenticar`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem(USER_TOKEN)).token}`
      }
    }).then((response) => {
      dispatch({
        type: types.MAXIMIZE_OPENED,
        payload:shell.openExternal(urlMaximize+`/c/login/autenticar?chaveDeAcesso=${response.data.st_token}#/busca`)
      });
    }).catch((err) => {
      errorHandler(dispatch, err, 'MAXIMIZE_ERROR', types.MAXIMIZE_LOADING);
    });
  };
};
