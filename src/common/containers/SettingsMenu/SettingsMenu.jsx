import React, { Component } from 'react';
import intl from 'react-intl-universal';
import Switch from 'react-switch';
import ReactTooltip from 'react-tooltip';
import moment from 'moment';
import { SYSTEM_VERSION, SYSTEM_NAME } from 'config/consts';

moment.locale('pt-br');

class SettingsMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      autoplay: JSON.parse(localStorage.getItem('AUTOPLAY')) || false,
      notification: JSON.parse(localStorage.getItem('NOTIFICATION')) || true
    };
    this.handleChangePlaying = this.handleChangePlaying.bind(this);
    this.handleChangeNotification = this.handleChangeNotification.bind(this);
  }

  handleChangePlaying(autoplay) {
    this.setState({ autoplay },() => localStorage.setItem('AUTOPLAY', JSON.stringify(autoplay)));
  }

  handleChangeNotification(notification) {
    this.setState({ notification },() => localStorage.setItem('NOTIFICATION', JSON.stringify(notification)));
  }

  handleItem = (title, icon, type, state, handleChange) => (
    <li className="settings-menu__item">
      <span className="settings-menu__item--title">{title}</span>
      <div className="settings-menu__item--playing">
        <span className="settings-menu__item--playing-label">
          <i className={`icon-${icon} settings-menu__item--playing-icon`}/>
          {type}
        </span>
        <Switch
          onChange={handleChange}
          checked={state}
          onColor="#48d852"
          onHandleColor="#FFF"
          offColor="#323131"
          offHandleColor="#FFF"
          handleDiameter={20}
          uncheckedIcon={false}
          checkedIcon={false}
          boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
          activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
          height={20}
          width={35}
        />
      </div>
    </li>
  )

  render() {
    return (
      <li
        className={`dropdown dropdown settings-menu ${this.props.open ? 'open' : ''}`}
        data-test="SettingsComponent"
      >
        <ReactTooltip />
        <span
          onClick={() => this.props.open ? this.props.changeOpen(false) : this.props.changeOpen(true)}
          aria-expanded={'false'}
          className={`settings-menu__dropdown-toggle${this.props.open ? '-open' : ''}`}
          data-toggle="dropdown"
          data-tip="Configurações"
          data-effect="solid"
        >
          <i className="icon-settings settings-menu__icon" />
        </span>
        <ul className={`settings-menu__dropdown-menu ${this.props.open ? 'open' : ''}`}>
          <li className="settings-menu__header">
            <span className="settings-menu__header--title">{intl.get('settings.title')}</span>
          </li>
          {this.handleItem(intl.get('settings.playing'), 'player-play', intl.get('settings.autoplay'), this.state.autoplay, this.handleChangePlaying) }
          {/* {this.handleItem(intl.get('settings.notification'), 'bell', intl.get('settings.allow'), this.state.notification, this.handleChangeNotification) } */}
          <li className="settings-menu__item">
            <span className="settings-menu__item--title">{intl.get('settings.about')}</span>
            <div className="settings-menu__item--about">
              <span className="settings-menu__item--about-title">
                {SYSTEM_NAME}
              </span>
              <span className="settings-menu__item--about-label">
                {intl.get('settings.version')} {SYSTEM_VERSION}
              </span>
            </div>
          </li>
        </ul>
      </li>
    );
  }
}

export default SettingsMenu;
