import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import SettingsMenu from './SettingsMenu';

describe('SettingsMenu component', () => {
  it('should render correctly with no props', () => {
    const component = shallow(<SettingsMenu />);
    expect(component).toMatchSnapshot();
  });
});
