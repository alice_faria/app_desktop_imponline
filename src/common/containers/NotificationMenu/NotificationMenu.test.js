import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import NotificationMenu from './NotificationMenu';

describe('NotificationMenu component', () => {
  it('should render correctly with no props', () => {
    const component = shallow(<NotificationMenu />);
    expect(component).toMatchSnapshot();
  });

  it('matches the snapshot', () => {
    const tree = renderer.create(<NotificationMenu />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
