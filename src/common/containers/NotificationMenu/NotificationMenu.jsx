import React, { Component } from 'react';

export default class NotificationMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  render() {
    return (
      <li
        className={`dropdown notifications-menu ${this.state.open ? 'open' : ''}`}
      >
        <span
          href={window.location.href}
          aria-expanded={this.state.open ? true : false}
          className="dropdown-toggle"
          data-toggle="dropdown"
        >
          <i className="icon-bell notifications-menu__icon"></i>
        </span>

      </li>
    );
  }
}
