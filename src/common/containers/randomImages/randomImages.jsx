import img_background0 from 'assets/images/img_background0.png';
import img_background1 from 'assets/images/img_background1.png';
import img_background2 from 'assets/images/img_background2.png';
import img_background3 from 'assets/images/img_background3.png';
import img_background4 from 'assets/images/img_background4.png';
import img_background5 from 'assets/images/img_background5.png';
import img_background6 from 'assets/images/img_background6.png';
import img_background7 from 'assets/images/img_background7.png';
import img_background8 from 'assets/images/img_background5.png';
import img_background9 from 'assets/images/img_background2.png';

export default (params) => {
  const images = [
    img_background0,
    img_background1,
    img_background2,
    img_background3,
    img_background4,
    img_background5,
    img_background6,
    img_background7,
    img_background8,
    img_background9
  ];
  const randomNum = parseInt(String(params).slice(-1));
  return images[randomNum];
};
