/* eslint-disable no-undefined */
import React from 'react';
import { shallow } from 'enzyme';

import { UserMenu as UnconnectedUserMenu } from './UserMenu';
import { findByTestAtrr } from 'helpers/wrappers';

const setup = (initialState, handleSubmit, user) => {
  return shallow(<UnconnectedUserMenu handleSubmit={handleSubmit} user={user} />);
};

//Testing the connected component
describe('Testing UserMenu', () => {
  let wrapper;

  beforeEach(() => {
    const initialState = {
      auth: {
        user: {
          st_nomecompleto: 'Marcondes de Souza Alves Gorgonho',
          st_login: 'marcondes.gorgonho',
          st_urlavatar: 'http://g2evolutiva.unyleya.xyz/upload/usuario/usuario_foto_100843925cf92040a5458.png'
        },
        validToken: true,
        loading: false
      }
    };
    const user = {
      st_nomecompleto: 'Marcondes de Souza Alves Gorgonho',
      st_login: 'marcondes.gorgonho',
      st_urlavatar: 'http://g2evolutiva.unyleya.xyz/upload/usuario/usuario_foto_100843925cf92040a5458.png'
    };
    const handleSubmit = jest.fn();
    wrapper = setup(initialState, handleSubmit, user);
  });

  it('should render the connected component', () => {
    const component = findByTestAtrr(wrapper, 'userMenuComponent');
    expect(component.length).toBe(1);
  });

  it('should renders all parts', () => {
    expect(wrapper).toMatchSnapshot();
  });

});
