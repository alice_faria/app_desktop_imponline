import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { reduxForm, Form, Field } from 'redux-form';
import { InputLabel } from 'common/components/form/fields/inputLabel/inputLabel';
import intl from 'react-intl-universal';
import Img from 'react-image';

import imgAvatarEmpty from 'assets/images/ic_avatar_empty.png';

export class UserMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }
  container = React.createRef();
  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
    this.props.initialize({ username: this.props.user.st_login, email: this.props.user.st_email });
  }
  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  onSubmit = () => null;
  render() {
    const { user, handleSubmit } = this.props;
    let firstName = user.st_nomecompleto;
    firstName = firstName.substring(0, firstName.indexOf(' ')) === '' ? firstName : firstName.substring(0, firstName.indexOf(' '));
    return (
      <li
        onClick={() => this.props.open ? this.props.changeOpen(false) : this.props.changeOpen(true)}
        className={`dropdown user-menu ${this.props.open ? 'open' : ''}`}
        data-test="userMenuComponent"
      >
        <span
          aria-expanded={this.props.open ? 'true' : 'false'}
          className="user-menu__dropdown-toggle"
          data-toggle="dropdown"
          style={{ padding: 0 }}
        >
          <Img src={[user.st_urlavatars3, user.st_urlavatar, imgAvatarEmpty]} alt="user" className="user-menu__image"
               loader={<Img src={[imgAvatarEmpty]} className="user-menu__image" alt="user"/>}/>
          <div className="user-menu__body">
            <span className="user-menu__body-welcome">{intl.get('userMenu.welcome')}</span>
            <span className="user-menu__body-name">{firstName} </span>
          </div>
          <i className={`icon-down-arrow user-menu__icon${this.props.open ? '-open' : ''}`}></i>&nbsp;&nbsp;
        </span>
        <ul className={`dropdown-menu ${this.props.open ? 'open' : ''}`}>
          <li className="dropdown-header">
            <Img src={[user.st_urlavatars3, user.st_urlavatar, imgAvatarEmpty]} alt="user" className="dropdown-menu__image" />
          </li>
          <li>
            <Form id="loginForm" className="form user__form-box" role='form' onSubmit={handleSubmit(this.onSubmit)} noValidate>
              <div className="form__body">
                <Field
                  id="username"
                  inputClass="input--default"
                  component={InputLabel}
                  style={{ textAlign: 'left', fontSize: '1.6rem', fontWeight: 'bold', color: '#8D9197', paddingRight: 0, paddingLeft: 0, marginBottom: '1rem' }}
                  inputStyle={{
                    fontSize: '1.3rem',
                    padding: '2.85rem .5rem .85rem .8rem',
                    borderRadius: '.4rem',
                    backgroundColor: 'transparent',
                    width: '100%',
                    height: '5.5rem',
                    color: '#8D9197'
                  }}
                  type="text"
                  name="username"
                  disabled
                  label={intl.get('userMenu.username')}
                  cols='12 12 12 12'
                />
                <Field
                  id="password"
                  inputClass="input--default"
                  component={InputLabel}
                  style={{ textAlign: 'left', fontSize: '1.6rem', fontWeight: 'bold', color: '#8D9197', paddingRight: 0, paddingLeft: 0 }}
                  inputStyle={{
                    fontSize: '1.3rem',
                    padding: '2.85rem .5rem .85rem .8rem',
                    borderRadius: '.4rem',
                    backgroundColor: 'transparent',
                    width: '100%',
                    height: '5.5rem',
                    color: '#8D9197'
                  }}
                  type="text"
                  disabled
                  name="email"
                  label={intl.get('userMenu.email')}
                  cols='12 12 12 12'
                />
              </div>
              <div className="dropdown__footer">
                <Link to='/logout'>
                  <div className="button dropdown__footer-button">{intl.get('userMenu.logout')}</div>
                </Link>
              </div>
            </Form>
          </li>
        </ul>
      </li>
    );
  }
}

const FormUserMenu = reduxForm({ form: 'userForm' })(UserMenu);

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.auth.user,
    dados: state.dados.meusDados
  };
};

export default withRouter(connect(mapStateToProps)(FormUserMenu));
