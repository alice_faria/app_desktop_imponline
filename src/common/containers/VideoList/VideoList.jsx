import React from 'react';
import { Line } from 'rc-progress';
import intl from 'react-intl-universal';
import produce from 'immer';
import _ from 'lodash';
import VideoFiles from '../videoFiles/videoFiles';
import moment from 'moment';

const isSelected = (index, props) => {
  const { courseModule, courseModulePlaying, courseClass } = props;
  return courseClass === index && courseModule === courseModulePlaying;
};

const cleanFavorited = (draftState, props) => {
  if (draftState[props.indexProjetoPedagogico].disciplinas[props.indexSubject].modulos[props.courseModule].videos.every(element => element === null)) {
    props.closeSidebar();
    draftState[props.indexProjetoPedagogico].disciplinas[props.indexSubject].modulos.splice(props.courseModule, 1);
  }
  if (draftState[props.indexProjetoPedagogico].disciplinas[props.indexSubject].modulos.every(element => element === null)) {
    props.closeSidebar();
    draftState[props.indexProjetoPedagogico].disciplinas.splice(props.indexSubject, 1);
  }
  if (draftState[props.indexProjetoPedagogico].disciplinas.every(element => element === null)) {
    props.setLoading();
    delete draftState[props.indexProjetoPedagogico];
    draftState.splice(props.indexProjetoPedagogico, 1);

  }
};

const handlePercent = (index, props, seconds, nu_segundos, nu_duracao, bl_assistido) => {
  //TODO: caso a regra de mostrar a barra do vídeo completo usando o bl_assistido mude, basta descomentar
  // if (isSelected(index, props)) {
  //   if (!_.isNull(seconds)) {
  //     return parseInt(seconds);
  //   }
  //   return parseInt(nu_segundos);
  // }
  // return parseInt(nu_segundos) * 100 / parseInt(nu_duracao);
  return parseInt(isSelected(index, props)
    ? !_.isNull(seconds)
      ? seconds
      : nu_segundos
    : nu_segundos) * 100 / parseInt(nu_duracao);
};

const handleFavorited = (item, index, props, itemFav, playing) => {
  const values = {
    st_aulapos: `${index + 1}`,
    id_disciplina: props.id_disciplina,
    st_modulopos: `${props.courseModule + 1}`,
    id_samba_id: `${item.id_samba_id}`,
    st_samba_title: item.st_samba_title,
    id_projetopedagogico: props.id_projetopedagogico,
    id_matriz: props.modules[props.courseModule].id_matriz
  };

  const consultingValues = {
    id_projetopedagogico: parseInt(props.id_projetopedagogico),
    st_tituloexibicao: props.course.st_tituloexibicao,
    st_semana: props.course.st_semana,
    st_coordenador: props.course.st_coordenador,
    st_projecthash: '6169876d5ac3a7d26e69d4471ef941b7',
    st_urlsamba: 'www.google.com',
    dt_inicio: props.course.dt_inicio,
    dt_termino: '2119-12-15 00:00:00.000000',
    // dt_termino: props.course.dt_termino,
    st_disciplina: props.course.st_disciplina,
    st_nomeprofessor: props.course.st_nomeprofessor + ' - Semana ' + props.course.st_semana,
    st_imagemprofessor: props.course.st_imagemprofessor,
    st_modulopos: `${props.courseModule + 1}`,
    st_aulapos: `${index + 1}`,
    id_samba_id: `${item.id_samba_id}`,
    st_samba_title: item.st_samba_title,
    st_modulomatriz: props.modules[props.courseModule].st_modulomatriz
  };

  const valuesUnset = {
    id_disciplina: props.id_disciplina,
    id_samba_id: `${item.id_samba_id}`,
    id_projetopedagogico: props.id_projetopedagogico,
    id_matriz: props.modules[props.courseModule].id_matriz
  };

  if (props.favorite) {
    console.log(props);
    const dataFavorite = produce(props.reduceFavorite, draftState => {
      delete draftState[props.indexProjetoPedagogico].disciplinas[props.indexSubject].modulos[props.courseModule].videos[index];
      props.excludeVideo(index, item);
      cleanFavorited(draftState, props);
    });
    if (item === playing) {
      props.disablePlayer();
    }
    props.unsetFavorite(dataFavorite, valuesUnset, props.type);
    if (props.resetConsulting) {
      // props.resetConsulting();
    }
  } else if (props.consulting) {
    const data = produce(props.course, draftState => {
      draftState.modulos[props.courseModule].videos[index].bl_favorito = !(itemFav || props.favorite);
    });
    if (itemFav) {
      props.unsetFavorite(data, valuesUnset, props.type);
    } else {
      props.setFavorite(data, consultingValues, props.type);
    }
  } else {
    const data = produce(props.course, draftState => {
      draftState.modules[props.courseModule].videos[index].bl_favorito = !(itemFav || props.favorite);
    });
    if (itemFav) {
      props.unsetFavorite(data, valuesUnset, props.type);
    } else {
      props.setFavorite(data, values, props.type);
    }
  }
};

export default (props) => {
  const { courseModule, modules, onClick, favorite,
    loading, handleFiles, files, consulting, seconds, videoPlaying } = props;
  const listVideos = modules[courseModule].videos.map((item, index) => {
    if (_.isNull(item)) {
      return undefined;
    }
    const secondsConverted = !_.isNull(seconds) ? seconds : item.nu_segundos;
    const secondsChanging = moment.duration(parseInt(secondsConverted), 'seconds').format('mm:ss', { trim: false });
    const secondsStatic = moment.duration(parseInt(item.nu_segundos), 'seconds').format('mm:ss', { trim: false });
    const duration = moment.duration(parseInt(item.nu_duracao), 'seconds').format();
    const percent = handlePercent(index, props, seconds, item.nu_segundos, item.nu_duracao, item.bl_assistido);
    return (
      <div className="videos__list--item" key={`itemVideo-${index}`}>
        <div className="videos__list--item-header">
          <div className="videos__list--item-header-group">
            <div
              className="videos__list--item-header-play"
              onClick={() => onClick(item, index)}>
              <i className="icon-play videos__list--item-header-icon" />
            </div>
            <div className="videos__list--item--tags-module">{intl.get('course.module')} {modules[courseModule].st_modulo_pos}</div>
            <div className="videos__list--item--tags-class">{intl.get('course.class')} {item.st_aula_pos ? item.st_aula_pos : index + 1}</div>
            {item.id_samba_id === videoPlaying
              ? <div className="videos__list--item--tags-watching">{intl.get('course.watching')}</div>
              : null}
          </div><i
            id="iconFav"
            className={`icon-bookmark${favorite || item.bl_favorito ? '_on' : ''} videos__list--item--icon`}
            data-tip="Favoritar Vídeo"
            data-place="bottom"
            data-effect="solid"
            onClick={() => handleFavorited(item, index, props, item.bl_favorito, videoPlaying)}
          />
        </div>
        <div className="videos__list--item-title" data-tip={item.st_samba_title.trim()}>
          {item.st_samba_title.trim().length > 40 ? item.st_samba_title.trim().slice(0, 36) + '...' : item.st_samba_title.trim()}
        </div>
        {consulting || favorite
          ? null
          : <div className="videos__list--item-progress">
            <div className="videos__list--item-progress-value">
              {`${isSelected(index, props) ? secondsChanging : secondsStatic}/${duration}`}
            </div>
            <Line percent={String(percent)} className="videos__list--item-progress-bar" strokeWidth="1" strokeColor="#CC0000" trailColor="#FFF" />
          </div>
        }
        <div className="videos__list--item-footer">
          <div className="consulting__footer--divider"></div>
          <VideoFiles
            loading={loading}
            handleFiles={handleFiles}
            videoID={item.id_samba_id}
            files={files}
          />
        </div>
      </div>
    );
  });
  return listVideos.sort((a, b) => {
    return a.st_aula_pos < b.st_aula_pos ? -1 : a.st_aula_pos > b.st_aula_pos ? 1 : 0;
  });
};
