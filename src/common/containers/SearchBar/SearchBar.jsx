import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Form, Field } from 'redux-form';
import intl from 'react-intl-universal';
import { setLoading, reset } from 'helpers/redux';
import types from 'common/containers/containersTypes';
import { search, videoToOpen } from 'common/containers/containersActions';
import { setCourse } from 'modules/system/pages/course/courseActions';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import Img from 'react-image';
import { SearchLabel } from 'common/components/form/fields/searchLabel/searchLabel';
import Loading from 'common/components/ui/loading/loading';
import img_search_empty from 'assets/images/img_search_empty.png';
import imgTeacherEmpty from 'assets/images/imgMockCourse.png';

class SearchBar extends Component {
  state = {
    isFocusedSearch: false,
    searched: false,
    navigate: undefined
  };

  handleSelectCourse = course => {
    this.setState({ navigate: `${course.id_matricula}+${course.id_disciplina}+${course.id_projetopedagogico}` },
      () => {this.props.setCourse(course.id_matricula, course.id_disciplina, course.id_projetopedagogico); this.cleanSearch();});
  }

  handleSelectClass = video => {
    const data = {
      video_id: video.id_samba_id,
      module: video.id_modulomatriz,
      video_title: video.st_samba_title,
      video_time: video.nu_segundos
    };
    this.setState({ navigate: `${video.id_matricula}+${video.id_disciplina}+${video.id_projetopedagogico}` },
      () => {
        this.props.videoToOpen(data);
        this.cleanSearch();
      });
  }

  renderCoursesResult = courses => {
    return courses.map(course => (
      <div className="result__course" onClick={() => this.handleSelectCourse(course)} key={Math.floor(Math.random() * 200000)}>
        <Img
          src={[course.st_imagemprofessor, imgTeacherEmpty]}
          className="result__course--image"
          alt="professor"
          style={{ backgroundImage: `url(${course.background})`, backgroundSize: 'cover' }}
        />
        <div className="result__course--info">
          <span className="result__course--info-course">{course.st_tituloexibicao}</span>
          <span
            className="result__course--info-class"
            data-tip={course.st_disciplina.substring(0, (course.st_disciplina).indexOf('_'))}>
            {course.st_disciplina.substring(0, (course.st_disciplina).indexOf('_'))}
          </span>
          <span className="result__course--info-teacher">{course.st_nomeprofessor}</span>
        </div>
      </div>
    ));
  }

  renderClassesResult = classes => {
    return classes.map(item => (
      <div className="result__class" onClick={() => this.handleSelectClass(item)} key={Math.floor(Math.random() * 2000000)}>
        <div className="result__class--course-title">{item.st_projetopedagogico}</div>
        <div className="result__class--item">
          <div className="result__class--item-title">{item.st_samba_title}</div>
          <div className="result__class--item-data">
            <Img
              src={[item.st_imagemprofessor, imgTeacherEmpty]}
              className="result__class--image"
              alt="professor"
              style={{ backgroundImage: `url(${item.background})`, backgroundSize: 'cover' }}
            />
            <div className="result__class--info">
              <span className="result__class--info-class">
                {item.st_disciplina.substring(0, (item.st_disciplina).indexOf('_'))}
              </span>
              <span className="result__class--info-teacher">{item.st_nomeprofessor}</span>
            </div>
          </div>
        </div>
      </div>
    ));
  }

  renderSearchResult = (result) => {
    if (!this.state.searched) {
      return <span className="searchBar__dropdown--typing">Pressione enter para pesquisar</span>;
    } else if (_.isEmpty(result.classes) && _.isEmpty(result.courses)) {
      return (
        <div className="searchBar__dropdown--empty">
          <img src={img_search_empty} alt='' className="searchBar__dropdown--empty-image" />
          <div className="searchBar__dropdown--empty-text">{intl.getHTML('searchBar.emptySearch')}</div>
        </div>
      );
    }
    return (
      <div className="searchBar__dropdown--result">
        <div className="searchBar__result">
          <div className="searchBar__result--courses">
            <div className="searchBar__result--title">{intl.get('searchBar.courses')}</div>
            {this.renderCoursesResult(result.courses)}
          </div>
          <div className="searchBar__result--classes">
            <div className="searchBar__result--title">{intl.get('searchBar.classes')}</div>
            {this.renderClassesResult(result.classes)}
          </div>
        </div>
      </div>
    );
  }

  handleFocus = (field) => this.setState({ [field]: true });
  handleBlur = (field) => this.setState({ [field]: false });

  cleanSearch = () => {
    this.setState({
      searched: false
    }, () => {
      this.props.reset();
      this.props.setLoading(types.SEARCH_LOADING, false);
    });
  }

  onSubmit = (value, event) => {
    this.setState({
      searched: true,
      isFocusedSearch: false
    }, () => {
      this.props.setLoading(types.SEARCH_LOADING, true);
      this.props.search(value.search);
    });
  }

  render() {
    const { handleSubmit, dirty } = this.props;
    if (!_.isUndefined(this.state.navigate)) {
      const navigate = this.state.navigate;
      this.props.history.push(`/search/${navigate}`);
      this.setState({ navigate: undefined });
    }
    return (
      <React.Fragment>
        <div className="searchBar">
          <Form id="searchForm" className="form searchBar__form-box" role='form' onSubmit={handleSubmit(this.onSubmit)} noValidate>
            <Field
              id="search"
              inputClass="searchBar__input"
              component={SearchLabel}
              style={{ textAlign: 'left', fontSize: '1.6rem', color: 'black', paddingRight: 0, paddingLeft: 0 }}
              type="text"
              name="search"
              icon={dirty ? 'icon-close' : 'icon-search'}
              iconRight
              iconOnClick={() => this.cleanSearch()}
              disabled={this.props.loading}
              isFocused={this.state.isFocusedSearch}
              onFocus={() => this.handleFocus('isFocusedSearch')}
              onBlur={() => this.handleBlur('isFocusedSearch')}
              placeholder={intl.get('searchBar.placeholder')}
              cols='12 12 12 12'
            />
          </Form>
          <div className={`searchBar__dropdown${this.state.isFocusedSearch || dirty ? '--open' : ''}`}>
            {this.props.loading
              ? <Loading isVisible={this.props.loading} overlay={false} width={50} height={50} className='searchBar__loading' />
              : this.renderSearchResult(this.props.result)}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

SearchBar = reduxForm({ form: 'searchForm' })(SearchBar);

const mapStateToProps = (state) => {
  return {
    result: state.containers.searchResult,
    loading: state.containers.loadingSearch
  };
};

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({
    setLoading,
    search,
    reset,
    videoToOpen,
    setCourse
  }, dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SearchBar));
