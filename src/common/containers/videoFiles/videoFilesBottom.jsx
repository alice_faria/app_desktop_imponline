import React, { Component } from 'react';
import _ from 'lodash';

export default class VideoFilesBottom extends Component {

  handleMyFiles = () => {
    const { files, close, loading } = this.props;
    const myFiles = files.filter(item => {
      if (item.id_samba_id === this.props.videoID) {
        return item;
      }
      return null;
    });
    if (_.isEmpty(myFiles) || close) {
      return (
        <React.Fragment>
          {close && <i className="icon-plus course__icons-plus" onClick={() => {
            this.props.handleClick();
            this.props.handleFiles(this.props.videoID);
          }} />}
          {loading && <i className="icon-spinner icn-spinner course__icons-loading" />}
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        <i className="icon-minus course__icons-minus" data-tip="Abrir Arquivos" onClick={() => this.props.handleClick()} />
        {myFiles[0].teoria && <a target="_blank" rel="noopener noreferrer" href={myFiles[0].teoria} > <i className="icon-course course__icons"/></a>}
        {myFiles[0].exercicio && <a target="_blank" rel="noopener noreferrer" href={myFiles[0].exercicio} > <i className="icon-file-check course__icons"/></a>}
        {myFiles[0].slide && <a target="_blank" rel="noopener noreferrer" href={myFiles[0].slide} > <i className="icon-presentation course__icons"/></a>}
        {/* {props.files.audio && <i className="icon-headphones course__icons" />} */}
        {/* <i className="icon-comment2 course__icons" /> */}
      </React.Fragment>
    );
  }

  render() {
    return (
      <div className="course__footer--icons">
        <div className="course__footer--icons-actions">
          {this.handleMyFiles()}
        </div>
        {/* <i className="icon-download-button course__icons" /> */}
      </div>
    );
  }
}
