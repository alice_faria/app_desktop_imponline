import React, { Component } from 'react';
import intl from 'react-intl-universal';
import ReactTooltip from 'react-tooltip';

class CastMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      chrome: false,
      apple: false
    };
  }

  render() {
    return (
      <li
        className={`dropdown dropdown cast-menu ${this.props.open ? 'open' : ''}`}
        data-test="CastComponent"
      >
        <ReactTooltip />
        <span
          onClick={() => this.props.open ? this.props.changeOpen(false) : this.props.changeOpen(true)}
          aria-expanded={'false'}
          className={`cast-menu__dropdown-toggle${this.props.open ? '-open' : ''}`}
          data-toggle="dropdown"
        >
          <i className="icon-cast cast-menu__icon" />
        </span>
        <ul className={`cast-menu__dropdown-menu ${this.props.open ? 'open' : ''}`}>
          <li className="cast-menu__header">
            <span className="cast-menu__header--title">{intl.get('cast.title')}</span>
          </li>
          <li className="cast-menu__item">
            <div className="cast-menu__item--about">
              <span className="cast-menu__item--about-label">
                {intl.get('cast.chrome')}
              </span>
            </div>
          </li>
          <li className="cast-menu__item">
            <div className="cast-menu__item--about">
              <span className="cast-menu__item--about-label">
                {intl.get('cast.appleTV')}
              </span>
            </div>
          </li>
        </ul>
      </li>
    );
  }
}

export default CastMenu;
