import { toastr } from 'react-redux-toastr';
import { setLoading } from 'helpers/redux/loading';
import intl from 'react-intl-universal';
import _ from 'lodash';
import { resetApplication } from 'pages/auth/authActions';
import { history } from 'main/configureStore';

export const errorHandler = (dispatch, err, typeError, typeLoading) => {
  try {
    if (_.isUndefined(err.response)) {
      dispatch({ type: 'ERROR_CONNECTION' });
      toastr.error('Erro!',intl.get('errorRequest.connection'));
      dispatch(setLoading(typeLoading, false));
    } else if (err.response.status === 403) {
      dispatch(resetApplication());
      dispatch({ type: 'LOGOUT' });
      history.push('/login');
    } else {
      dispatch({ type: typeError, payload: err.response.data });
      toastr.error(`${err.response.data.titulo}! ${err.response.data.mensagem}`);
      dispatch(setLoading(typeLoading, false));
    }
  } catch (error) {
    toastr.error('Erro!',intl.get('errorRequest.default'));
    dispatch({ type: typeError });
    dispatch(setLoading(typeLoading, false));
  }
};
