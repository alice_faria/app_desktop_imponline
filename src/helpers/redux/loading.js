/**
 * @function Função que serve para alterar o satatus do loading
 * @param {Boolean} status
 * @returns {Object} Evento que diz para a aplicação qual o status do loading
 */
export const setLoading = (type, status) => {
  return { type: type, payload: status };
};
