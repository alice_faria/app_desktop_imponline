import { initialize, reset as resetForm } from 'redux-form';

//////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// ACTIONS AUXILIARES DO REDUX FORM /////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Função auxiar para inicialização do formulário do redux form
 * @param {String} formName Nome do formuálrio
 * @param {Object} values Valores do formulário
 */
export const initializeForm = (formName, values) => {
  return initialize(formName, values);
};

/**
 * Função auxiar para limpar os campos do formulário
 * @param {String} formName Nome do formuálrio
 */
export const reset = (formName) => {
  return resetForm(formName);
};
