import React, { Fragment } from 'react';

export const TermsText = (
  <Fragment>
    <div style={{ whiteSpace: 'pre-wrap' }}>
      <span style={{ fontWeight: 'bold' }} />
      <p><span style={{ fontWeight: 'bold' }}>  &emsp;1. Das partes</span></p>
      <p>&emsp;&emsp;</p>
      <p><span style={{ fontWeight: 'bold' }}>ALUNO</span>: denominado <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span>;</p>
      <p>&emsp;&emsp;</p>
      <p><span style={{ fontWeight: 'bold' }}>UNYLEYA EDITORA E CURSOS S.A.</span>, pessoa jurídica de direito privado,
      com sede na rua Jacarandá, lote 16, Águas Claras, Brasília–DF, CEP: 71.927–540, inscrita no CNPJ sob o nº. 14.019.108/0001–30,
      denominada <span style={{ fontWeight: 'bold' }}>CONTRATADA</span>.</p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>1. Da Metodologia do curso</span></p>
      <p> &emsp;&emsp;1. O curso consiste na apresentação de conteúdo em Ambiente Virtual de Aprendizagem (AVA), preparado exclusivamente para este fim.
        <br />&emsp;&emsp;2. No ambiente virtual de aprendizagem, o <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span> terá a sua disposição:
        <br />&emsp;&emsp;&emsp;1. Videoaulas de teoria (com exceção de cursos exclusivamente de exercícios);
        <br />&emsp;&emsp;&emsp;2. Videoaulas com resolução de exercícios;
        <br />&emsp;&emsp;&emsp;3. Banco de Questões.
        <br />&emsp;&emsp;3. O conteúdo permanecerá à disposição do <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span>,
        no ambiente virtual de aprendizagem, pelo período de 8 (oito) meses para cursos completos, e de 6 (seis) meses para matérias avulsas,
        que serão contados a partir da efetivação da matrícula. ou pelo prazo determinado na descrição do produto no site, caso este seja superior aos prazos mencionados acima.
        <br />&emsp;&emsp;4. Unyleya Editora e Cursos aborda os tópicos mais relevantes do edital, os mais incidentes em provas de concursos.
        <br />&emsp;&emsp;5. Acesso ilimitado às videoaulas.
        <br />&emsp;&emsp;6. As aulas serão disponibilizadas de acordo com as gravações dos professores.
      </p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>1. Das Regras para Certificados e Declarações</span>
        <br />&emsp;&emsp;1. O <span style={{ fontWeight: 'bold' }}>ALUNO/CONTRATANTE</span> deve solicitar o certificado ou a declaração por escrito ao Serviço
        de Atendimento ao Aluno (SAA) ou pelo e–mail atendimento@imponline.com.br. O certificado será encaminhado para o e–mail cadastrado.
        <br />&emsp;&emsp;2. Para emissão do certificado, o aluno deverá ter assistido, no mínimo, 75% (setenta e cinco por cento) das videoaulas oferecidas.
        <br />&emsp;&emsp;3. É fundamental o preenchimento do nome completo e sem abreviações, pois os certificados e declarações serão emitidos com base neste registro.
        <br />&emsp;&emsp;4. O certificado será emitido com o nome e CPF cadastrados no sistema. Não é possível emissão do certificado ou declarações em nome de terceiros.
        <br />&emsp;&emsp;5. Após a emissão do certificado ou declaração, nenhum dado poderá ser alterado, sendo de inteira responsabilidade do ALUNO/
        <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span> a informação correta de seus dados cadastrais;
        <br />&emsp;&emsp;6. Constarão no certificado as seguintes informações: nome completo, CPF, nome completo do curso realizado, carga horária,
        período de início, término do curso e data de emissão. Os certificados não podem ser modificados.
        <br />&emsp;&emsp;7. Na declaração de curso constarão: nome completo, CPF, matrícula, nome completo do curso realizado, carga horária,
        período de início e término do curso, conteúdo programático (somente disciplinas avulsas) e data de impressão.
        A declaração de matrícula terá os mesmos itens, com exceção do conteúdo programático. Será inserida nesta declaração a situação do curso.
        <br />&emsp;&emsp;8. A declaração de matrícula somente será emitida no período em que o curso estiver ativo.
        <br />&emsp;&emsp;9. No caso de emissão de 2ª via de certificado, será cobrada uma taxa de R$30,00 (trinta reais).
        <br />&emsp;&emsp;10. O aluno terá o prazo de 12 (doze) meses após a expiração do curso para solicitar o certificado.
        <br />&emsp;&emsp;11. Para a entrega das declarações, por unidade, via postal (carta registrada/AR) será cobrado o valor de R$20,00 (vinte reais).
        <br />&emsp;&emsp;12. Para a entrega das declarações, por unidade, via postal (SEDEX) será cobrado o valor de R$35,00 (trinta e cinco reais).
      </p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>1. Da matrícula e do pagamento do curso</span>
        <br />&emsp;&emsp;1. A efetivação da matrícula só ocorre mediante a comprovação de pagamento integral do curso, caso este tenha sido adquirido à vista.
        Caso o valor do curso tenha sido parcelado, a efetivação da matrícula se dará a partir do pagamento da 1ª parcela.
        <br />&emsp;&emsp;2. A Unyleya reserva–se ao direito de confirmar a matrícula do aluno somente após a aprovação do cadastro do
        <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span>. O período de acesso ao curso adquirido somente terá início após a aprovação do cadastro do
        <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span>.
        <br />&emsp;&emsp;3. O valor, as formas de pagamento e as especificações do conteúdo, das matérias e professores (tutores) que participarão do curso,
         estão disponíveis no site da <span style={{ fontWeight: 'bold' }}>UNYLEYA</span> ou poderão ser solicitadas pelo e-mail atendimento@imponline.com.br
        <br />&emsp;&emsp;4. A Unyleya Editora e Cursos não efetua troca de cursos.
      </p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>1. Do Acesso ao curso</span>
        <br />&emsp;&emsp;1. O <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span> receberá por e-mail o login e a senha para acesso ao Portal do Aluno:
        a) em até 8 (oito) horas para pagamentos realizados mediante cartão de crédito; e b) em até 2 (dois) dias úteis para pagamentos realizados em boleto bancário.
        <br />&emsp;&emsp;2. Recomendamos a utilização dos navegadores Chrome, Mozilla e Opera. Não é indicada a utilização dos navegadores Internet Explore e Safari.
        <br />&emsp;&emsp;3. Eventuais dúvidas ou dificuldades de acesso ao Ambiente Virtual de Aprendizagem (AVA) deverão ser comunicadas pelos canais abaixo.
        As solicitações serão respondidas em até 7 (sete) dias úteis:
      </p>
      <p>&emsp;Atendimento Online
        <br />&emsp;Telefones: 0800 602 67 69 ¦ (61) 3031-5789
        <br />&emsp;E-mail: atendimento@imponline.com.br
        <br />&emsp;SAA: Serviço de Atenção ao Aluno (área do aluno)
      </p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>1. Da Responsabilidade do CONTRATANTE</span>
        <br />&emsp;&emsp;O <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span> não poderá compartilhar seu acesso (login e senha) com terceiros,
        nem acessar simultaneamente o ambiente virtual de aprendizagem por meio de mais de um computador pessoal ou equipamento similar.
        Tentativas nesse sentido implicarão bloqueio imediato do perfil em uso, ficando a <span style={{ fontWeight: 'bold' }}>UNYLEYA</span>
        autorizada a cancelar o contrato do curso, sem devolução dos valores pagos ou devidos e sem prejuízo da responsabilidade civil e criminal do infrator.
      </p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>1. DA Rescisão do Curso</span></p>
      <p> &emsp;&emsp;1. O presente instrumento poderá ser rescindido por iniciativa do <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span>,
      mediante requerimento enviado ao e-mail atendimento@imponline.com.br com os seguintes dados: nome completo, CPF, motivo e curso que deseja cancelamento.
      Essas informações devem ser enviadas do e-mail do <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span> cadastrado no sistema. Solicitações verbais
        ou por outros meios de comunicação não serão aceitas.
      <br />&emsp;&emsp;2. Após o <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span> efetuar a matrícula no curso e efetivar seu primeiro acesso,
        a <span style={{ fontWeight: 'bold' }}>UNYLEYA</span> arcará com custos operacionais referentes à oferta do curso. Portanto, o cancelamento da
        matrícula estará sujeito às seguintes condições:
      <br />&emsp;&emsp;&emsp;1. Se o cancelamento se der a partir do 8º (oitavo) dia a partir do primeiro acesso ao ambiente virtual de aprendizagem,
        será cobrado do <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span> o correspondente a 20% (vinte por cento) do valor do curso;
      <br />&emsp;&emsp;&emsp;2. Se o cancelamento se der após o 10º (décimo) dia a partir do primeiro acesso ao ambiente virtual de aprendizagem,
        além do valor citado no item anterior, será cobrado do <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span> o valor proporcional,
        correspondente à disponibilidade do curso, desde a adesão ao curso, até a data da solicitação do pedido de cancelamento.
      <br />&emsp;&emsp;&emsp;3. Caso todas as aulas já estejam disponíveis, será possível efetuar o cancelamento do curso, mas o cliente não fará
        jus a qualquer devolução dos valores contratados.
      <br />&emsp;&emsp;&emsp;4. A <span style={{ fontWeight: 'bold' }}>UNYLEYA</span> terá o prazo de 30 (trinta) dias para efetuar as restituições devidas.
      <br />&emsp;&emsp;&emsp;5. Se o pagamento for realizado mediante cartão de crédito, o cancelamento será realizado junto à administradora do cartão,
        por meio de uma carta de estorno. Após a solicitação de ressarcimento do produto por e-mail, serão descontadas as multas.
        A visualização do valor pode ocorrer em até 2 (duas) faturas, conforme data de fechamento. Sob nenhuma hipótese, o ressarcimento será feito na conta de terceiros.
      <br />&emsp;&emsp;&emsp;6. Se o pagamento for feito em boleto, o cancelamento será realizado por meio de depósito em conta.
        Após a solicitação de ressarcimento do produto por e-mail, serão descontadas as multase efetuado um depósito na conta do aluno.
      <br />&emsp;&emsp;&emsp;7. Para os Simulados, não há possibilidade de trocas ou devoluções após o acesso ao AVA.
        Por isso, as normas e itens acima mencionados não se aplicam a esta categoria de produto. A <span style={{ fontWeight: 'bold' }}>UNYLEYA</span>
        acredita que o aluno tem acesso, já em seu primeiro contato com o AVA, aos serviços oferecidos e prestados e a todo o material disponível.
      <br />&emsp;&emsp;&emsp;8. O cancelamento da consultoria de estudos pode ser feito até a 8a semana, contada a partir do início da turma.
        Se o cancelamento se der a partir do 8º (oitavo) dia a partir do primeiro acesso ao ambiente virtual de aprendizagem, será cobrado do
      <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span> o correspondente a 20% (vinte por cento) do valor do curso acrescido do valor de R$ 150,00
        (cento e cinquenta reais) por semana, contado a partir da data de início da turma.
      <br />&emsp;&emsp;&emsp;9. Fica facultado à <span style={{ fontWeight: 'bold' }}>UNYLEYA</span> o direito de não aceitar
      devoluções frequentes do mesmo aluno ou pelo mesmo motivo.
      <br />&emsp;&emsp;&emsp;10. Caso ocorra cancelamento total ou parcial, o cliente (aluno) não fará jus ao certificado de conclusão do curso.
      <br />&emsp;&emsp;&emsp;11. Além das condições anteriormente descritas, a multa de cancelamento do IMP Flex é acrescida de R$ 150,00 (cento e cinquenta)
        para cada FREEPASS solicitado e mais o valor integral dos livros que compõem o produto.
      </p>
      <p>&emsp;&emsp;</p>
      <p>O Portal terá o prazo de 30 (trinta) dias para efetuar as restituições devidas.</p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>1. DOS LIVROS E E-BOOKS</span></p>
      <p> &emsp;&emsp;1. Na compra de livros impressos:
        <br />&emsp;&emsp;&emsp;1. O envio é feito até 3 (três) dias úteis após a confirmação do pagamento;
        <br />&emsp;&emsp;&emsp;2. O envio é feito pelos Correios, pela modalidade impresso módico;
        <br />&emsp;&emsp;&emsp;3. O prazo de entrega varia de região para região, 10 (dez) dias úteis;
        <br />&emsp;&emsp;&emsp;4. O código de rastreamento é enviado após o livro ser postado, assim que disponibilizado pelos Correios.</p>
      <p> &emsp;&emsp;1. Na compra de e-Books:
        <br />&emsp;&emsp;&emsp;1. O <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span> deverá verificar se o horário do relógio em seu dispositivo é
        igual ao horário real, bem como checar também se a data do calendário condiz com a atual,
        pois caso estes dados estejam alterados, pode haver um conflito com o tempo de validade do arquivo;
        <br />&emsp;&emsp;&emsp;2. Lembramos que, por questão de segurança, é possível acessar o e-book em até 6 (seis) dispositivos com o mesmo Adobe ID;
        <br />&emsp;&emsp;&emsp;3. Após a confirmação do pagamento, o cliente receberá um e-mail com as instruções para download do e-book: a) em até 8 (oito)
        horas para pagamentos realizados no cartão de crédito; e b) em até 2 (dois) dias úteis, para pagamentos realizados mediante boleto bancário;
        <br />&emsp;&emsp;&emsp;4. Os e-books da CONTRATADA são criptografados utilizando o DRM (Digital Rights Management) da Adobe. Para acessá-los,
        é necessário seguir as instruções descritas abaixo, considerando o tipo de dispositivo que irá utilizar para ler o seu e-book.</p>
      <p>&emsp;&emsp;</p>
      <p><span style={{ fontWeight: 'bold' }}>ATENÇÃO!</span> É de responsabilidade do <span style={{ fontWeight: 'bold' }}>CLIENTE/CONTRATANTE</span>
        manter o dispositivo atualizado de acordo com as orientações dos criadores do software e sistema operacional para perfeito
        funcionamento do aplicativo para leitura do e-book.</p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>Instruções</span></p>
      <p>&emsp;&emsp;<span style={{ fontWeight: 'bold' }}>Passo 1</span> - É necessário utilizar um Adobe ID (inscrição gratuita). Caso não tenha,
      veja aqui como criá-la: https://www.adobe.com/br/account/sign-in.adobedotcom.html</p>
      <p>&emsp;&emsp;<span style={{ fontWeight: 'bold' }}>Passo 2</span></p>
      <p> &emsp;&emsp;<span style={{ fontWeight: 'bold' }}>Se for utilizar um aparelho móvel com sistema operacional iOS:</span>
        <br />&emsp;1. Faça o download do aplicativo (app) "Bluefire Reader". É gratuito e está disponível na AppStore.
        <br />&emsp;2. Em seguida, acesse o app e faça login com o seu Adobe ID (Passo 1).
        <br />&emsp;3. Depois, clique no link do e-book e inicie a leitura.
      </p>
      <p> &emsp;&emsp;<span style={{ fontWeight: 'bold' }}>Se for utilizar um aparelho móvel com sistema operacional Android:</span>
        <br />&emsp;1. Faça o download do aplicativo (app) "Aldiko Reader". É gratuito e está disponível na Play Store ou no Android Market.
        <br />&emsp;2. Em seguida, faça login com o seu Adobe ID (Passo 1).
        <br />&emsp;3. Depois, clique no link do e-book e inicie a leitura.
      </p>
      <p> &emsp;&emsp;<span style={{ fontWeight: 'bold' }}>Se for utilizar um computador (PC ou Macintosh):</span>
        <br />&emsp;1. Faça o download do Adobe Digital Editions. É gratuito e está disponível no site da adobe em: http://www.adobe.com/br/products/digital-editions/download.html
        <br />&emsp;2. Em seguida, faça login com o seu Adobe ID (Passo 1).
        <br />&emsp;3. Depois, clique no link do e-book e inicie a leitura.
      </p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>Importante</span></p>
      <p> &emsp;&emsp;1. As informações contidas nas mensagens enviadas ao aluno, inclusive o link enviado para o download do e-book adquirido, são pessoais e intransferíveis.
        <br />&emsp;&emsp;2. O encaminhamento dessas informações para terceiros pode ocasionar a utilização da licença do produto e, consequentemente, sua perda.
        <br />&emsp;&emsp;3. A <span style={{ fontWeight: 'bold' }}>UNYLEYA</span> disponibiliza apenas um link para download, que deve ser efetuado exclusivamente
        conforme as orientações aqui descritas.
        <br />&emsp;&emsp;4. Não serão disponibilizados outros links para downloads quando a licença do produto já estiver em utilização.
        <br />&emsp;&emsp;5. Dúvidas sobre a compra de e-books, entre em contato pelo e-mail: comercial@imponline.com.br
      </p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>1. Da Pós-graduação</span></p>
      <p>&emsp;&emsp;</p>
      <p>O Termo e Condições acima não se aplica aos cursos de Pós-graduação.</p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>1. Das Disposições gerais</span></p>
      <p> &emsp;&emsp;1. Os conteúdos dos cursos são de propriedade exclusiva da <span style={{ fontWeight: 'bold' }}>UNYLEYA</span> e de seus professores-tutores.
       Não poderão, sem expressa autorização, ser reproduzidos, copiados, divulgados ou distribuídos a qualquer título, tampouco ter o seu conteúdo modificado.
      <br />&emsp;&emsp;2. A <span style={{ fontWeight: 'bold' }}>UNYLEYA</span> não se responsabiliza pela qualidade de conexão via internet entre o computador do
      <span style={{ fontWeight: 'bold' }}>CONTRATANTE</span> e seus servidores, uma vez que essa qualidade depende de diversos fatores externos ao controle.
        Entretanto, recomenda-se que o aluno tenha uma conexão mínima de 2000kbps (2MB).
      <br />&emsp;&emsp;3. A <span style={{ fontWeight: 'bold' }}>UNYLEYA</span> se reserva o direito de cancelar a matrícula e o acesso ao AVA, sem prévio aviso,
        daquele que incorrer em qualquer vedação constante deste contrato ou de outros avisos e instruções de uso posteriormente divulgados, ou que, de alguma forma,
         possam resultar em atividades ilegais ou imorais, hipótese em que não haverá a devolução de qualquer valor já pago pelo</p>
      <p>&emsp;&emsp;</p>
      <p>&emsp;<span style={{ fontWeight: 'bold' }}>1. Do Foro</span></p>
      <p>&emsp;&emsp;As partes contratantes atribuem ao presente contrato plena eficácia e força executiva judicial.
        Fica eleito o foro de Brasília-DF para dirimir quaisquer dúvidas.</p>
      <p>&emsp;&emsp;Os informativos dos cursos, livros e e-books, disponíveis no site são parte integrante deste contrato.</p>
    </div>
  </Fragment>
);
