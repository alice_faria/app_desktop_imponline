export const handleTags = (string) => {
  let stringParsed = string;
  if (stringParsed.includes('<i>')) {
    stringParsed = stringParsed.replace(/<i>/g, '<span style="font-style: italic">');
    stringParsed = stringParsed.replace(/<\/i>/g, '</span>');
  }
  if (stringParsed.includes('<b>')) {
    stringParsed = stringParsed.replace(/<b>/g, '<span style="font-weight: bold">');
    stringParsed = stringParsed.replace(/<\/b>/g, '</span>');
  }
  return stringParsed;
};
