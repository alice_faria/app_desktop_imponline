export const mockSuccess = (data) => ({
  status: 200,
  response: {
    token: data.token,
    usuario: data.usuario,
    acessos: data.acessos
  }
});
export const mockSuccessPassword = (data) => ({
  status: 200,
  response: {
    titulo: data.title,
    mensagem: data.message
  }
});
export const mockError = (error) => ({
  status: 418,
  response: {
    titulo: error.title,
    mensagem: error.message
  }
});
