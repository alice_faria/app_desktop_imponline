import React from 'react';
import moment from 'moment';
import parse from 'html-react-parser';
import { MessageBox } from 'react-chat-elements';

import 'react-chat-elements/dist/main.css';
import { handleTags } from 'helpers/handleTags';

export const handleDate = item => {
  if (moment(item.date).format('DD/MM') === moment().format('DD/MM')) {
    return 'Hoje';
  }
  if (moment(item.date).format('DD/MM') === moment().add(-1, 'days').format('DD/MM')) {
    return 'Ontem';
  }
  return moment(item.date).format('DD/MM');
};

const itsMe = item => {
  return item.fromMe ? 'right' : 'left';
};

export const handleMessages = messages => {
  return messages.map((item, index, arr) => (
    <React.Fragment>
      {index === 0
        ? <span className="messages__chat--time">{moment(item.date).format('DD/MM')}</span>
        : moment(item.date).format('DD/MM') === moment(arr[index - 1].date).format('DD/MM')
          ? null
          : <span className="messages__chat--time">{handleDate(item)}</span>
      }
      <MessageBox
        key={index}
        position={`${itsMe(item)}`}
        dateString={moment(item.date).format('HH:mm')}
        text={parse(handleTags(item.message))}
        status={`${item.fromMe ? item.status : ''}`}
      />
    </React.Fragment>
  ));
};
