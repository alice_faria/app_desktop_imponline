/* eslint-disable max-len */
/* eslint-disable no-undefined */
export const FORM_RULES = {
  required: value => (value ? undefined : 'Esse campo é obrigatório'),
  // verifyEmail: value => (!verifyEmailAPI(value) ? 'Esse email já está cadastrado' : undefined),
  max: max => value => value && value.length > max ? `Esse campo deve possuir no máximo ${max} caracteres` : undefined,
  min: min => value => value && value.length < min ? `Esse campo deve possuir no minimo ${min} caracteres` : undefined,
  number: value => value && isNaN(Number(value)) ? 'Este campo só aceita números' : undefined,
  minValue: min => value => value && value < min ? `O valor deve ser maior que ${min}` : undefined,
  minValue3: value => value && value < 3 ? 'O valor deve ser maior que 3' : undefined,
  maxValue: max => value => value && value < max ? `O valor deve ser menor que ${max}` : undefined,
  password: value => (value && !/((?=.*\d)(?=.*[a-z])(?=.*[A-Z]){8,20})/i.test(value) ? 'Senha inválida' : undefined),
  email: value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Email inválido' : undefined,
  tooOld: value => value => value && value > 65 ? 'You might be too old for this' : undefined,
  alphaNumeric: value => value && /[^a-zA-Z0-9 ]/i.test(value) ? 'Somente caracteres alfanuméricos' : undefined
};

export const MESSAGES = {
  required: 'Este campo é obrigatório',
  whitespace: 'Este campo não pode ser vazio',
  mail: 'Digite um email válido',
  number: 'Este campo deve conter apenas números',
  url: 'Digite uma url válida'
};

export const FORMRULES = {
  required: { required: true, message: MESSAGES.required },
  whitespace: { whitespace: true, message: MESSAGES.whitespace },
  number: { pattern: /\d+/, message: MESSAGES.number },
  mail: { pattern: /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i, message: MESSAGES.mail },
  url: { pattern: /[-a-zA-Z0-9@:%_+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_+.~#?&//=]*)?/, message: MESSAGES.url }
};
