export const getFirstValidElement = (array) => {
  return array.findIndex(item => {
    return item !== null;
  });
};

