
// eslint-disable-next-line no-unused-vars
import React from 'react';
import { render } from 'react-dom';

import { Provider } from 'react-redux';
import Favicon from 'react-favicon';

import Routes from './main/routes';
import logoImp from './assets/images/logo_icon_white.png';
import { ConnectedRouter } from 'connected-react-router';
import { configureStore, history } from './main/configureStore';
import ErrorBoundary from './ErrorBoundary';
import * as serviceWorker from './serviceWorker';

const store = configureStore();

serviceWorker.unregister();

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Favicon url={logoImp} />
      <ErrorBoundary>
        <Routes />
      </ErrorBoundary>
    </ConnectedRouter>
  </Provider>
  ,document.getElementById('root')
);
