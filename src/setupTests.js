import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { JSDOM } from 'jsdom';
import toJson from 'enzyme-to-json';
import { initMock, mockServiceCreator } from '../mockServiceCreator.js';
import 'jest-canvas-mock';

configure({
  adapter: new Adapter(),
  disableLifecycleMethods: true
});

const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
const { window } = jsdom;

function copyProps(src, target) {
  const props = Object.getOwnPropertyNames(src)
    .filter(prop => typeof target[prop] === 'undefined')
    .reduce((result, prop) => ({
      ...result,
      [prop]: Object.getOwnPropertyDescriptor(src, prop)
    }), {});
  Object.defineProperties(target, props);
}

global.window = window;
global.document = window.document;
global.toJson = toJson;
global.initMock = initMock;
global.mockServiceCreator = mockServiceCreator;
global.navigator = {
  userAgent: 'node.js'
};
global.router = {
  go: () => {},
  push: () => {},
  goBack: () => {},
  replace: () => {},
  isActive: () => {},
  goForward: () => {},
  setRouteLeaveHook: () => {},
  getCurrentLocation: () => {}
};
copyProps(window, global);
