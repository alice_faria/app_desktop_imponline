import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { Terms } from './terms';

Enzyme.configure({ adapter: new Adapter() });

const setup = () => {
  let props = {
    auth: {
      user: null,
      validToken: undefined,
      loading: false
    },
    authActions: {
      forgotPassword: jest.fn(),
      setLoading: jest.fn()
    }
  };

  let TermsWrapper = shallow(<Terms {...props} />);

  return { props, TermsWrapper };
};

//Testing the connected component
describe('Testing Login', () => {
  const { TermsWrapper } = setup();

  it('renders all parts', () => {
    expect(TermsWrapper).toMatchSnapshot();
  });

});

