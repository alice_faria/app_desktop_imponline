import React from 'react';

import { TermsText } from '../../helpers/termsText';

const _backToHome = (props) => {
  return props.history.push('/login');
};

const Terms = (props) => {

  return (
    <section className="terms">
      <div className="padding-bottom-medium">
        <div className="terms__header">
          <div className="margin-top-small ">
            <i
              className="icon-left-arrow"
              style={{
                fontSize: '1.5rem',
                color: '#FFF',
                position: 'absolute',
                left: '5.7rem',
                top: '4rem',
                backgroundColor: '#1F1F1F',
                padding: '0.3rem .8rem',
                cursor: 'pointer'
              }}
              onClick={() => _backToHome(props)}
            />
            <span className="forgotPassword__header-box--title ">TERMOS E CONDIÇÕES</span>
          </div>
        </div>
        <div className="terms-default fade in">
          <div className="terms__body center-text" style={{ paddingTop: 20 }}>
            {TermsText}

          </div>
        </div>
      </div>
    </section>
  );
};
export { Terms };
