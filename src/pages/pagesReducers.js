import authReducer from './auth/authReducer';

export default {
  auth: authReducer
};
