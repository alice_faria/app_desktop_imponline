/* eslint-disable max-nested-callbacks */
import * as AuthActions from '../authActions';
import moxios from 'moxios';
import { makeMockStore } from 'helpers/___mocks__/mockConfig';
import { mockError, mockSuccess, mockSuccessPassword } from 'helpers/___mocks__/MockResponses';

const store = makeMockStore({ user: {} });

describe('Testing Login Actions', () => {
  beforeEach(() => {
    // eslint-disable-next-line no-unused-expressions
    moxios.install();
    store.clearActions();
  });
  afterEach(() => {
    moxios.uninstall();
  });

  const user = {
    st_login: 'repudiandae',
    st_senha: 'quasi'
  };

  it('should fail in login without user data', () => {
    const expected = [
      {
        payload: {
          titulo: 'Não foi possível autenticar',
          mensagem: 'Usuário e Senha obrigatórios'
        },
        type: 'ERROR_LOGIN'
      },
      { payload: false, type: 'AUTH_LOADING' }
    ];

    const error = {
      title: 'Não foi possível autenticar',
      message: 'Usuário e Senha obrigatórios'
    };

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith(mockError(error));
    });

    return store.dispatch(AuthActions.login(user)).then(() => {
      const actions = store.getActions();
      expect(actions).toEqual(expected);
    });
  });

  it('should fail in login incorrectly user data', () => {
    const error = {
      title: 'Não foi possível autenticar',
      message: 'Usuário e/ou senha inválidos'
    };

    const expected = [
      {
        payload: {
          titulo: 'Não foi possível autenticar',
          mensagem: 'Usuário e/ou senha inválidos'
        },
        type: 'ERROR_LOGIN'
      },
      { payload: false, type: 'AUTH_LOADING' }
    ];

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith(mockError(error));
    });

    return store.dispatch(AuthActions.login(user)).then(() => {
      const actions = store.getActions();
      expect(actions).toEqual(expected);
    });
  });

  it('Should dispatch a sync Action to logout', () => {
    const store = makeMockStore();

    store.dispatch(AuthActions.logout());

    const actions = store.getActions();
    const expectPayload = [{ type: 'RESET_APPLICATION' }, { type: 'LOGOUT' }];
    expect(actions).toEqual(expectPayload);
  });

});

describe('Testing Forgot Password', () => {
  const store = makeMockStore({ user: {} });
  beforeEach(() => {
    // eslint-disable-next-line no-unused-expressions
    moxios.install();
    store.clearActions();
  });
  afterEach(() => {
    moxios.uninstall();
  });

  it('should return wrong email format error', () => {
    const error = {
      title: 'Não foi possível recuperar a senha',
      message: 'Por favor, informe um e-mail válido'
    };

    const email = {
      st_email: 'blablabla'
    };

    const expected = [
      {
        payload: {
          titulo: 'Não foi possível recuperar a senha',
          mensagem: 'Por favor, informe um e-mail válido'
        },
        type: 'ERROR_FORGOT_PASSWORD'
      },
      { payload: false, type: 'AUTH_LOADING' }
    ];

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith(mockError(error));
    });

    return store.dispatch(AuthActions.forgotPassword(email)).then(() => {
      const actions = store.getActions();
      expect(actions).toEqual(expected);
    });
  });
  it('should send email to user', () => {
    const success = {
      title: 'Dados enviados com sucesso',
      message: 'Um e-mail com seus dados de acesso foram enviados para o e-mail informado'
    };

    const email = {
      st_email: 'teste@teste.com'
    };

    const expected = [
      {
        payload: {
          titulo: 'Dados enviados com sucesso',
          mensagem: 'Um e-mail com seus dados de acesso foram enviados para o e-mail informado'
        },
        type: 'FORGOT_PASSWORD_FETCHED'
      },
      { payload: false, type: 'AUTH_LOADING' }
    ];

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith(mockSuccessPassword(success));
    });

    return store.dispatch(AuthActions.forgotPassword(email)).then(() => {
      const actions = store.getActions();
      expect(actions).toEqual(expected);
    });
  });
});

describe('Testing Login Actions', () => {
  beforeEach(() => {
    moxios.install();
    store.clearActions();
  });
  afterEach(() => {
    moxios.uninstall();
  });

  const user = {
    st_login: 'samuel.silva',
    st_senha: 'homologa'
  };

  it('successfull api request calls setCurrentUser action', () => {
    const data = {
      token: 'jwt token',
      usuario: {
        id_usuario: '10214797',
        st_login: 'samuel.silva',
        st_nomecompleto: 'Samuel Viana da Silva',
        id_usuariopai: null,
        bl_ativo: '1',
        st_cpf: '02878465121',
        id_registropessoa: '4',
        bl_redefinicaosenha: '1',
        id_titulacao: null,
        st_urlavatar: null,
        st_email: '1729460@unyhomologa.com',
        id_matricula: '686666'
      },
      acessos: [
        {
          id_usuario: '10214797',
          st_nomecompleto: 'Samuel Viana da Silva',
          id_entidade: '12',
          st_nomeentidade: 'IMP Online',
          st_razaosocial: 'IMP Online (compra via consultor)',
          st_urlimglogo: 'http://g2evolutiva.unyleya.xyz/upload/entidade/12.png',
          st_nomeperfil: 'Aluno',
          st_perfilpedagogico: 'Aluno',
          id_perfilpedagogico: '5',
          id_projetopedagogico: '3621363',
          st_tituloexibicao: 'DETRAN SP - Oficial Estadual de Trânsito - Pós Edital',
          bl_consultoria: '0',
          id_matricula: '686666',
          id_entidadematricula: '12',
          dt_inicio: '2019-06-05 16:49:13.000000',
          dt_termino: '2020-01-31 00:00:00.000000',
          id_evolucaomatricula: '6',
          st_evolucaomatricula: 'Cursando',
          id_situacaomatricula: '50',
          st_situacaomatricula: 'Ativa',
          st_situacaoperfil: 'Ativo',
          bl_bloqueiadisciplina: '0',
          st_urlacesso: '425f59d3c3687edfb2fc4ec62b4bbe40',
          bl_novoportal: '1',
          bl_acessoapp: '1',
          st_urlportal: 'http://portalmanutencao.unyleya.xyz',
          st_urlnovoportal: 'http://portalfaculdade.manutencao.unyleya.xyz',
          st_urlacessodireto: 'http://portalfaculdade.manutencao.unyleya.xyz/login/direto/0/10214797/12/10214797/425f59d3c3687edfb2fc4ec62b4bbe40'
        }
      ]
    };

    const expected = [
      { type: 'TOKEN_FETCHED', payload: { token: 'jwt token', expiration_date: undefined } },
      { type: 'TOKEN_VALIDATED', payload: true },
      { type: 'USER_FETCHED', payload: data.usuario }
    ];

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith(mockSuccess(data));
    });

    return store.dispatch(AuthActions.login(user)).then(() => {
      const actions = store.getActions();
      expect(actions).toEqual(expected);
    });
  });
});
