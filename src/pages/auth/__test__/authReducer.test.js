/* eslint-disable no-undefined */
import AuthReducers from '../authReducer';
import types from '../authTypes';

describe('Testing Auth Reducers', () => {

  it('should return initial state', () => {
    const action = { type: 'dummy_action' };

    expect(AuthReducers(undefined, action)).toMatchSnapshot();
  });

  it('should return that token is validated',() => {
    const action = { type: types.TOKEN_VALIDATED, payload: true };

    expect(AuthReducers(undefined, action)).toMatchSnapshot();
  });

  it('should return that token is invalidated',() => {
    const action = { type: types.TOKEN_VALIDATED };

    expect(AuthReducers(undefined, action)).toMatchSnapshot();
  });

  it('should save user token in localStorage and return then',() => {
    const token = '[jwt token]';
    const action = { type: types.TOKEN_FETCHED, payload: token };

    expect(AuthReducers(undefined, action)).toMatchSnapshot();
  });

  it('should activate loading component',() => {
    const action = { type: types.AUTH_LOADING, payload: true };

    expect(AuthReducers(undefined, action)).toMatchSnapshot();
  });

  it('should deactivate loading component',() => {
    const action = { type: types.AUTH_LOADING, payload: false };

    expect(AuthReducers(undefined, action)).toMatchSnapshot();
  });

  it('should save user data in localStorage and return then',() => {
    const userData = {
      id_usuario: '0000000000',
      st_login: 'login.usuario',
      st_nomecompleto: 'João da Silva Santos',
      id_usuariopai: null,
      bl_ativo: '1',
      st_cpf: '00000000000',
      id_registropessoa: '0',
      bl_redefinicaosenha: '0',
      st_urlavatar: null,
      id_titulacao: null
    };
    const action = { type: types.USER_FETCHED, payload: userData };

    expect(AuthReducers(undefined, action)).toMatchSnapshot();
  });

  it('should logout user and remove data user from localStorage',() => {
    const action = { type: types.LOGOUT };

    expect(AuthReducers(undefined, action)).toMatchSnapshot();
  });
});
