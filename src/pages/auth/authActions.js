/* eslint-disable no-undefined */
import axios from 'axios';
import types from './authTypes';

import { toastr } from 'react-redux-toastr';

import { setLoading } from './../../helpers/redux/loading';
import { BASE_API } from './../../config/consts';

import { history } from 'main/configureStore';
import { errorHandler } from 'helpers/errorHandler';

/**
 * @function setUserData - Função que serve para colocar os dados
 * do usuário no reducer
 * @param {String} user
 * @returns {Object} Evento que diz para a aplicação inserir o usuário
 * no reducer
 */
export const setUserData = (user) => {
  return {
    type: types.USER_FETCHED,
    payload: user
  };
};

/**
 * @function login - Função utilizada para solicitar os dados de
 * acesso e do usuário
 * @param {Object} values
 * @returns {Function} Função para pegar os dados do usuário
 */
export const login = (values, router) => {
  const newValues = {
    st_login: values.username,
    st_senha: values.password
  };
  return (dispatch) => {
    return axios({
      method: 'post',
      url: `${BASE_API}/api/login/autenticar`,
      data: newValues
    })
      .then((response) => {
        const data_token = {
          token: response.data.token,
          expiration_date: response.data.dt_validade
        };
        const user = { ...response.data.usuario, id_matricula: response.data.acessos[0].id_matricula };
        delete user['token'];
        dispatch({ type: types.TOKEN_FETCHED, payload: data_token });
        dispatch({ type: types.TOKEN_VALIDATED, payload: true });
        dispatch(setUserData(user));
        history.push('/');
      })
      .catch((err) => {
        errorHandler(dispatch, err, 'ERROR_LOGIN', types.AUTH_LOADING);
      });
  };
};

export const resetApplication = () => {
  return (dispatch) => {
    dispatch({ type: 'RESET_APPLICATION' });
  };
};

/**
 * @function logout - Função que retira os dados do usuário do
 * cache do navegador e revoga o token na API
 * @returns {Object} - Ação para colocar o reducer no estado
 * inicial
 */
export const logout = () => {
  return (dispatch) => {
    dispatch(resetApplication());
    dispatch({ type: types.LOGOUT });
  };
};

/**
 * @function forgotPassword - Função utilizada para que o usuário
 * consiga recuparar sua senha
 * @param {string} data
 * @param {String} router
 * @returns {Function} Função para enviar email de recuperação de senha
 */
export const forgotPassword = (data) => {
  const newEmail = { st_email: data.email };
  return (dispatch) => {
    return axios({
      method: 'post',
      url: `${BASE_API}/api/login/recuperar-senha`,
      data: newEmail
    })
      .then((response) => {
        dispatch({ type: 'FORGOT_PASSWORD_FETCHED', payload: response.data });
        dispatch(setLoading(types.AUTH_LOADING, false));
        toastr.success(`${response.data.titulo}\n${response.data.mensagem}`);
      })
      .catch((err) => {
        errorHandler(dispatch, err, 'ERROR_FORGOT_PASSWORD', types.AUTH_LOADING);
      });
  };
};
