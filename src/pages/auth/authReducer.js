import types from './authTypes';

import { USER_KEY, USER_TOKEN } from '../../config/consts';
import update from "immutability-helper";

const initialState = {
  user: JSON.parse(localStorage.getItem(USER_KEY)) || null,
  userToken: JSON.parse(localStorage.getItem(USER_TOKEN)) || null,
  validToken: false,
  loading: false,
  recoveyResponse: {},
  recoverySuccess: false
};

/**
 * @param {Object} state - Default application state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.TOKEN_VALIDATED:
      if (payload) {
        return { ...state, validToken: true };
      } else {
        localStorage.removeItem(USER_KEY);
        localStorage.removeItem(USER_TOKEN);
        return { ...state, validToken: false, user: null };
      }
    case types.TOKEN_FETCHED:
      localStorage.setItem(USER_TOKEN, JSON.stringify(payload));
      return {
        ...state,
        userToken: payload || initialState.userToken
      };
    case types.AUTH_LOADING:
      return {
        ...state,
        loading: payload || initialState.loading
      };
    case types.USER_FETCHED:
      localStorage.setItem(USER_KEY, JSON.stringify(payload));
      return {
        ...state,
        user: payload || initialState.user,
        loading: false
      };
    case types.DADOS_IMAGEM:
      let item = JSON.parse(localStorage.getItem(USER_KEY));
      item.st_urlavatars3 = payload.data.st_urlavatars3;
      localStorage.setItem(USER_KEY, JSON.stringify(item));
      return update(state, {
        user: {
          st_urlavatars3: {$set:payload.data.st_urlavatars3}
        }
      });
    case types.DADOS_IMAGEM_FIRST:
      let itemFirst = JSON.parse(localStorage.getItem(USER_KEY));
      itemFirst.st_urlavatars3 = payload.data.st_urlavatars3;
      localStorage.setItem(USER_KEY, JSON.stringify(itemFirst));
      return update(state, {
        user: {
          st_urlavatars3: {$set:payload.data.st_urlavatars3}
        }
      });
    case types.FORGOT_PASSWORD_FETCHED:
      return {
        ...state,
        recoveyResponse: payload || initialState.recoveyResponse,
        recoverySuccess: true,
        loading: false
      };
    case types.LOGOUT:
      localStorage.removeItem(USER_KEY);
      localStorage.removeItem(USER_TOKEN);
      return {
        ...state,
        ...initialState,
        user: null,
        userToken: false
      };
    default:
      return state;
  }
};
