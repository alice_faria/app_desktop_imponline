import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { ForgotPassword } from './forgotPassword';

Enzyme.configure({ adapter: new Adapter() });

const setup = () => {
  let props = {
    auth: {
      user: null,
      validToken: undefined,
      loading: false
    },
    authActions: {
      forgotPassword: jest.fn(),
      setLoading: jest.fn()
    }
  };

  let ForgotPassWrapper = shallow(<ForgotPassword {...props} />);

  return { props, ForgotPassWrapper };
};

//Testing the connected component
describe('Testing Login', () => {
  const { ForgotPassWrapper } = setup();

  it('renders all parts', () => {
    expect(ForgotPassWrapper).toMatchSnapshot();
  });

});

