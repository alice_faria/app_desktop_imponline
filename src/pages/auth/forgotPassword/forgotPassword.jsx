import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Form, Field } from 'redux-form';

import imgForgotPassword from 'assets/images/img_email.png';
import imgForgotPasswordSuccess from 'assets/images/img_email_success.png';
import Message from 'common/components/message/message';
import Loading from 'common/components/ui/loading/loading';
import Button from 'common/components/ui/button/button';

import types from '../authTypes';
import { forgotPassword } from '../authActions';
import { setLoading } from 'helpers/redux';
import { InputLabel } from 'common/components/form/fields';
import { FORM_RULES } from 'helpers/validations';

export class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false
    };
  }

  handleFocus = (field) => this.setState({ [field]: true });
  handleBlur = (field) => this.setState({ [field]: false });

  onSubmit = (values) => {
    this.props.setLoading(types.AUTH_LOADING, true);
    this.props.forgotPassword(values, this.props.router);
  }

  _backToHome = () => {
    return this.props.history.push('/login');
  }

  toggleIcon = () => {
    if (this.props.recovery) {
      return imgForgotPasswordSuccess;
    }
    return imgForgotPassword;
  }

  render() {
    const { handleSubmit, invalid, pristine, submitting } = this.props;
    return (
      <section className="forgotPassword">
        <div className={this.props.recovery ? 'forgotPassword__box--success' : 'forgotPassword__box'}>
          <div className="login__header-box">
            <div className="margin-top-small ">
              <i
                className="icon-left-arrow"
                style={{
                  fontSize: '1.5rem',
                  color: '#FFF',
                  position: 'absolute',
                  left: '5.7rem',
                  top: '4rem',
                  backgroundColor: '#1F1F1F',
                  padding: '0.3rem .8rem',
                  cursor: 'pointer'
                }}
                onClick={this._backToHome}
              />
              <span className="forgotPassword__header-box--title ">Recuperar senha</span>
            </div>
            <div className="">
              <img src={this.toggleIcon()} className={this.props.recovery ? 'forgotPassword__header-box--success' : 'forgotPassword__header-box--img'} alt="Logo" />
            </div>
          </div>
          <div className="forgotPassword__body">
            {this.props.recovery
              ? <p>Cheque <span style={{ fontWeight: 'bold' }}>seu e-mail.</span><br />Enviamos a recuperação de senha.</p>
              : <p>Para recuperar a sua senha,<br /> insira o <span style={{ fontWeight: 'bold' }}>seu e-mail.</span></p>
            }
          </div>
          {this.props.loading
            ? <Loading
              isVisible={this.props.loading}
              ref="loading"
            />
            : this.props.recovery ? null
              : <Form className="form forgotPassword__form-box" role='form' onSubmit={handleSubmit(this.onSubmit)} >
                <div className="form__body">
                  <Field
                    inputClass="input--default"
                    component={InputLabel}
                    style={{ textAlign: 'left', fontSize: '1.6rem', fontWeight: 'bold', color: 'white', paddingRight: 0, paddingLeft: 0 }}
                    inputStyle={{
                      fontSize: '1.3rem',
                      padding: '2.85rem .5rem .85rem .85rem',
                      borderRadius: '.4rem',
                      backgroundColor: 'transparent',
                      width: '100%',
                      height: '5.5rem'
                    }}
                    type="text"
                    name="email"
                    isFocused={this.state.isFocused}
                    autoFocus
                    onFocus={() => this.handleFocus('isFocused')}
                    onBlur={() => this.handleBlur('isFocused')}
                    autocomplete="email"
                    label="Email"
                    cols='12 12 12 12'
                    validate={[FORM_RULES.required, FORM_RULES.email]}
                  />
                </div>
                <div>
                  <Button
                    text='Recuperar'
                    type="submit"
                    disabled={invalid || pristine || submitting}
                    style={{ width: '20rem', height: '4rem', padding: 5 }}
                    rounded
                    radius={10}
                    buttonType='primary-forgotPass'
                  />
                </div>
              </Form>
          }
        </div>
        <Message />
      </section>
    );
  }
}

ForgotPassword = reduxForm({ form: 'passwordForm' })(ForgotPassword);

const mapStateToProps = (state) => {
  return {
    loading: state.auth.loading,
    recovery: state.auth.recoverySuccess
  };
};

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({ forgotPassword, setLoading }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
