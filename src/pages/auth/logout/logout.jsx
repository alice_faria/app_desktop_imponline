import React,{ Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logout } from '../authActions';
import { Redirect } from 'react-router-dom';

class Logout extends Component {

  componentDidMount() {
    this.props.logout();
  }

  render() {
    return <Redirect to='/login' />;
  }
}

const mapStateToProps = null;

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ logout }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(Logout);
