/* eslint-disable no-mixed-spaces-and-tabs */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Form, Field } from 'redux-form';
import { Link, Redirect } from 'react-router-dom';
import _ from 'lodash';

import { FORM_RULES } from 'helpers/validations';

import LogoImp from 'assets/images/logo_white.png';
import { InputLabel } from 'common/components/form/fields';
import Message from 'common/components/message/message';
import Loading from 'common/components/ui/loading/loading';
import Button from 'common/components/ui/button/button';

import types from '../authTypes';
import { login } from '../authActions';
import { setLoading } from 'helpers/redux';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      valueUser: '',
      valuePassword: '',
      isFocusedUsername: false,
      isFocusedPassword: false,
      showPassword: false
    };
    this.handleBasicInputChanges = this.handleBasicInputChanges.bind(this);

  }

  handleBasicInputChanges(e) {
    this.setState({
      valueUser: e.target.value

    });
  }
  handleBasicInputChangesPassword(e) {
    this.setState({
      valuePassword: e.target.value
    });
  }

  _toogleShowPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  };

  onSubmit = (values) => {
    this.props.setLoading(types.AUTH_LOADING, true);
    this.props.login(values, this.props.router);
  };

  _goToTerms = () => {
    return this.props.history.push('/termo-de-uso');
  }

  handleFocus = (field) => this.setState({ [field]: true });
  handleBlur = (field) => this.setState({ [field]: false });

  render() {
    const { handleSubmit, pristine, submitting, invalid } = this.props;
    if (!_.isUndefined(this.props.user) && !_.isNull(this.props.user)) {
      return <Redirect to='/' />;
    }
    return (
      <section data-test="loginComponent" className={this.state.isModalOpen ? 'login overlay' : 'login'}>
        <div className="login__box">
          <div className="login__header-box">
            <div className="login__logo">
              <img src={LogoImp} style={{ width: 200 }} className="login__header-box--img" alt="Logo" />
            </div>
          </div>
          {this.props.loading
            ? <Loading
              isVisible={this.props.loading}
              ref="loading"
            />
            : <React.Fragment>
              <Form id="loginForm" className="form login__form-box" role='form' onSubmit={handleSubmit(this.onSubmit)} noValidate>
                <div className="form__body">
                  <Field
                    id="username"
                    inputClass="input--default"
                    component={InputLabel}
                    style={{ textAlign: 'left', fontSize: '1.6rem', fontWeight: 'bold', color: 'white', paddingRight: 0, paddingLeft: 0 }}
                    inputStyle={{
                      fontSize: '1.3rem',
                      padding: '2.85rem .5rem .85rem .8rem',
                      borderRadius: '.4rem',
                      backgroundColor: 'transparent',
                      width: '100%',
                      height: '5.5rem'
                    }}
                    type="text"
                    name="username"
                    autoFocus
                    isFocused={this.state.isFocusedUsername}
                    autocomplete="username"
                    onFocus={() => this.handleFocus('isFocusedUsername')}
                    onBlur={() => this.handleBlur('isFocusedUsername')}
                    onUpdate={this.handleBasicInputChanges}
                    label="Usuário"
                    cols='12 12 12 12'
                    validate={[FORM_RULES.required]}
                  />
                  <Field
                    id="password"
                    inputClass="input--default"
                    component={InputLabel}
                    style={{ textAlign: 'left', fontSize: '1.6rem', fontWeight: 'bold', color: 'white', paddingRight: 0, paddingLeft: 0 }}
                    inputStyle={{
                      fontSize: '1.3rem',
                      padding: '2.85rem .5rem .85rem .8rem',
                      borderRadius: '.4rem',
                      backgroundColor: 'transparent',
                      width: '100%',
                      height: '5.5rem'
                    }}
                    type={this.state.showPassword ? 'text' : 'password'}
                    name="password"
                    onFocus={() => this.handleFocus('isFocusedPassword')}
                    onBlur={() => this.handleBlur('isFocusedPassword')}
                    isFocused={this.state.isFocusedPassword}
                    iconRight
                    icon={this.state.showPassword ? 'icon-eye_off' : 'icon-eye_on'}
                    autocomplete="current-password"
                    iconClick={this._toogleShowPassword}
                    onUpdate={this.handleBasicInputChangesPassword}
                    label="Senha"
                    cols='12 12 12 12'
                    validate={[FORM_RULES.required]}
                  />
                </div>
                <div>
                  <Button
                    text='Entrar'
                    type="submit"
                    style={{ width: '20rem', height: '4rem', padding: 5 }}
                    rounded
                    disabled={invalid || pristine || submitting}
                    buttonType='primary'
                    colors='rgba(204,0,0,1)'
                    radius={8}
                  />
                </div>
              </Form>
              <div className="login__footer-box">
                <Link to='esqueci-minha-senha' style={{ flex: 1, textAlign: 'left' }}>Esqueci minha senha</Link>
              </div>
              <div className="login__footer">
                Ao entrar, você concorda com <br />os <span className="login__terms" onClick={this._goToTerms}>termos de uso.</span>
              </div>
            </React.Fragment>
          }
        </div>
        <Message />
      </section>
    );
  }
}

const formLogin = reduxForm({ form: 'loginForm' })(Login);

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    validToken: state.auth.validToken,
    loading: state.auth.loading
  };
};

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({ login, setLoading }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(formLogin);
