/* eslint-disable no-undefined */
import React from 'react';
import { shallow } from 'enzyme';

import { Login as UnconnectedLogin } from './login';
import { makeMockStore } from 'helpers/___mocks__/mockConfig';
import { findByTestAtrr } from 'helpers/wrappers';

const setup = (initialState = {}, handleSubmit) => {
  const store = makeMockStore(initialState);
  return shallow(<UnconnectedLogin handleSubmit={handleSubmit} store={store}/>);
};

//Testing the connected component
describe('Testing Login', () => {
  let wrapper;

  beforeEach(() => {
    const initialState = {
      auth: {
        user: 'Daniel Auler',
        validToken: true,
        loading: false
      },
      authActions: {
        login: jest.fn(),
        setLoading: jest.fn()
      }
    };
    const handleSubmit = jest.fn();
    wrapper = setup(initialState, handleSubmit);
  });

  it('should render the connected component', () => {
    const component = findByTestAtrr(wrapper, 'loginComponent');
    expect(component.length).toBe(1);
  });

  it('should renders all parts', () => {
    expect(wrapper).toMatchSnapshot();
  });

});
