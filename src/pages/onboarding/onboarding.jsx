import React from 'react';
import Flickity from 'react-flickity-component';

import img1 from '../../assets/images/onboarding1.png';
import img2 from '../../assets/images/onboarding2.png';
import img3 from '../../assets/images/onboarding3.png';
import Button from '../../common/components/ui/button/button';

import { Redirect } from 'react-router-dom';

import './flickity.css';

const images = [
  {
    image: img1,
    key: 0,
    subheader:
      <p><span style={{ fontWeight: 'bold' }}>Favorite conteúdos </span>para acessar<br />
        de um jeito rápido
      </p>
  },
  {
    image: img2,
    key: 1,
    subheader:
      <p>Veja notificações de
        <span style={{ fontWeight: 'bold' }}> eventos ao<br /> vivo no YouTube</span>
      </p>
  },
  {
    image: img3,
    key: 2,
    subheader:
      <p>
        <span style={{ fontWeight: 'bold' }}>Continue assistindo </span>
        de onde <br />você parou
      </p>
  }
];

class Onboarding extends React.Component {
  state = {
    redirect: false
  }

  _goToLogin = () => {
    this.setState({ redirect: true });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to="/login" />;
    }
    return (
      <section className="onboarding">
        <div className="onboarding__body">
          <Flickity
            elementType={'div'}
            options={{
              draggable: false,
              cellAlign: 'center',
              contain: true,
              arrowShape: {
                x0: 10,
                x1: 60, y1: 55,
                x2: 70, y2: 55,
                x3: 20
              }
            }}
          >
            {images.map((item, index) => (
              <div className="onboarding__cell" key={item.key}>
                <img src={item.image} className="onboarding__img" alt="carrousel" />
                {item.subheader}
              </div>
            ))}
          </Flickity>
        </div>
        <div className="onboarding__button">
          <Button
            text='Entrar'
            type="button"
            style={{ width: '20rem', height: '4rem', padding: 5 }}
            rounded
            radius={10}
            onClick={() => this._goToLogin()}
            buttonType='primary-onboarding'
          />
        </div>
      </section>
    );
  }
}

export default Onboarding;
