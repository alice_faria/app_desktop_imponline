import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Onboarding from './onboarding';

Enzyme.configure({ adapter: new Adapter() });

const setup = () => {

  let OnboardingWrapper = mount(<Onboarding />);

  return { OnboardingWrapper };
};

//Testing the connected component
describe('Testing Login', () => {
  const { OnboardingWrapper } = setup();

  it('renders all parts', () => {
    expect(OnboardingWrapper).toMatchSnapshot();
  });

});

