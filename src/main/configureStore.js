import { createStore, applyMiddleware, compose } from 'redux';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';

import promise from 'redux-promise';
import multi from 'redux-multi';
import thunk from 'redux-thunk';
import ua from 'universal-analytics';

import rootReducer from './rootReducers';

export const history = createBrowserHistory();

const enhancers = [];
export const middleware = [
  thunk,
  multi,
  promise,
  routerMiddleware(history)
];

const visitor = ua("UA-158514167-1", {http:true});

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

export const configureStore = (preloadState) => {
  preloadState = {analytics: visitor}
  return createStore(
    rootReducer(history),
    preloadState,
    composedEnhancers,
  );
};
