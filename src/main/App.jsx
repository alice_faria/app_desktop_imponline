import React, { Component } from 'react';

import axios from 'axios';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter, Redirect } from 'react-router-dom';

import _ from 'lodash';
import intl from 'react-intl-universal';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';

import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';
import '../common/dependencies';
import '../assets/styles/main.scss';
import ModuleSystem from '../modules/system/main/module-system';
import { setLoading } from '../helpers/redux';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

momentDurationFormatSetup(moment);
const locales = {
  'pt-BR': require('../locales/pt-BR.json')
};

class App extends Component {
  constructor(props) {
    super(props);
    moment.locale('pt-br');
    axios.defaults.headers.common['Accept'] = 'application/json';

    //Para mais linguagens no sistema, basta descomentar a linha abaixo
    // const currentLocale = locales[navigator.language] ? navigator.language : 'pt-BR';
    const currentLocale = 'pt-BR';
    intl.init({
      currentLocale,
      locales
    });
  }
  componentDidMount() {
    history.listen(location => {
      this.props.visitor.pageview(location.hash).send();
    });
  }

  render() {
    if (_.isUndefined(this.props.user) || _.isNull(this.props.user)) {
      return <Redirect to='/onboarding' />;
    } else if (moment(this.props.auth.userToken.expiration_date).diff(moment()) < 0) {
      return <Redirect to='/login' />;
    }
    return (
      <ModuleSystem>
        {this.props.children}
      </ModuleSystem>
    );
  }
}

const mapStateToProps = state => {
  return {
    visitor: state.analytics,
    user: state.auth.user,
    auth: state.auth,
    home: state.homePage,
    loading: state.homePage.loading
  };
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      setLoading
    },
    dispatch
  )
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App));
