import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { reducer as toastrReducer } from 'react-redux-toastr';
import { connectRouter } from 'connected-react-router';

import pagesReducers from '../pages/pagesReducers';
import systemReducers from '../modules/system/systemReducers';
import containersReducers from '../common/containers/containersReducer';

const rootReducer = (history) => combineReducers({
  analytics: (state = {}) => state,
  router: connectRouter(history),
  form: formReducer,
  toastr: toastrReducer,
  containers: containersReducers,

  ...pagesReducers,
  ...systemReducers

});

export default rootReducer;
