import React from 'react';
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import App from './App';

import Login from '../pages/auth/login/login';
import Logout from '../pages/auth/logout/logout';
import ForgotPassword from '../pages/auth/forgotPassword/forgotPassword';
import { Terms } from '../pages/terms/terms';
import Onboarding from '../pages/onboarding/onboarding';

import { systemRouters } from '../modules/system/main/routes-module-system';

export default props => (
  <Router>
    <Switch>
      <Route exact path='/login' component={(props) => <Login {...props} />} />
      <Route exact path='/onboarding' component={(props) => <Onboarding {...props} />} />
      <Route exact path='/logout' component={(props) => <Logout {...props} />} />
      <Route exact path='/termo-de-uso' component={(props) => <Terms {...props} />} />
      <Route exact path='/esqueci-minha-senha' component={(props) => <ForgotPassword {...props} />} />
      <App>
        {systemRouters}
      </App>
      <Redirect from="*" to="/" />
    </Switch>
  </Router>
);
