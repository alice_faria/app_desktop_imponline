const { app, BrowserWindow, dialog, protocol } = require('electron');
const { autoUpdater } = require('electron-updater');

const path = require('path');
const isDev = require('electron-is-dev');

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    backgroundColor: '#070707',
    width: 1046,
    height: 726,
    minHeight: 726,
    minWidth: 1046,
    show: false,
    webPreferences: {
      nodeIntegration: true
    }
  });
  mainWindow.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '..', 'build','index.html')}`);
  mainWindow.on('closed', () => (mainWindow = null));
  mainWindow.once('ready-to-show', () => {
    mainWindow.show();
  });
  mainWindow.removeMenu();
  mainWindow.webContents.on('new-window', (e, url) => {
    e.preventDefault();
    require('electron').shell.openExternal(url);
  });
}

autoUpdater.on('update-downloaded', () => {
  dialog.showMessageBox({
    title: 'Instalar atualizações',
    message: 'Atualizações baixadas, a aplicação será reiniciada para atualizar...'
  }, () => {
    setImmediate(() => autoUpdater.quitAndInstall());
  });
});

app.on('ready', () => {
  createWindow();
});
app.on('window-all-closed', () => {
  app.quit();
});

app.on('ready', () => {
  autoUpdater.checkForUpdates();
});

