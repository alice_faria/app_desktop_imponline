require('jest-canvas-mock');

module.exports = {
  name: 'imp-desktop',
  notify: true,
  verbose: true,
  testEnvironment: 'node',
  testURL: 'http://localhost:3000/',
  setupFiles: ['<rootDir>/src/setupTests.js', 'jest-canvas-mock'],
  testMatch: [
    '<rootDir>/src/**/__tests__/**/*.(spec|test).js?(x)',
    '<rootDir>/src/**/?(*.)(spec|test).js?(x)'
  ],
  collectCoverageFrom: ['src/**/*.{js,jsx}'],
  transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(js|jsx)$'],
  modulePathIgnorePatterns: ['<rootDir>/node_modules'],
  moduleNameMapper: {
    '\\.(css|less|sass|scss)$': '<rootDir>/src/helpers/__mocks__/styleMock.js',
    '\\.(gif|ttf|eot|svg|png)$': '<rootDir>/src/helpers/__mocks__/fileMock.js',
    '^store(.*)$': '<rootDir>/src/$1',
    '^api(.*)$': '<rootDir>/src/api/$1',
    '^proptypes(.*)$': '<rootDir>/src/proptypes/$1',
    '^helpers(.*)$': '<rootDir>/src/helpers/$1',
    '^actions(.*)$': '<rootDir>/src/actions/$1',
    '^reducers(.*)$': '<rootDir>/src/reducers/$1',
    '^__mocks__(.*)$': '<rootDir>/src/helpers/__mocks__/$1',
    '^components([^\\.]*)$': '<rootDir>/src/components/$1',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/src/__mocks__/fileMock.js',
    '\\.(css|sass)$': 'identity-obj-proxy'
  },
  transform: {
    '^.+\\.js$': 'babel-jest',
    '^.+\\.css$': '<rootDir>/config/jest/cssTransform.js',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/config/jest/fileTransformer.js'
  }
};
